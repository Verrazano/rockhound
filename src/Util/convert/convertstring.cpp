#include "convertstring.h"
#include <sstream>

int toInt(std::string s)
{
	std::stringstream ss(s);
	int i;
	ss >> i;
	return i;

}

unsigned int toUInt(std::string s)
{
	std::stringstream ss(s);
	unsigned int i;
	ss >> i;
	return i;

}

float toFloat(std::string s)
{
	std::stringstream ss(s);
	float i;
	ss >> i;
	return i;

}

bool toBool(std::string s)
{
	return s == "true";

}

std::string toString(int i)
{
	std::stringstream ss;
	ss << i;
	std::string s;
	ss >> s;
	return s;

}

std::string toString(unsigned int i)
{
	std::stringstream ss;
	ss << i;
	std::string s;
	ss >> s;
	return s;

}

std::string toString(float i)
{
	std::stringstream ss;
	ss << i;
	std::string s;
	ss >> s;
	return s;

}

std::string toString(bool i)
{
	return i ? "true" : "false";

}
