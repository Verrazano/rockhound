#ifndef MOUSETOVIEW_H_
#define MOUSETOVIEW_H_

#include <SFML/Graphics.hpp>

sf::Vector2f mouseToView(sf::View& v, sf::RenderWindow& win);

#endif /* MOUSETOVIEW_H_ */
