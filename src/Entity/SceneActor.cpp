#include "SceneActor.h"
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/RenderTarget.hpp>

SceneActor::SceneActor(std::string name)
{
	m_texture = NULL;
	m_parent = NULL;
	m_points = sf::VertexArray(sf::Quads, 4);

	setName(name);

}

SceneActor::SceneActor(std::string name, sf::Vector2f size, sf::Color color, sf::Vector2f pos)
{
	m_texture = NULL;
	m_parent = NULL;
	m_points = sf::VertexArray(sf::Quads, 4);

	setName(name);
	setPosition(pos);
	setSize(size);
	setOrigin(size.x/2, size.y/2);
	setColor(color);
	setFrameSize(size);

}

SceneActor::SceneActor(std::string name, sf::Vector2f size,
		sf::Texture* texture, uint32_t frame, sf::Vector2f frameSize, sf::Vector2f pos)
{
	m_texture = NULL;
	m_parent = NULL;
	m_points = sf::VertexArray(sf::Quads, 4);

	setName(name);
	setPosition(pos);
	setSize(size);
	setOrigin(size.x/2, size.y/2);
	setTexture(texture);
	setFrameSize(frameSize);
	setTextureRect(getFrame(frame));

}

SceneActor::SceneActor(std::string name, sf::Vector2f size,
		sf::Texture* texture, sf::IntRect frame, sf::Vector2f pos)
{
	m_texture = NULL;
	m_parent = NULL;
	m_points = sf::VertexArray(sf::Quads, 4);

	setName(name);
	setPosition(pos);
	setSize(size);
	setOrigin(size.x/2, size.y/2);
	setTexture(texture);
	setFrameSize(sf::Vector2f(frame.width, frame.height));
	setTextureRect(frame);

}

SceneActor::SceneActor(std::string name, sf::Texture* texture, sf::IntRect frame, sf::Vector2f pos)
{
	m_texture = NULL;
	m_parent = NULL;
	m_points = sf::VertexArray(sf::Quads, 4);

	setName(name);
	setPosition(pos);
	setSize(frame.width, frame.height);
	setOrigin(frame.width/2, frame.height/2);
	setTexture(texture);
	setFrameSize(sf::Vector2f(frame.width, frame.height));
	setTextureRect(frame);

}

SceneActor::SceneActor(std::string name, sf::Texture* texture, sf::Vector2f pos)
{
	m_texture = NULL;
	m_parent = NULL;
	m_points = sf::VertexArray(sf::Quads, 4);

	setName(name);
	setPosition(pos);
	sf::Vector2f size(texture->getSize().x, texture->getSize().y);
	setSize(size);
	setOrigin(size.x/2, size.y/2);
	setTexture(texture);
	setFrameSize(size);

}

void SceneActor::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	states.texture = m_texture;
	target.draw(m_points, states);

	std::list<SceneActor>::const_iterator it;
	for(it = m_children.begin(); it != m_children.end(); it++)
	{
		it->draw(target, states);

	}

}

void SceneActor::setPosition(float x, float y)
{
	setPosition(sf::Vector2f(x, y));

}

#include <iostream>

void SceneActor::setPosition(sf::Vector2f position)
{
	if(m_parent != NULL)
	{
		position += m_parent->getOrigin();

	}

	Transformable::setPosition(position);

}

void SceneActor::setName(std::string name)
{
	m_name = name;

}

std::string SceneActor::getName()
{
	return m_name;

}

void SceneActor::setSize(float width, float height)
{
	setSize(sf::Vector2f(width, height));

}

void SceneActor::setSize(sf::Vector2f size)
{
	//m_points[0].position = sf::Vector2f(0, 0);
	m_points[1].position = sf::Vector2f(size.x, 0);
	m_points[2].position = sf::Vector2f(size.x, size.y);
	m_points[3].position = sf::Vector2f(0, size.y);

	setOrigin(size.x/2, size.y/2);

}

sf::Vector2f SceneActor::getSize()
{
	return m_size;

}

void SceneActor::setFrameSize(float width, float height)
{
	setFrameSize(sf::Vector2f(width, height));

}

void SceneActor::setFrameSize(sf::Vector2f size)
{
	m_frameSize = size;

}

sf::Vector2f SceneActor::getFrameSize()
{
	return m_frameSize;

}

void SceneActor::setTexture(sf::Texture* texture)
{
	if(texture == NULL)
	{
		return;

	}

	m_texture = texture;
	setTextureRect(sf::IntRect(0, 0, texture->getSize().x, texture->getSize().y));

}

void SceneActor::setTextureRect(sf::IntRect frame)
{
	m_points[0].texCoords = sf::Vector2f(frame.left, frame.top);
	m_points[1].texCoords = sf::Vector2f(frame.left + frame.width, frame.top);
	m_points[2].texCoords = sf::Vector2f(frame.left + frame.width, frame.top + frame.height);
	m_points[3].texCoords = sf::Vector2f(frame.left, frame.top + frame.height);

}

void SceneActor::setColor(sf::Color color)
{
	m_color = color;

	m_points[0].color = m_color;
	m_points[1].color = m_color;
	m_points[2].color = m_color;
	m_points[3].color = m_color;

}

sf::Color SceneActor::getColor()
{
	return m_color;

}

sf::IntRect SceneActor::getFrame(uint32_t frame)
{
	if(m_texture == NULL || m_frameSize.x == 0 || m_frameSize.y == 0)
	{
		return sf::IntRect(0, 0, 0, 0);

	}

	unsigned int x = m_texture->getSize().x/m_frameSize.x;
	unsigned int y = m_texture->getSize().y/m_frameSize.y;

	if(x == 0 || y == 0)
	{
		return sf::IntRect(0, 0, 0, 0);

	}

	unsigned int frames = x*y;

	if(frame > frames)
	{
		return sf::IntRect(0, 0, m_frameSize.x, m_frameSize.y);

	}

	unsigned int fx = frame%x;
	unsigned int fy = frame/x;
	fx *= m_frameSize.x;
	fy *= m_frameSize.y;

	//fx += frameOffsetX;
	//fy += frameOffsetY;

	return sf::IntRect(fx, fy, m_frameSize.x, m_frameSize.y);

}

SceneActor* SceneActor::getParent()
{
	return m_parent;

}

SceneActor* SceneActor::addChild(SceneActor sceneActor)
{
	std::string name = sceneActor.getName();
	sceneActor.m_parent = this;
	sf::Vector2f pos = sceneActor.getPosition();
	sceneActor.setPosition(pos);
	m_children.push_back(sceneActor);
	return getChild(name);

}

bool SceneActor::hasChild(std::string name)
{
	return getChild(name) != NULL;

}

SceneActor* SceneActor::getChild(std::string name)
{
	std::list<SceneActor>::iterator it;
	for(it = m_children.begin(); it != m_children.end(); it++)
	{
		if(it->getName() == name)
		{
			SceneActor* sceneActor = &(*it);
			return sceneActor;

		}

	}

	return NULL;

}

void SceneActor::remChild(std::string name)
{
	std::list<SceneActor>::iterator it;
	for(it = m_children.begin(); it != m_children.end(); it++)
	{
		if(it->getName() == name)
		{
			m_children.erase(it);

		}

	}

}

