#ifndef TAGMANAGER_H_
#define TAGMANAGER_H_
/*
The MIT License (MIT)

Copyright (c) 2014 Harrison D. Miller

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

author: Harrison D. Miller
psuedonym: Verrazano
contact: hdmscratched@gmail.com
*/

#include <list>
#include <string>

/**TagManager class.
 * Manages tags. Used to dynamically make temporary flags.
 */
class TagManager
{
public:
	/**TagManager constructor.
	 */
	TagManager();

	/**TagManager deconstructor.
	 */
	virtual ~TagManager();

	/**Adds a tag to the manager.
	 * @param tag the tag to add.
	 */
	void addTag(std::string tag);

	/**Removes a tag from the manager.
	 * @param tag the tag to remove.
	 */
	void remTag(std::string tag);

	/**Returns true if the manager contains the tag.
	 * @param the tag to check for in the manager.
	 * @return true if the manager contains the tag.
	 */
	bool hasTag(std::string tag);

	/**Clears the manager of tags.
	 */
	void clearTags();

private:
	std::list<std::string> m_tags; ///< The list of tags.

};

#endif /* TAGMANANGER_H_ */
