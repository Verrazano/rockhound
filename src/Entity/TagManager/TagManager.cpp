#include "TagManager.h"

TagManager::TagManager()
{
}

TagManager::~TagManager()
{
}

void TagManager::addTag(std::string tag)
{
	if(!hasTag(tag))
	{
		m_tags.push_back(tag);

	}

}

void TagManager::remTag(std::string tag)
{
	m_tags.remove(tag);

}

bool TagManager::hasTag(std::string tag)
{
	std::list<std::string>::iterator it;
	for(it = m_tags.begin(); it != m_tags.end(); it++)
	{
		if((*it) == tag)
		{
			return true;

		}

	}

	return false;

}

void TagManager::clearTags()
{
	m_tags.clear();

}
