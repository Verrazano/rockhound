#ifndef VARBASE_H_
#define VARBASE_H_
/*
The MIT License (MIT)

Copyright (c) 2014 Harrison D. Miller

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

author: Harrison D. Miller
psuedonym: Verrazano
contact: hdmscratched@gmail.com
*/

#include <string>
#include <stdint.h>

/**VarBase class.
 * An abstract container for data.
 */
class VarBase
{
public:
	/**VarBase constructor.
	 * @param name the name of the variable usually followed by and underscore and the type.
	 * @param data a void* to the data.
	 * @param size the size of the data's type.
	 */
	VarBase(std::string name, void* data, uint32_t size);

	/**VarBase deconstrutor.
	 * virtual deconstructor should be implemented by subclasses in order
	 * to properly destroy the data.
	 */
	virtual ~VarBase();

	/**Returns the name of the var
	 * @return the name of the var.
	 */
	std::string getName();

	/**Returns the data of the var.
	 * @return the data of the var.
	 */
	void* getData();

	/**Returns the size of the data's type.
	 * @return the size of the data's type.
	 */
	uint32_t getSize();

private:
	std::string m_name; ///< The name of the var.
	void* m_data; ///< The data of the var.
	uint32_t m_size; ///< The size of the data's type

};

#endif /* VARBASE_H_ */
