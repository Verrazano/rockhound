template <class T>
Var<T>::Var(std::string name, T val) :
    VarBase(name, new T(val), sizeof(T))
{
}

template <class T>
Var<T>::Var(std::string name, T* ptr) :
    VarBase(name, ptr, sizeof(T))
{
}

template <class T>
Var<T>::~Var()
{
    delete reinterpret_cast<T*>(getData());

}

template <class T>
void Var<T>::set(T val)
{
    (*reinterpret_cast<T*>(getData())) = val;

}

template <class T>
void Var<T>::set(T* ptr)
{
    delete reinterpret_cast<T*>(getData());
    m_data = ptr;

}

template <class T>
T Var<T>::get()
{
    return (*reinterpret_cast<T*>(getData()));

}

template <class T>
T* Var<T>::getPtr()
{
    return reinterpret_cast<T*>(getData());

}