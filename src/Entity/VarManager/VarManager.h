#ifndef VARMANAGER_H_
#define VARMANAGER_H_
/*
The MIT License (MIT)

Copyright (c) 2014 Harrison D. Miller

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

author: Harrison D. Miller
psuedonym: Verrazano
contact: hdmscratched@gmail.com
*/

#include <list>
#include <string>
#include <stdint.h>

#include "VarBase.h"
#include "Var.h"

/**VarManager class.
 * Stores data by a string identifier.
 */
class VarManager
{
public:
	VarManager();

	virtual ~VarManager();

	void setInt(std::string name, int32_t val);
	void setUInt(std::string name, uint32_t val);
	void setFloat(std::string name, float val);
	void setString(std::string name, std::string val);
	void setBool(std::string name, bool val);

	int32_t getInt(std::string name);
	uint32_t getUInt(std::string name);
	float getFloat(std::string name);
	std::string getString(std::string name);
	bool getBool(std::string name);

	bool exists(std::string name);

private:
	template <class T>
	void set(std::string name, T val);

	template <class T>
	T get(std::string name, T defaultValue);

	template <class T>
	T* getPtr(std::string name);

	void* getData(std::string name);

	VarBase* getVar(std::string name);
	std::list<VarBase*> m_vars;

};

#include "VarManager.inl"

#endif /* VARMANAGER_H_ */
