#include "VarManager.h"

VarManager::VarManager()
{
}

VarManager::~VarManager()
{
	while(!m_vars.empty())
	{
		delete (*m_vars.begin());
		m_vars.erase(m_vars.begin());

	}

}

void VarManager::setInt(std::string name, int32_t val)
{
	set<int32_t>(name+"_int", val);

}

void VarManager::setUInt(std::string name, uint32_t val)
{
	set<uint32_t>(name+"_uint", val);

}

void VarManager::setFloat(std::string name, float val)
{
	set<float>(name+"_float", val);

}

void VarManager::setString(std::string name, std::string val)
{
	set<std::string>(name+"_string", val);

}

void VarManager::setBool(std::string name, bool val)
{
	set<bool>(name+"_bool", val);

}

int VarManager::getInt(std::string name)
{
	return get<int32_t>(name+"_int", 0);

}

unsigned int VarManager::getUInt(std::string name)
{
	return get<uint32_t>(name+"_uint", 0);

}

float VarManager::getFloat(std::string name)
{
	return get<float>(name+"_float", 0.0f);

}

std::string VarManager::getString(std::string name)
{
	return get<std::string>(name+"_string", "");

}

bool VarManager::getBool(std::string name)
{
	return get<bool>(name+"_bool", false);

}

void* VarManager::getData(std::string name)
{
    if(exists(name))
    {
        VarBase* vb = getVar(name);
        return vb->getData();

    }
    else
    {
        return NULL;

    }

}

bool VarManager::exists(std::string name)
{
	return getVar(name) != NULL;

}

VarBase* VarManager::getVar(std::string name)
{
	std::list<VarBase*>::iterator it;
	for(it = m_vars.begin(); it != m_vars.end(); it++)
	{
		if((*it)->getName() == name)
		{
			return (*it);

		}

	}

	return NULL;

}
