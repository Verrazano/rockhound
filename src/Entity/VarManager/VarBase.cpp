#include "VarBase.h"

VarBase::VarBase(std::string name, void* data, uint32_t size)
{
	m_name = name;
	m_data = data;
	m_size = size;

}

VarBase::~VarBase()
{
}

std::string VarBase::getName()
{
	return m_name;

}

void* VarBase::getData()
{
	return m_data;

}

uint32_t VarBase::getSize()
{
	return m_size;

}
