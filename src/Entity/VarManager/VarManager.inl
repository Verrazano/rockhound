template <class T>
void VarManager::set(std::string name, T val)
{
    if(exists(name))
    {
        VarBase* vb = getVar(name);
        
        if(sizeof(T) != vb->getSize())
        {
            return;
        
        }
        
        Var<T>* v = reinterpret_cast<Var<T>* >(vb);
        v->set(val);
    
    }
    else
    {
        Var<T>* v = new Var<T>(name, val);
        m_vars.push_back(v);
    
    }

}

template <class T>
T VarManager::get(std::string name, T defaultValue)
{
    if(exists(name))
    {
        VarBase* vb = getVar(name);
        
        if(sizeof(T) != vb->getSize())
        {
            return defaultValue;
        
        }
        
        Var<T>* v = reinterpret_cast<Var<T>* >(vb);
        return v->get();
    
    }
    else
    {
        return defaultValue;
    
    }

}

template <class T>
T* VarManager::getPtr(std::string name)
{
    if(exists(name))
    {
        VarBase* vb = getVar(name);
        
        if(sizeof(T) != vb->getSize())
        {
            return NULL;
        
        }
        
        Var<T>* v = reinterpret_cast<Var<T>* >(vb);
        return v->getPtr();
    
    }
    else
    {
        return NULL;
    
    }
    
}