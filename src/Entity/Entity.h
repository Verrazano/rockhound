#ifndef ENTITY_H_
#define ENTITY_H_

#include "ID/ID.h"
#include "TagManager/TagManager.h"
#include "VarManager/VarManager.h"
#include "SceneActor.h"
#include <string>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

class Entity : public VarManager, public TagManager, public ID
{
public:
	Entity(float z, sf::Vector2f position, sf::Vector2f size, float mass = 1.0f, float maxVelocity = 25.0f, float drag = 0.93f);

	//every x frames update logic and sprites
	unsigned int updateRate;

	virtual ~Entity();

	virtual std::string getName() = 0;

	//logic for the entity goes here
	virtual void onTick() = 0;

	//logic for the animation stuff goes here (preferably)
	virtual void onDraw() = 0;

	//logic for what happens directly before the entity dies goes here
	virtual void onDeath() = 0;

	//check to see whether this entity collides with another entity or they pass through one another
	virtual bool doesCollide(Entity* other) = 0;

	//logic for what happens when this entity collides with another entity goes here
	virtual void onCollide(Entity* other) = 0;

	//physics stuff
	void updateBody();
	void setVelocity(sf::Vector2f vel);
	void addForce(sf::Vector2f force);
	void addForce(float angle, float force);
	void setVelocity(float angle, float vel);
	void setPosition(sf::Vector2f pos);

	sf::FloatRect getBody();

	//psuedo physics stuff
	sf::Vector2f velocity;
	float mass; //for calculating new velocity from addForce
	float maxVelocity; //self explan.
	float drag; //fake deacceleration
	sf::RectangleShape body; //not the sprite (is a sprite just for debug see drawBody)
	bool drawBody; //draw the body (outline)
	bool isStatic; //don't check in collision model (on it's side)
	float rotVel;
	float rotVelDrag;

	void setAngle(float angle);
	float getAngle();

	void updateSprites();
	sf::Vector2f getSpritePos();

	float distanceTo(Entity* other);
	float distanceTo(sf::Vector2f point);

	bool isDead();

	SceneActor sprite;

	static bool compare(Entity* lhs, Entity* rhs);
	float z;
	float health;
	float maxHealth;

};

//TODO: zorder rendering maybe? probably not we wont keep all the entities in one place just most of them

#endif /* ENTITY_H_ */
