#include "EntityManager.h"
#include <algorithm>
#include "../../Engine/Engine.h"
#include <iostream>

EntityManager::EntityManager()
{
	suspend = false;

}

EntityManager::~EntityManager()
{
	clear();

}

void EntityManager::clear()
{
	while(!entities.empty())
	{
		delete (*entities.begin());
		entities.erase(entities.begin());

	}

}

//use this with new (of derivied class)
std::string EntityManager::addEntity(Entity* e)
{
	if(e == NULL)
		return "";
	Entity* t = getEntity(e->getIDString());
	if(t != NULL)
	{
		game->out("already has entity can't replace with: " + e->getName() + "\n");
		return t->getIDString();

	}

	entities.push_back(e);
	//sort based on z order
	std::stable_sort(entities.begin(), entities.end(), Entity::compare);
	return e->getIDString();

}

bool EntityManager::hasEntity(Entity* e)
{
	if(e == NULL)
		return false;
	return hasEntity(e->getIDString());

}

bool EntityManager::hasEntity(std::string id)
{
	return getEntity(id) != NULL;

}

void EntityManager::remEntity(Entity* e)
{
	if(e == NULL)
		return;
	remEntity(e->getIDString());

}

void EntityManager::remEntity(std::string id)
{
	//again check if id is valid TODO check every where that I take an id

	std::vector<Entity*>::iterator it;
	for(it = entities.begin(); it != entities.end(); it++)
	{
		if((*it)->getIDString() == id)
		{
			delete (*it);
			entities.erase(it);
			return;

		}

	}

}

Entity* EntityManager::getEntity(std::string id)
{
	//TODO: check if the id is even valid (eg right length, just some optimization)
	std::vector<Entity*>::iterator it;
	for(it = entities.begin(); it != entities.end(); it++)
	{
		if((*it)->getIDString() == id)
		{
			return (*it);

		}

	}

	return NULL;

}

std::vector<Entity*> EntityManager::getEntitiesWithTag(std::string tag)
{
	std::vector<Entity*> ret;
	std::vector<Entity*>::iterator it;
	for(it = entities.begin(); it != entities.end(); it++)
	{
		if((*it)->hasTag(tag))
		{
			ret.push_back((*it));

		}

	}

	return ret;

}

std::vector<Entity*> EntityManager::getEntitiesWithName(std::string name)
{
	std::vector<Entity*> ret;
	std::vector<Entity*>::iterator it;
	for(it = entities.begin(); it != entities.end(); it++)
	{
		if((*it)->getName() == name)
		{
			ret.push_back((*it));

		}

	}

	return ret;

}

std::vector<Entity*> EntityManager::getEntitiesWithinDist(float dist, Entity* from)
{
	std::vector<Entity*> ret;
	if(from == NULL)
		return ret;
	std::vector<Entity*>::iterator it;
	for(it = entities.begin(); it != entities.end(); it++)
	{
		if((*it) == from)
			continue;

		if(from->distanceTo((*it)) <= dist)
		{
			ret.push_back((*it));

		}

	}

	return ret;

}

std::vector<Entity*> EntityManager::getEntitiesWithTagInDist(float dist, std::string tag, Entity* from)
{
	std::vector<Entity*> ret;
	if(from == NULL)
		return ret;
	std::vector<Entity*>::iterator it;
	for(it = entities.begin(); it != entities.end(); it++)
	{
		if((*it)->hasTag(tag) && from->distanceTo((*it)) <= dist)
		{
			ret.push_back((*it));

		}

	}

	return ret;

}

std::vector<Entity*> EntityManager::getEntitiesWithNameInDist(float dist, std::string name, Entity* from)
{
	std::vector<Entity*> ret;
	if(from == NULL)
		return ret;
	std::vector<Entity*>::iterator it;
	for(it = entities.begin(); it != entities.end(); it++)
	{
		if((*it)->getName() == name && from->distanceTo((*it)) <= dist)
		{
			ret.push_back((*it));

		}

	}

	return ret;

}

Entity* EntityManager::giveEntity(std::string id)
{
	std::vector<Entity*>::iterator it;
	for(it = entities.begin(); it != entities.end(); it++)
	{
		if((*it)->getIDString() == id)
		{
			Entity* entity = *it;
			entities.erase(it);
			return entity;

		}

	}

	return NULL;

}

std::string EntityManager::takeFrom(EntityManager* manager, std::string id)
{
	if(manager == NULL)
	{
		return "";

	}

	return addEntity(manager->giveEntity(id));

}

unsigned int EntityManager::numEntitiesWithName(std::string name)
{
	return getEntitiesWithName(name).size();

}

void EntityManager::handleCollisons()
{
	if(suspend)
		return;

	for(unsigned int i = 0; i < entities.size(); i++)
	{
		if(entities[i]->isStatic)
		{
			continue;

		}

		for(unsigned int j = 0; j < entities.size(); j++)
		{
			if(i == j)
			{
				continue;

			}

			sf::FloatRect intersect;
			if(entities[i]->getBody().intersects(entities[j]->getBody(), intersect))
			{
				if(entities[i]->doesCollide(entities[j]) && entities[j]->doesCollide(entities[i]))
				{
					entities[i]->onCollide(entities[j]);
					if(entities[j]->isStatic)
						entities[j]->onCollide(entities[i]);

					if(intersect.width > intersect.height)
						if(entities[i]->getSpritePos().y > intersect.top)
							entities[i]->body.move(0, intersect.height);
						else
							entities[i]->body.move(0, -intersect.height);
					else
						if(entities[i]->getSpritePos().x > intersect.left)
							entities[i]->body.move(intersect.width, 0);
						else
							entities[i]->body.move(-intersect.width, 0);

				}

			}

		}

	}

}

void EntityManager::setSuspend(bool state)
{
	suspend = state;

}

bool EntityManager::isSuspended()
{
	return suspend;

}
