#ifndef ENTITYMANAGER_H_
#define ENTITYMANAGER_H_

#include "../Entity.h"
#include <vector>
#include <string>

class EntityManager
{
public:
	EntityManager();

	//does take control of all entities
	//this class will delete entities hen you call rem or this class is destructed
	virtual ~EntityManager();

	void clear();

	//use this with new (of derivied class)
	//returns the id of the entity
	std::string addEntity(Entity* e);

	bool hasEntity(Entity* e);
	bool hasEntity(std::string id);

	void remEntity(Entity* e);
	void remEntity(std::string id);

	Entity* getEntity(std::string id);

	std::vector<Entity*> getEntitiesWithTag(std::string tag);
	std::vector<Entity*> getEntitiesWithName(std::string name);

	std::vector<Entity*> getEntitiesWithinDist(float dist, Entity* from);
	std::vector<Entity*> getEntitiesWithTagInDist(float dist, std::string tag, Entity* from);
	std::vector<Entity*> getEntitiesWithNameInDist(float dist, std::string name, Entity* from);

	std::string takeFrom(EntityManager* manager, std::string id);

	unsigned int numEntitiesWithName(std::string name);

	Entity* giveEntity(std::string id);

	void handleCollisons();

	void setSuspend(bool state);
	bool isSuspended();

	bool suspend;
	std::vector<Entity*> entities;

};

#endif /* ENTITYMANAGER_H_ */
