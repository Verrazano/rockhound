#include "Entity.h"
#include <math.h>
#include "../Engine/Engine.h"
#include <iostream>

Entity::Entity(float z, sf::Vector2f position, sf::Vector2f size, float mass,
		float maxVelocity, float drag) :
				sprite("sprite")
{
	this->z = z;
	this->mass = mass;
	this->maxVelocity = maxVelocity;
	this->drag = drag;

	body.setSize(size);
	body.setOrigin(size.x/2, size.y/2);
	body.setFillColor(sf::Color::Transparent);
	body.setOutlineColor(sf::Color::Red);
	body.setOutlineThickness(1.0f);
	body.setPosition(position);

	isStatic = false;
	drawBody = game->settings.debug;

	updateRate = 2;

	if(game->settings.debug)
		game->out("entity created with id: " + getIDString());

	rotVel = 0.0f;
	rotVelDrag = drag;
	maxHealth = 100.0f;
	health = maxHealth;

}

Entity::~Entity()
{
}

void Entity::updateBody()
{
	float mag = sqrt(pow(velocity.x, 2) + pow(velocity.y, 2));
	if(mag > maxVelocity)
	{
		velocity /= mag;
		velocity *= maxVelocity;

	}

	sf::Vector2f pos = body.getPosition();
	pos += velocity;
	body.setPosition(pos);
	velocity *= drag;
	body.rotate(rotVel);
	rotVel *= rotVelDrag;

	//std::cout << "velocity: " << velocity.x << ", " << velocity.y << "\n";

}

void Entity::setVelocity(sf::Vector2f vel)
{
	velocity = vel;

}

void Entity::addForce(sf::Vector2f force)
{
	sf::Vector2f nvel = force;
	//nvel *= mass;
	//not really how you do this but whatever
	//nvel.x = sqrt(fabs(nvel.x))*nvel.x/fabs(nvel.x);
	//nvel.y = sqrt(fabs(nvel.y))*nvel.y/fabs(nvel.y);
	velocity += nvel;

}

void Entity::addForce(float angle, float force)
{
	if(force == 0)
	{
		return;

	}
	angle = ((angle)*3.14159/180);
	addForce(sf::Vector2f(cos(angle)*force, sin(angle)*force));

}

void Entity::setVelocity(float angle, float vel)
{
	angle = ((angle)*3.14159/180);
	setVelocity(sf::Vector2f(cos(angle)*vel, sin(angle)*vel));

}

void Entity::setPosition(sf::Vector2f pos)
{
	body.setPosition(pos);

}

sf::FloatRect Entity::getBody()
{
	return body.getGlobalBounds();

}

void Entity::setAngle(float angle)
{
	body.setRotation(angle);

}

float Entity::getAngle()
{
	return body.getRotation();

}

void Entity::updateSprites()
{
	sprite.setPosition(getSpritePos());
	sprite.setRotation(getAngle());

}

sf::Vector2f Entity::getSpritePos()
{
	return body.getPosition();

}

float Entity::distanceTo(Entity* other)
{
	if(!other)
	{
		return 0.0f;

	}

	return distanceTo(other->getSpritePos());

}

float Entity::distanceTo(sf::Vector2f point)
{
	sf::Vector2f opos = point;
	sf::Vector2f pos = getSpritePos();
	pos = opos - pos;
	float dist = sqrt(pow(pos.x, 2) + pow(pos.y, 2));
	return dist;

}

bool Entity::compare(Entity* lhs, Entity* rhs)
{
	float lhsz = lhs->z;
	float rhsz = rhs->z;

	return lhsz < rhsz;
}


bool Entity::isDead()
{
	return health <= 0.0f;

}
