#ifndef SCENEACTOR_H_
#define SCENEACTOR_H_

#include <list>
#include <string>
#include <stdint.h>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/VertexArray.hpp>
#include <SFML/Graphics/Transformable.hpp>

//TODO: z positioning
// frame animation
// transform animation
// organize actors by id's rather than names?

class SceneActor : public sf::Drawable, public sf::Transformable
{
public:
	SceneActor(std::string name);
	SceneActor(std::string name, sf::Vector2f size, sf::Color color, sf::Vector2f pos = sf::Vector2f(0, 0));
	SceneActor(std::string name, sf::Vector2f size,
			sf::Texture* texture, uint32_t frame, sf::Vector2f frameSize, sf::Vector2f pos = sf::Vector2f(0, 0));
	SceneActor(std::string name, sf::Vector2f size,
			sf::Texture* texture, sf::IntRect frame, sf::Vector2f pos = sf::Vector2f(0, 0));
	SceneActor(std::string name, sf::Texture* texture, sf::IntRect frame, sf::Vector2f pos = sf::Vector2f(0, 0));
	SceneActor(std::string name, sf::Texture* texture, sf::Vector2f pos = sf::Vector2f(0, 0));

	void draw(sf::RenderTarget& target, sf::RenderStates states) const;

    void setPosition(float x, float y);
    void setPosition(sf::Vector2f position);

	void setName(std::string name);
	std::string getName();

	void setSize(float width, float height);
	void setSize(sf::Vector2f size);
	sf::Vector2f getSize();

	void setTexture(sf::Texture* texture);
	void setTextureRect(sf::IntRect frame);

	void setColor(sf::Color color);
	sf::Color getColor();

	void setFrameSize(float width, float height);
	void setFrameSize(sf::Vector2f size);
	sf::Vector2f getFrameSize();

	sf::IntRect getFrame(uint32_t frame);

	SceneActor* getParent();

	SceneActor* addChild(SceneActor sceneActor);
	bool hasChild(std::string name);
	SceneActor* getChild(std::string name);
	void remChild(std::string name);

private:
	std::string m_name;

	sf::Texture* m_texture;
	sf::Vector2f m_frameSize;

	sf::Color m_color;
	sf::VertexArray m_points;
	sf::Vector2f m_size;

	SceneActor* m_parent;
	std::list<SceneActor> m_children;

};

#endif /* SCENEACTOR_H_ */
