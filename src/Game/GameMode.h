#ifndef GAMEMODE_H_
#define GAMEMODE_H_

#include "../Entity/Entity.h"

class GameMode
{
public:
	GameMode()
	{
		started = false;
	}

	virtual ~GameMode()
	{
	}

	//return true on start game
	virtual bool onMenu() = 0;

	//on start of the game this can happen multiple times during the lifetime of the application
	//so it's good to use this instead of the constructor.
	virtual void onStart() = 0;

	//called before entities tick
	virtual void onTick() = 0;

	//called during the gui draw sequence
	virtual void onDraw() = 0;

	void load(std::string loadPhase, float loadPercent);
	virtual void onLoad() = 0;

	virtual void onEntityCreated(Entity* e) = 0;

	virtual void onEntityDestroyed(Entity* e) = 0;

	virtual bool isGameOver() = 0;

	std::string loadPhase;
	float loadPercent;
	bool started;

};

#endif /* GAMEMode_H_ */
