#include "Stargate.h"
#include "../../Engine/Engine.h"
#include <math.h>

Stargate::Stargate(sf::Vector2f pos, float a) : Entity(1.5, pos, sf::Vector2f(250, 25))
{
	sprite.addChild(SceneActor("gate", sf::Vector2f(250, 25), sf::Color::Cyan));
	sprite.addChild(SceneActor("side1", sf::Vector2f(50, 50), sf::Color::Black, sf::Vector2f(-150, 0)));
	sprite.addChild(SceneActor("side2", sf::Vector2f(50, 50), sf::Color::Black, sf::Vector2f(150, 0)));
	setAngle(a);
	isStatic = true;
	updateRate = 60;

}

std::string Stargate::getName()
{
	return "stargate";

}

void Stargate::onTick()
{
	emitBits(2);

}

void Stargate::onDraw()
{

}

void Stargate::onDeath()
{

}

bool Stargate::doesCollide(Entity* other)
{
	if(idlink != "" && other->hasTag("ship"))
	{
		return true;

	}

	return false;

}

void Stargate::onCollide(Entity* other)
{

	//add a cool down?
	if(idlink != "")
	{
		Entity* e = game->entities.getEntity(idlink);
		if(e != NULL && e->getName() == "stargate")
		{
			sf::Vector2f pos = e->getSpritePos();
			float a = (e->getAngle() - 90)*3.14159/180;

			pos.x += 200*cos(a);
			pos.y += 200*sin(a);

			other->setPosition(pos);
			other->setAngle(a);

			//pos.x *=2;
			//pos.y *=2;
			sf::Mouse::setPosition(sf::Vector2i(pos.x, pos.y), game->window);
			emitBits(50);
			dynamic_cast<Stargate*>(e)->emitBits(25);

		}

	}

}


void Stargate::setLink(std::string link)
{
	if(link != "")
	{
		Stargate* s = dynamic_cast<Stargate*>(game->entities.getEntity(link));
		if(s != NULL)
		{
			s->idlink = getIDString();
			idlink = link;

		}

	}

}

void Stargate::emitBits(unsigned int count)
{
	float power = 100;
	for(unsigned int i = 0; i < count; i++)
	{
		float angle = rand()%360;
		angle = angle*3.14159/180;
		sf::Vector2f pos = getSpritePos();
		pos.x += game->randFrom(-60, 60);
		pos.y += game->randFrom(-60, 60);
		game->particles.emit("blue bit", pos, sf::Vector2f(cos(angle)*power, sin(angle)*power), 8, 0.5);

	}

}
