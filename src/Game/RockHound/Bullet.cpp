#include "Bullet.h"
#include "../../Engine/Engine.h"
#include <math.h>

sf::Texture* Bullet::bulletText;

Bullet::Bullet(sf::Vector2f pos, float angle, float vel) :
	Entity(-1.5, pos, sf::Vector2f(32, 32))
{
	drag = 1.0f;
	updateRate = 5;

	static bool loaded = false;
	if(!loaded)
	{
		loaded = true;
		bulletText = game->files.get<sf::Texture>("rocket.png");

	}

	sprite.addChild(SceneActor("bullet", bulletText));
	setVelocity(angle, vel);


}

std::string Bullet::getName()
{
	return "bullet";

}

void Bullet::onTick()
{
	health -= 2;
	emitSmoke(2);

}

void Bullet::onDraw()
{

}

void Bullet::onDeath()
{

}

bool Bullet::doesCollide(Entity* other)
{
	if(other->hasTag("ship"))
	{
		return true;

	}

	return false;

}

void Bullet::onCollide(Entity* other)
{

}

void Bullet::emitSmoke(unsigned int count)
{
	float a = getAngle();
	float power = 24;
	for(unsigned int i = 0; i < count; i++)
	{
		float a1 = game->randFrom(-16, 16) + a - 180;
		a1 = a1*3.14159/180;
		sf::Vector2f pos = getSpritePos();
		pos.x += game->randFrom(-15, 15);
		pos.y += game->randFrom(-15, 15);
		game->particles.emit("smoke", pos, sf::Vector2f(cos(a1)*power, sin(a1)*power), 3);

	}

}
