#include "Pirate.h"
#include "../../Engine/Engine.h"
#include "Bullet.h"

sf::Texture* Pirate::pirateTexture;

Pirate::Pirate(sf::Vector2f pos) :
	Ship(0.9, pos, sf::Vector2f(266, 243), true, 0.007/2,  0.2/2, 2, 0.2, 1, 15)
{
	static bool loaded = false;
	if(!loaded)
	{
		loaded = true;
		pirateTexture = game->files.get<sf::Texture>("pirate.png");

	}

	sprite.addChild(SceneActor("pirate", pirateTexture))->setRotation(90);
	target = pos;
	canFollow = false;
	player = NULL;

}

std::string Pirate::getName()
{
	return "pirate";

}

void Pirate::onTick()
{
	if(player == NULL)
	{
		std::vector<Entity*> players = game->entities.getEntitiesWithTag("player");
		if(players.size() != 0)
		{
			Entity* e = players[0];
			if(e != NULL)
			{
				player = dynamic_cast<PlayerMiner*>(e);

			}

		}

	}
	else
	{
		float dist = distanceTo(player);
		if(dist == 0)
		{
			player = NULL;
			return;

		}

		if(dist < 800)
		{
			canFollow = true;

		}
		else
		{
			canFollow = false;

		}

		if(canFollow)
		{
			target = player->getSpritePos();
			if(dist < 800)
			{
				if(shootTimer.getElapsedTime().asSeconds() >= 0.6)
				{
					shootTimer.restart();
					game->addEntity(new Bullet(getSpritePos(), getAngle(), 200));

				}

			}

		}
		else
		{
			if(getBody().contains(target))
			{
				target = sf::Vector2f(rand()%(int)game->settings.worldSize, rand()%(int)game->settings.worldSize);

			}

		}

		updateShip();

	}

}

void Pirate::onDraw()
{

}

void Pirate::onDeath()
{

}

bool Pirate::doesCollide(Entity* other)
{
	if(other->getName() == "mine")
	{
		return true;
	}
	return false;

}

void Pirate::onCollide(Entity* other)
{

}

bool Pirate::isPressingThrottle()
{
	return true;

}

//0 no dir, -1 left, 1 right
int Pirate::isPressingDir()
{
	return 0;

}

float Pirate::getThrottle()
{
	return 1.0f;

}

sf::Vector2f Pirate::getTarget()
{
	return target;

}
