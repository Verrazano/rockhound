#include "PlayerMiner.h"
#include "../../Engine/Engine.h"
#include <math.h>
#include <iostream>
#include "BeaconBot.h"
#include "Bomb.h"

sf::SoundBuffer* PlayerMiner::chachingBuffer;

PlayerMiner::PlayerMiner(sf::Vector2f pos) :
	Miner(1, pos, true)
{
	static bool loaded = false;
	if(!loaded)
	{
		loaded = true;
		chachingBuffer = game->files.get<sf::SoundBuffer>("res/chaching.wav");

	}

	chaching.setBuffer(*chachingBuffer);
	chaching.setVolume(50.0f);

	addTag("player");
	updateRate = 1;
	credits = 20;
	//maxFuel = 500;
	//fuel = 500;
	//item = new BeaconBot(sf::Vector2f(0, 0));
	item = new Bomb(sf::Vector2f(0, 0));
	health = 100000;

}

void PlayerMiner::onTick()
{
	std::cout << "ticking\n";
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	{
		std::cout << "pos: " << getSpritePos().x << ", " << getSpritePos().y << "\n";
		if(item != NULL)
		{
			item->setPosition(getSpritePos());
			game->addEntity(item);
			item = NULL;

		}

	}

	if(sf::Mouse::isButtonPressed(sf::Mouse::Left) || game->inputs.isState("cRightTrigger", ButtonCallback::pressed))
	{
		setMining(true);

	}
	else
	{
		setMining(false);

	}

	Miner::onTick();

}

bool PlayerMiner::isPressingThrottle()
{
	if(game->inputs.isState("cConnected", ButtonCallback::pressed))
	{
		return game->inputs.isState("cRightTrigger", ButtonCallback::pressed);

	}

	return sf::Keyboard::isKeyPressed(sf::Keyboard::W) || sf::Keyboard::isKeyPressed(sf::Keyboard::Up);

}

int PlayerMiner::isPressingDir()
{
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		return -1;

	}
	else if(sf::Keyboard::isKeyPressed(sf::Keyboard::D) || sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		return 1;

	}

	return 0;

}

float PlayerMiner::getThrottle()
{
	if(game->inputs.isState("cConnected", ButtonCallback::pressed))
		return distToTarget();
	bool pressing = sf::Keyboard::isKeyPressed(sf::Keyboard::W) || sf::Keyboard::isKeyPressed(sf::Keyboard::Up);
	return pressing ? 1 : 0;

}

sf::Vector2f PlayerMiner::getTarget()
{
	if(game->inputs.isState("cConnected", ButtonCallback::pressed))
	{
		sf::Vector2f p;
		p.x = game->inputs.getValue("cLeftX");
		p.y = game->inputs.getValue("cLeftY");
		target = p;
		p += getSpritePos();
		return p;

	}

	target = game->getGUIMousePos();
	float width = game->guiWidth;
	float height = game->guiHeight;
	target.x -= width/2;
	target.y -= height/2;
	if(fabs(target.x) > width/2)
	{
		target.x  = (width/2)*target.x/fabs(target.x);

	}

	if(fabs(target.y) > height/2)
	{
		target.y  = (height/2)*target.y/fabs(target.y);

	}

	sf::Vector2f real = target;
	real += getSpritePos();
	return real;

}

float PlayerMiner::distToTarget()
{
	sf::Vector2f real = target;
	real += getSpritePos();
	sf::Vector2f dif = real - getSpritePos();
	float dist = sqrt(pow(dif.x, 2) + pow(dif.y, 2));
	return dist;

}

std::string PlayerMiner::getCreditsInfo()
{
	return "CREDITS:\n"+toString(credits);

}

void PlayerMiner::onDeath()
{
	game->out("WTF WHY AM I DYING\n");

}

std::string PlayerMiner::getHealthInfo()
{
	if(health == maxHealth)
	{
		return "Conditions Good";

	}
	else if(health >= maxHealth - maxHealth/4)
	{
		return "Damage Sustained";

	}
	else if(health >= maxHealth/2)
	{
		return "Critical Damage";

	}

	return "Hull Fine";

}

std::string PlayerMiner::getItemInfo()
{
	if(item == NULL)
	{
		return "No Item";

	}

	return "Item: " + item->getName();

}
