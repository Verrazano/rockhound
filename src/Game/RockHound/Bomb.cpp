#include "Bomb.h"
#include "../../Engine/Engine.h"
#include <math.h>
#include <iostream>

sf::Texture* Bomb::bombText;

Bomb::Bomb(sf::Vector2f pos) : Entity(0.5, pos, sf::Vector2f(50, 50))
{
	static bool loaded = false;
	if(!loaded)
	{
		loaded = true;

		bombText = game->files.get<sf::Texture>("bomb.png");

	}

	addTag("bomb");

	updateRate = 30;
	isStatic = true;
	drag = 0.98;

	sprite.addChild(SceneActor("bomb", bombText, sf::IntRect(0, 0, 83, 151)));

	health = 25;
	frame2 = false;

	bomb = Explosion(this, 500, 200, 150);
	fuse.restart();
	firsttick = false;

}

std::string Bomb::getName()
{
	return "bomb";

}

void Bomb::onTick()
{
	/*if(!firsttick)
	{
		firsttick = true;
		fuse.restart();
		return;

	}

	if(fuse.getElapsedTime().asSeconds() < 3)
	{
		if(animTime.getElapsedTime().asSeconds() > 0.3)
		{
			animTime.restart();
			if(!frame2)
			{
				frame2 = true;
				Sprite& s = getSprite("bomb");
				s.sprite.setTextureRect(sf::IntRect(83, 0, 107, 151));
				s.sprite.setSize(sf::Vector2f(107, 151));
				s.sprite.setOrigin(107/2, 151/2);

			}
			else
			{
				frame2 = false;
				Sprite& s = getSprite("bomb");
				s.sprite.setTextureRect(sf::IntRect(0, 0, 83, 151));
				s.sprite.setSize(sf::Vector2f(83, 151));
				s.sprite.setOrigin(83/2, 151/2);

			}

		}

	}
	else
	{
		if(animTime.getElapsedTime().asSeconds() > 0.3)
		{
			animTime.restart();
			if(!frame2)
			{
				frame2 = true;
				Sprite& s = getSprite("bomb");
				s.sprite.setTextureRect(sf::IntRect(269, 0, 107, 151));
				s.sprite.setSize(sf::Vector2f(107, 151));
				s.sprite.setOrigin(107/2, 151/2);

			}
			else
			{
				frame2 = false;
				Sprite& s = getSprite("bomb");
				s.sprite.setTextureRect(sf::IntRect(190, 0, 79, 151));
				s.sprite.setSize(sf::Vector2f(79, 151));
				s.sprite.setOrigin(79/2, 151/2);

			}

		}

		if(fuse.getElapsedTime().asSeconds() > 6)
		{
			if(health > 0)
			{
				std::cout << "explodered\n";
				health = -1;
				bomb.explode();

			}

		}


	}*/
	//TODO all this crap

}

void Bomb::onDraw()
{
//do nothing
}

void Bomb::onDeath()
{
	//explode into smoke
	//play explody sounds

	unsigned int count = 200;

	float power = 50;
	for(unsigned int i = 0; i < count; i++)
	{
		float angle = rand()%360;
		angle = angle*3.14159/180;
		sf::Vector2f pos = getSpritePos();
		pos.x += game->randFrom(-60, 60);
		pos.y += game->randFrom(-60, 60);
		game->particles.emit("smoke", pos, sf::Vector2f(cos(angle)*power, sin(angle)*power), 8, 0.5);

	}


}

bool Bomb::doesCollide(Entity* other)
{
	if(other->hasTag("ship"))
	{
		return true;

	}

	return false;

}

void Bomb::onCollide(Entity* other)
{

}
