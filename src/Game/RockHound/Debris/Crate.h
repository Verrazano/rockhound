#ifndef CRATE_H_
#define CRATE_H_

#include "Debri.h"

class Crate : public Debri
{
public:
	Crate(sf::Vector2f pos, float rot);

	std::string getName();

	void onTick();

	void onDraw();

	void onDeath();

	static sf::Texture* crateText;

};

#endif /* CRATE_H_ */
