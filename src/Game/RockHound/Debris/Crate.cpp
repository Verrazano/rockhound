#include "Crate.h"
#include "../../../Engine/Engine.h"

sf::Texture* Crate::crateText;

Crate::Crate(sf::Vector2f pos, float rot) : Debri(0.2, pos, sf::Vector2f(66, 66))
{
	static bool loaded = false;
	if(!loaded)
	{
		loaded = true;

		crateText = game->files.get<sf::Texture>("crate.png");

	}

	sprite.addChild(SceneActor("crate", crateText));
	setAngle(rot);

}

std::string Crate::getName()
{
	return "crate";

}

void Crate::onTick()
{

}

void Crate::onDraw()
{

}

void Crate::onDeath()
{

}
