#ifndef DEBRI_H_
#define DEBRI_H_

#include "../../../Entity/Entity.h"

class Debri : public Entity
{
public:
	Debri(float z, sf::Vector2f pos, sf::Vector2f size);

	virtual std::string getName() = 0;

	virtual void onTick() = 0;

	virtual void onDraw() = 0;

	virtual void onDeath() = 0;

	//call these in your own doesCollide/onCollide if you override them
	virtual bool doesCollide(Entity* other);

	virtual void onCollide(Entity* other);

};

#endif /* DEBRI_H_ */
