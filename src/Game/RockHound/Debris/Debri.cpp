#include "Debri.h"
#include <math.h>
#include <iostream>

Debri::Debri(float z, sf::Vector2f pos, sf::Vector2f size) : Entity(z, pos, size)
{
	addTag("debri");
	updateRate = 300;
	isStatic = true;

}

//call these in your own doesCollide/onCollide if you override them
bool Debri::doesCollide(Entity* other)
{
	if(other->hasTag("ship"))
	{
		return true;

	}

	return false;

}

void Debri::onCollide(Entity* other)
{
	//inherit velocity
	sf::Vector2f vel = other->velocity;
	vel.x *= 1.2;
	vel.y *= 1.2;
	velocity += vel;
	int rot = (rand()%30)-15;
	//addForce(rot, 50);
	rotVel += rot;

}
