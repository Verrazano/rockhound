#ifndef BULLET_H_
#define BULLET_H_

#include "../../Entity/Entity.h"

class Bullet : public Entity
{
public:
	Bullet(sf::Vector2f pos, float angle, float vel);

	std::string getName();

	void onTick();

	void onDraw();

	void onDeath();

	bool doesCollide(Entity* other);

	void onCollide(Entity* other);

	static sf::Texture* bulletText;

	void emitSmoke(unsigned int count);

};


#endif /* BULLET_H_ */
