#ifndef BOMB_H_
#define BOMB_H_

#include "../../Entity/Entity.h"
#include "Explosion.h"

class Bomb : public Entity
{
public:
	Bomb(sf::Vector2f pos);

	std::string getName();

	void onTick();

	void onDraw();

	void onDeath();

	bool doesCollide(Entity* other);

	void onCollide(Entity* other);

	static sf::Texture* bombText;
	sf::Clock animTime;
	sf::Clock fuse;
	Explosion bomb;
	bool frame2;
	bool firsttick;

};

#endif /* BOMB_H_ */
