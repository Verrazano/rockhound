#ifndef MINE_H_
#define MINE_H_

#include "../../Entity/Entity.h"
#include "Explosion.h"

class Mine : public Entity
{
public:
	Mine(sf::Vector2f pos);

	std::string getName();

	void onTick();

	void onDraw();

	void onDeath();

	bool doesCollide(Entity* other);

	void onCollide(Entity* other);

	static sf::Texture* mineText;
	float m_rot;
	std::string targetID;
	float range;
	sf::Clock animTime;
	Explosion bomb;
	bool frame2;

};

#endif /* MINE_H_ */
