#include "Wall.h"

Wall::Wall(float x, float y, float width, float height) :
	Entity(-1000, sf::Vector2f(x*100, y*100), sf::Vector2f(width*100, height*100))
{
	addTag("wall");
	isStatic = true;
	//novel idea why not check for collision only when an object is going to tick
	updateRate = 10000000;

}

std::string Wall::getName()
{
	return "wall";

}

void Wall::onDraw()
{

}

void Wall::onDeath(){}

bool Wall::doesCollide(Entity* other){return false;}

void Wall::onCollide(Entity* other){}
