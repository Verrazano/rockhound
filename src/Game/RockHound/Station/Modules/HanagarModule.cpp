#include "HangarModule.h"
#include "../Station.h"
#include <SFML/Graphics/Color.hpp>

HangarModule::HangarModule(Station* station, int x, int y) :
	StationModule(station, x, y, 5, 5)
{
	//load 500x500 image of the hangar
	//create walls and cool slidy doors

	sprite.addChild(SceneActor("sprite", sf::Vector2f(5*100, 5*100), sf::Color::Green));


}

std::string HangarModule::getName()
{
	return "hangar";

}

void HangarModule::onTick()
{
	//derp don't really need to do nothing to really animate

}

void HangarModule::onDraw()
{

}
