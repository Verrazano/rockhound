#ifndef HANGARMODULE_H_
#define HANGARMODULE_H_

#include "../StationModule.h"

class Station;

class HangarModule : public StationModule
{
public:
	HangarModule(Station* station, int x, int y);

	std::string getName();

	void onTick();

	void onDraw();

};

#endif /* HANGARMODULE_H_ */
