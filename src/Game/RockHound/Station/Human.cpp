#include "Human.h"

Human::Human(sf::Vector2f pos) :
	Entity(10, pos, sf::Vector2f(50, 50))
{
	speed = 10;

}

bool Human::doesCollide(Entity* other)
{
	if(other->hasTag("wall") || other->getName() == "wall")
		return true;

	return false;

}

void Human::onCollide(Entity* other)
{

}

void Human::setSpeed(float speed)
{
	this->speed = speed;

}

