#ifndef STATION_H_
#define STATION_H_

/*Station gameplay is almost like it's own mini game within rockhound
 * it'll will be essentially unrelated to the rest of the game
 * station gameplay will almost play like a top down adventure or rpg game.
 * you will be on a station as your player character there you can buy parts and gear
 * for your ship talk to npcs find out info get jobs specific to the station
 * and even maybe do some jobs within the station it's self.
 *
 * The station does serve a good purpose though it splits the menu for buying items/ships/parts
 * into the station it's self this way the overworld(space) station is left with 3 buttons
 * sell crystal, refuel, dock. Instead of sell crystal, refuel, buy upgrades > upgrade x billion
 * instead you can go inside the station effectively pausing the game and relieving stress of having to deal
 * with diminishing gas pirates or other things and look around and compare for the items you want.
 *
 * The station class will be an  abstract object that allows for modular stations to be built and put together
 * with their own unique shops, npcs and behaviour such as locked doors that can only be completed by talking to
 * an npc and completing a mission.
 *
 * for system to work well a station must essentially act like it's own gamemode object everything in the overworld
 * can't be ticked only the things in the station things inside the station must be drawn and ticked.
 */

/*I need to think of a way to suspend the gameplay in the RockHound game or other but only temporarily*/

//alright there is now a way to swap the currently active entities so I can make the engine use a different entitymanager

#include "../../../Entity/EntityManager/EntityManager.h"

class Station
{
public:
	Station();

	//handles updating specific station logic like locked doors and stuff maybe
	//leaving from the station
	virtual void onTick();

	//draws all the modules
	virtual void onDraw();

	//sets where the player character comes into and leaves the station
	void setSpawn(float x, float y);

	//stations will have modules that can be added to the station, modules are rooms on the station they are in a grid system.
	void addModule(int x, int y, StationModule* mod);

	//addwall makes a wall entity
	void addWall(int x, int y, float width, float height);

	//set this to the activeEntityManager of the engine when we are inside of the station
	EntityManager entities;

};

#endif /* STATION_H_ */
