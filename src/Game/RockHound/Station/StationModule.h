#ifndef STATIONMODULE_H_
#define STATIONMODULE_H_

#include "../../../Entity/Entity.h"

class Station;

class StationModule : public Entity
{
public:
	StationModule(Station* station,
			int x, int y, int width, int height);

	virtual std::string getName() = 0;

	virtual void onTick() = 0;

	virtual void onDraw() = 0;

	void onDeath();

	bool doesCollide(Entity* other);
	void onCollide(Entity* other);

};

#endif /* STATIONMODULE_H_ */
