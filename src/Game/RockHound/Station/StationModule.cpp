#include "StationModule.h"
#include "Station.h"

StationModule::StationModule(Station* station,
		int x, int y, int width, int height) :
		Entity(-100, sf::Vector2f(x*100, y*100),
				sf::Vector2f(width*100, height*100))
{
	addTag("module");
	updateRate = 300;
	isStatic = true;

}

void StationModule::onDeath()
{
	//do nothing on death

}

bool StationModule::doesCollide(Entity* other)
{
	return false;
}

void StationModule::onCollide(Entity* other)
{
	//do nothing lol

}
