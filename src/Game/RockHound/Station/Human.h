#ifndef HUMAN_H_
#define HUMAN_H_

#include "../../../Entity/Entity.h"

class Human : public Entity
{
public:
	Human(sf::Vector2f pos);

	virtual std::string getName() = 0;

	virtual void onTick() = 0;

	virtual void onDraw() = 0;

	virtual void onDeath() = 0;

	bool doesCollide(Entity* other);

	void onCollide(Entity* other);

	void setSpeed(float speed);

	float speed;

};

#endif /* HUMAN_H_ */
