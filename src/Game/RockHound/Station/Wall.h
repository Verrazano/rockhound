#ifndef WALL_H_
#define WALL_H_

#include "../../../Entity/Entity.h"

class Wall : public Entity
{
public:
	Wall(float x, float y, float width, float height);

	std::string getName();

	void onDraw();

	void onDeath();

	bool doesCollide(Entity* other);

	void onCollide(Entity* other);

};

#endif /* WALL_H_ */
