#include "Sector.h"
#include "../../../Engine/Engine.h"
#include "../../../Util/convert/convertstring.h"
#include <iostream>

Sector::Sector(sf::Vector2f pos, unsigned int id, float radius) :
	Entity(10, pos, sf::Vector2f(10, 10))
{
	this->radius = radius;
	this->id = id;
	isStatic = true;

	if(game->settings.debug)
	{
		sprite.addChild(SceneActor("vert", sf::Vector2f(5, 25), sf::Color::Red));
		sprite.addChild(SceneActor("horz", sf::Vector2f(25, 5), sf::Color::Red));

	}

	updateRate = 1000000000;

	if(game->settings.debug)
	{
		text.setString("sector " + toString(id) + "\n" + toString(pos.x) + ", " + toString(pos.y));
		text.setPosition(pos.x - 30, pos.y - 100);
		text.setColor(sf::Color::Red);
		text.setFont(game->font);

	}

}

std::string Sector::getName()
{
	return "sector";

}

void Sector::onTick()
{
}

void Sector::onDraw()
{
	game->window.draw(text);

}

void Sector::onDeath()
{
}

bool Sector::doesCollide(Entity* other)
{
	return false;

}

void Sector::onCollide(Entity* other)
{
}

void Sector::setBiomeName(std::string biome)
{
	sf::Vector2f pos = getSpritePos();
	biomeName = biome;
	text.setString("sector\n" + toString(pos.x) + ", " + toString(pos.y) + "\n"+biomeName);

}
