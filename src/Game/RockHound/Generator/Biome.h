#ifndef BIOME_H_
#define BIOME_H_

#include "Sector.h"
#include <string>

class Biome
{
public:
	Biome(){}

	virtual ~Biome(){}

	virtual std::string getName() = 0;
	virtual unsigned int getWeight() = 0;

	//TODO: use delauny triangulation to find the radius
	//tag everything with this biome name and sector id
	virtual bool generate(Sector* sector, float radius) = 0;
	float rarity;

	static bool compare(Biome* lhs, Biome* rhs)
	{
		return lhs->rarity < rhs->rarity;

	}

};

#endif /* BIOME_H_ */
