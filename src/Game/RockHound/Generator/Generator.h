#ifndef GENERATOR_H_
#define GENERATOR_H_

#include "Biome.h"
#include <vector>

class Generator
{
public:
	Generator();

	virtual ~Generator();

	void addBiome(Biome* biome);

	void generate(unsigned int worldSize, unsigned int seed);

	unsigned int worldSize;

	//general sectors
	unsigned int m_sectors;
	float minSectorDist;
	float maxSectorDist;

private:
	unsigned int m_totalWeight;
	std::vector<Biome*> m_biomes;

};

#endif /* GENERATOR_H_ */
