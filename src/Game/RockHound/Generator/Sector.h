#ifndef SECTOR_H_
#define SECTOR_H_

#include "../../../Entity/Entity.h"

class Sector : public Entity
{
public:
	Sector(sf::Vector2f pos, unsigned int id, float radius = 700);

	std::string getName();

	void onTick();

	void onDraw();

	void onDeath();

	bool doesCollide(Entity* other);

	void onCollide(Entity* other);

	void setBiomeName(std::string biome);

	sf::Text text;
	std::string biomeName;
	unsigned int id;
	float radius;

};

#endif /* SECTOR_H_ */
