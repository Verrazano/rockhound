#include "Generator.h"
#include "../../../Engine/Engine.h"
#include <algorithm>
#include <iostream>
#include <math.h>

Generator::Generator()
{

}

Generator::~Generator()
{
	while(!m_biomes.empty())
	{
		Biome* biome = *m_biomes.begin();
		delete biome;

		m_biomes.erase(m_biomes.begin());

	}

}

void Generator::addBiome(Biome* biome)
{
	m_biomes.push_back(biome);

}

void Generator::generate(unsigned int worldSize, unsigned int seed)
{
	game->gameMode->load("determining the cosmos", 0);
	if(m_biomes.size() == 0)
		return;

	//std::cout << "biomes: " << m_biomes.size() << "\n";

	srand(seed);

	//make all the settings
	this->worldSize = worldSize;

	float worldRatio = 10000.0f/worldSize;
	m_sectors = worldSize/(worldRatio*30);
	//m_sectors = 333;
	//TODO: change it so the worldsize determines the number of sectors
	//not distance between sectors

	/*std::cout << "world ratio: " << worldRatio << "\n";
	std::cout << "Sectors: " << m_sectors << "\n";*/

	float maxLoad = m_sectors*2;
	float curLoad = 0.0f;

	//float sectorRatio = 30.03/((float)worldSize/((float)worldSize/(worldRatio*30)));//(float)worldSize/(float)m_sectors;
	float sectorRatio = 30.03;

	//std::cout << "sector ratio: " << sectorRatio << "\n";

	//make it so the number of sectors determines a radius which determines the distance between sectors
	minSectorDist = sectorRatio*25;
	maxSectorDist = sectorRatio*38;
	//std::cout << "min: " << minSectorDist << " max: " << maxSectorDist << "\n";
	//sf::sleep(sf::seconds(5));

	//generate randomly place sectors
	std::vector<Entity*> sectors;

	while(sectors.size() != m_sectors)
	{
		sf::Vector2f pos;
		pos.x = game->randFrom(-worldSize, worldSize);
		pos.y = game->randFrom(-worldSize, worldSize);

		unsigned int touching = 0;
		float radius = maxSectorDist + 10;

		for(unsigned int u = 0; u < sectors.size(); u++)
		{
			float dist = sectors[u]->distanceTo(pos);
			if(dist >= maxSectorDist)
			{
				//std::cout << "not in range\n";
				continue;

			}

			if(dist <= minSectorDist)
			{

				touching = 0;
				break;

			}

			if(dist < radius)
			{
				radius = dist;

			}

			touching++;

		}

		if(sectors.size() != 0)
		{
			if(touching != 1)
			{
				//std::cout << "touching to many or to few\n";
				continue;

			}

		}

		/*std::cout  << "pos: " << pos.x << ", " << pos.y << "\n";
		std::cout << "sectors: " << sectors.size() << "\n";*/
		game->addEntity(new Sector(pos, sectors.size(), radius));
		sectors = game->entities.getEntitiesWithName("sector");

		curLoad++;
		game->gameMode->load("marking the world", curLoad/maxLoad);

	}

	//total weight
	m_totalWeight = 0;
	for(unsigned int i = 0; i < m_biomes.size(); i++)
	{
		std::cout << "weight: " << m_biomes[i]->getWeight();
		m_totalWeight += m_biomes[i]->getWeight();

	}

	//std::cout << "totalWeight: " << m_totalWeight << "\n";

	//assign rarity
	for(unsigned int i = 0; i < m_biomes.size(); i++)
	{
		m_biomes[i]->rarity = (float)m_biomes[i]->getWeight()/(float)m_totalWeight;

	}

	std::stable_sort(m_biomes.begin(), m_biomes.end(), Biome::compare);
	/*for(unsigned int i = 0; i < m_biomes.size(); i++)
	{
		std::cout << "rarity for " << m_biomes[i]->getName() << " " << m_biomes[i]->rarity << "\n";

	}*/

	for(unsigned int i = 0; i < sectors.size(); i++)
	{
		float rarity = rand()%100;
		rarity /= 100;

		Sector* sector = dynamic_cast<Sector*>(sectors[i]);

		float curRarity = 0;
		for(unsigned int u = 0; u < m_biomes.size(); u++)
		{
			Biome* biome = m_biomes[u];
			curRarity += biome->rarity;
			if(curRarity > rarity)
			{

				curLoad++;
				game->gameMode->load("generating biomes", curLoad/maxLoad);
				sector->setBiomeName(m_biomes[u]->getName());
				m_biomes[u]->generate(sector, sector->radius);
				break;

			}

		}

	}

}

