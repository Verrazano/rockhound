#ifndef CRYSTAL_H_
#define CRYSTAL_H_

#include "../../../Entity/Entity.h"
#include "CrystalInfo.h"

class Crystal : public Entity
{
public:
	Crystal(CrystalInfo::Color color, CrystalInfo::Type type,
			sf::Vector2f pos, float vel, float angle, float rot);

	//returns the color + crystal, i.e: blue crystal.
	std::string getName();

	//slowly erodes health until the crystal dies
	//fade crystal spirte and emit particles
	void onTick();

	void onDraw();

	//explode into lots of particles
	//and make a shattering noise if not picked up
	void onDeath();

	//can only collide if this has settled
	bool doesCollide(Entity* other);

	void onCollide(Entity* other);

	CrystalInfo::Color getColor();
	CrystalInfo::Type getType();

	void emitBits(unsigned int count);

	static sf::IntRect getFrame(CrystalInfo::Color color, CrystalInfo::Type type);

	static sf::Texture* blueCrystal; //tier 1
	static sf::Texture* greenCrystal; //tier 2
	static sf::Texture* redCrystal; //tier 3
	static sf::Texture* goldCrystal; //tier 4

private:
	void setupSprite();

	CrystalInfo::Color m_color;
	CrystalInfo::Type m_type;
	float m_rot;

	static void loadResources();

};

#endif /* CRYSTAL_H_ */
