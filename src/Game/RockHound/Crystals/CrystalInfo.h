#ifndef CRYSTALINFO_H_
#define CRYSTALINFO_H_

namespace CrystalInfo
{

typedef enum Color
{
	Blue = 0,
	Green,
	Red,
	Gold

} Color;

typedef enum Type
{
	ButtonIcon = 0,
	GUIIcon,
	RockSmall,
	RockLarge,
	Full,
	Bit

} Type;

} //namespace CrystalInfo

#endif /* CRYSTALINFO_H_ */
