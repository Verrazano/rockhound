#include "Crystal.h"
#include "../../../Engine/Engine.h"
#include <math.h>
#include <iostream>

sf::Texture* Crystal::blueCrystal;
sf::Texture* Crystal::greenCrystal;
sf::Texture* Crystal::redCrystal;
sf::Texture* Crystal::goldCrystal;

Crystal::Crystal(CrystalInfo::Color color, CrystalInfo::Type type,
		sf::Vector2f pos, float vel, float angle, float rot) :
	Entity(-0.5, pos, sf::Vector2f(171*0.4, 210*0.4))
{
	loadResources();

	//handle bad types
	if(type != CrystalInfo::RockSmall && type != CrystalInfo::RockLarge && type != CrystalInfo::Full)
	{
		type = CrystalInfo::RockSmall;

	}

	//settings
	addTag("crystal");
	addTag("shatter");

	updateRate = 30;
	isStatic = true;

	rotVelDrag = 1.0f;
	drag = 0.97f;

	setVelocity(angle, vel);

	maxHealth = game->randFrom(100, 200);
	health = maxHealth;

	//members
	m_color = color;
	m_type = type;
	m_rot = rot;
	rotVel = m_rot;

	setupSprite();

}

std::string Crystal::getName()
{
	CrystalInfo::Color color = getColor();

	std::string crystal;
	if(color == CrystalInfo::Blue)
	{
		crystal = "blue crystal";

	}
	else if(color == CrystalInfo::Green)
	{
		crystal = "green crystal";

	}
	else if(color == CrystalInfo::Red)
	{
		crystal = "red crystal";

	}
	else if(color == CrystalInfo::Gold)
	{
		crystal = "gold crystal";

	}

	return crystal;

}

void Crystal::onTick()
{
	//erode health
	health -= 2.0f;

	//fade the crystal away
	SceneActor* s = sprite.getChild("crystal");
	float ratio = (health + health/4) / maxHealth;
	if(ratio > 1)
	{
		ratio = 1;

	}

	s->setColor(sf::Color(255, 255, 255, ratio*255));

	//emit crystal bit particles
	emitBits(1);

}

void Crystal::onDraw()
{
}

//explode into crystal particles and make a shattering noise?
void Crystal::onDeath()
{
	if(hasTag("shatter"))
	{
		//TODO: play random shatter sound

	}

	//explode into particle bits
	emitBits(20);

}

bool Crystal::doesCollide(Entity* other)
{
	//If this has settled you can collide with it (pick it up)
	if(fabs(velocity.x) <= 2.0f && fabs(velocity.y) <= 2.0f)
	{
		return true;

	}

	return false;

}

void Crystal::onCollide(Entity* other)
{
}

CrystalInfo::Color Crystal::getColor()
{
	return m_color;

}

CrystalInfo::Type Crystal::getType()
{
	return m_type;

}

void Crystal::emitBits(unsigned int count)
{
	CrystalInfo::Color color = getColor();

	std::string bit;
	if(color == CrystalInfo::Blue)
	{
		bit = "blue bit";

	}
	else if(color == CrystalInfo::Green)
	{
		bit = "green bit";

	}
	else if(color == CrystalInfo::Red)
	{
		bit = "red bit";

	}
	else if(color == CrystalInfo::Gold)
	{
		bit = "gold bit";

	}
	else
	{
		std::cout << "bad crap\n";

	}

	float power = 100;
	for(unsigned int i = 0; i < count; i++)
	{
		float angle = rand()%360;
		angle = angle*3.14159/180;
		sf::Vector2f pos = getSpritePos();
		pos.x += game->randFrom(-60, 60);
		pos.y += game->randFrom(-60, 60);
		game->particles.emit(bit, pos, sf::Vector2f(cos(angle)*power, sin(angle)*power), 8, 0.5);

	}

}

sf::IntRect Crystal::getFrame(CrystalInfo::Color color, CrystalInfo::Type type)
{
	//Hard coded value for all the crystal frames because the sprite sheets are really messy (but whatever it works)
	//This is probably the best way to do this as a result of the sprite sheet mess.
	if(color == CrystalInfo::Blue)
	{
		if(type == CrystalInfo::ButtonIcon)
		{
			return sf::IntRect(0, 42, 43, 115);

		}
		else if(type == CrystalInfo::GUIIcon)
		{
			return sf::IntRect(55, 43, 107, 125);

		}
		else if(type == CrystalInfo::RockSmall)
		{
			return sf::IntRect(189, 18, 107, 170);

		}
		else if(type == CrystalInfo::RockLarge)
		{
			return sf::IntRect(304, 20, 138, 170);

		}
		else if(type == CrystalInfo::Full)
		{
			return sf::IntRect(446, 0, 181, 225);

		}
		else if(type == CrystalInfo::Bit)
		{
			return sf::IntRect(662, 73, 45, 56);

		}

	}
	else if(color == CrystalInfo::Green)
	{
		if(type == CrystalInfo::ButtonIcon)
		{
			return sf::IntRect(0, 57, 45, 120);

		}
		else if(type == CrystalInfo::GUIIcon)
		{
			return sf::IntRect(74, 65, 111, 120);

		}
		else if(type == CrystalInfo::RockSmall)
		{
			return sf::IntRect(190, 49, 107, 170);

		}
		else if(type == CrystalInfo::RockLarge)
		{
			return sf::IntRect(345, 54, 105, 122);

		}
		else if(type == CrystalInfo::Full)
		{
			return sf::IntRect(498, 0, 143, 207);

		}
		else if(type == CrystalInfo::Bit)
		{
			return sf::IntRect(667, 65, 35, 44);

		}

	}
	else if(color == CrystalInfo::Red)
	{
		if(type == CrystalInfo::ButtonIcon)
		{
			return sf::IntRect(0, 45, 56, 124);

		}
		else if(type == CrystalInfo::GUIIcon)
		{
			return sf::IntRect(74, 47, 93, 125);

		}
		else if(type == CrystalInfo::RockSmall)
		{
			return sf::IntRect(198, 4, 100, 159);

		}
		else if(type == CrystalInfo::RockLarge)
		{
			return sf::IntRect(338, 0, 115, 170);

		}
		else if(type == CrystalInfo::Full)
		{
			return sf::IntRect(462, 0, 190, 182);

		}
		else if(type == CrystalInfo::Bit)
		{
			return sf::IntRect(672, 85, 42, 50);

		}

	}
	else if(color == CrystalInfo::Gold)
	{
		if(type == CrystalInfo::ButtonIcon)
		{
			return sf::IntRect(0, 31, 42, 132);

		}
		else if(type == CrystalInfo::GUIIcon)
		{
			return sf::IntRect(66, 22, 121, 131);

		}
		else if(type == CrystalInfo::RockSmall)
		{
			return sf::IntRect(196, 4, 98, 184);

		}
		else if(type == CrystalInfo::RockLarge)
		{
			return sf::IntRect(327, 5, 121, 165);

		}
		else if(type == CrystalInfo::Full)
		{
			return sf::IntRect(474, 0, 170, 229);

		}
		else if(type == CrystalInfo::Bit)
		{
			return sf::IntRect(665, 71, 43, 67);

		}

	}

	return sf::IntRect(0, 0, 1, 1);

}

void Crystal::setupSprite()
{
	if(m_color == CrystalInfo::Blue)
	{
		SceneActor* s = sprite.addChild(SceneActor("crystal", blueCrystal, getFrame(m_color, m_type)));
		s->setScale(0.7, 0.7);

	}
	else if(m_color == CrystalInfo::Green)
	{
		SceneActor* s = sprite.addChild(SceneActor("crystal", greenCrystal, getFrame(m_color, m_type)));
		s->setScale(0.7, 0.7);

	}
	else if(m_color == CrystalInfo::Red)
	{
		SceneActor* s = sprite.addChild(SceneActor("crystal", redCrystal, getFrame(m_color, m_type)));
		s->setScale(0.7, 0.7);

	}
	else if(m_color == CrystalInfo::Gold)
	{
		SceneActor* s = sprite.addChild(SceneActor("crystal", goldCrystal, getFrame(m_color, m_type)));
		s->setScale(0.7, 0.7);

	}

}

void Crystal::loadResources()
{
	static bool loaded = false;
	if(!loaded)
	{
		loaded = true;

		//textures
		blueCrystal = game->files.get<sf::Texture>("bluecrystals.png");
		greenCrystal = game->files.get<sf::Texture>("greencrystals.png");
		redCrystal = game->files.get<sf::Texture>("redcrystals.png");
		goldCrystal = game->files.get<sf::Texture>("goldcrystals.png");

		//particles
		ParticleManager& particles = game->particles;
		particles.add("blue bit", blueCrystal, getFrame(CrystalInfo::Blue, CrystalInfo::Bit), 250);
		particles.add("green bit", greenCrystal, getFrame(CrystalInfo::Green, CrystalInfo::Bit), 250);
		particles.add("red bit", redCrystal, getFrame(CrystalInfo::Red, CrystalInfo::Bit), 250);
		particles.add("gold bit", goldCrystal, getFrame(CrystalInfo::Gold, CrystalInfo::Bit), 250);

	}

}
