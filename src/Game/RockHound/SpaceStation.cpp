#include "SpaceStation.h"
#include "../../Engine/Engine.h"
#include <math.h>
#include <iostream>
#include "RockHound.h"
#include "PlayerMiner.h"

sf::Texture* SpaceStation::spaceStationTexture;
sf::Texture* SpaceStation::gasIcon;
sf::Texture* SpaceStation::sellCrystalIcon;

SpaceStation::SpaceStation(sf::Vector2f pos) :
	Entity(2, pos, sf::Vector2f(885*0.7, 708*0.7))
{
	static bool loaded = false;
	if(!loaded)
	{
		loaded = true;
		spaceStationTexture = game->files.get<sf::Texture>("spacestation.png");
		gasIcon = game->files.get<sf::Texture>("gas.png");
		sellCrystalIcon = game->files.get<sf::Texture>("sellcrystal.png");

	}

	SceneActor* s = sprite.addChild(SceneActor("main", spaceStationTexture));
	s->setPosition(sf::Vector2f(-61*0.7, 0));
	rotVelDrag = 0.99;
	updateRate = 30;
	isStatic = true;

	playerEntity = dynamic_cast<PlayerMiner*>(game->entities.getEntitiesWithTag("player")[0]);
	playerAt = 0;
	playerDist = 0;
	shopOpen = false;

	fuelButton = Button(sf::Vector2f(game->guiWidth/2, game->guiHeight-132),
			sf::Vector2f(64, 64), gasIcon, sf::Vector2f(63, 60), "\n\n\nBuy Gas", *RockHound::pixel);
	sellButton = Button(sf::Vector2f(game->guiWidth/2, game->guiHeight-132),
			sf::Vector2f(64, 64), sellCrystalIcon, sf::Vector2f(22, 54), "\n\n\nSell Crystal", *RockHound::pixel);
	shopButton = Button(sf::Vector2f(game->guiWidth/2, game->guiHeight-132),
			sf::Vector2f(64, 64), RockHound::guiCash, sf::Vector2f(22, 54), "\n\n\nShop", *RockHound::pixel);


	titaniumDrill = Button(sf::Vector2f(game->guiWidth/2 - 180 - 64, game->guiHeight-132 - 20),
			sf::Vector2f((64*4), 112), RockHound::guiCash, sf::Vector2f(43/2, 78/2), "\n\n\nTitanium Drill\n  100 Credits", *RockHound::pixel);
	diamondoniumDrill = Button(sf::Vector2f(game->guiWidth/2 - 180 - 64, game->guiHeight-132 - 112 - 10 - 20),
			sf::Vector2f((64*4), 112), RockHound::guiCash, sf::Vector2f(43/2, 78/2), "\n\n\nDiamondonium Drill\n   250 Credits", *RockHound::pixel);
	uberoniumDrill = Button(sf::Vector2f(game->guiWidth/2 - 180 - 64, game->guiHeight-132 - 112*2 - 20 - 20),
			sf::Vector2f((64*4), 112), RockHound::guiCash, sf::Vector2f(43/2, 78/2), "\n\n\nUberonium Drill\n  600 Credits", *RockHound::pixel);

	tank100 = Button(sf::Vector2f(game->guiWidth/2 + 112 - 64*1.4, game->guiHeight-132 - 20),
			sf::Vector2f((64*4), 112), RockHound::guiCash, sf::Vector2f(43/2, 78/2), "\n\n\n100L Tank\n100 Credits", *RockHound::pixel);
	tank150 = Button(sf::Vector2f(game->guiWidth/2 + 112 - 64*1.4, game->guiHeight-132 - 112 - 10 - 20),
			sf::Vector2f((64*4), 112), RockHound::guiCash, sf::Vector2f(43/2, 78/2), "\n\n\n150L Tank\n300 Credits", *RockHound::pixel);
	tank250 = Button(sf::Vector2f(game->guiWidth/2 + 112 - 64*1.4, game->guiHeight-132 - 112*2 - 20 - 20),
			sf::Vector2f((64*4), 112), RockHound::guiCash, sf::Vector2f(43/2, 78/2), "\n\n\n250L Tank\n500 Credits", *RockHound::pixel);


	mark4 = Button(sf::Vector2f(game->guiWidth/2 + 112 + 112*1.6, game->guiHeight-132 - 20),
			sf::Vector2f((64*4), 112), RockHound::guiCash, sf::Vector2f(43/2, 78/2), "\n\n\nMiner Mk. 4\n350 Credits", *RockHound::pixel);
	mark5  = Button(sf::Vector2f(game->guiWidth/2 + 112 + 112*1.6, game->guiHeight-132 - 112 - 10 - 20),
			sf::Vector2f((64*4), 112), RockHound::guiCash, sf::Vector2f(43/2, 78/2), "\n\n\nMiner Mk. 5\n700 Credits", *RockHound::pixel);
	mark6 = Button(sf::Vector2f(game->guiWidth/2 + 112 + 112*1.6, game->guiHeight-132 - 112*2 - 20 - 20),
			sf::Vector2f((64*4), 112), RockHound::guiCash, sf::Vector2f(43/2, 78/2), "\n\n\nMiner Mk. 6\n1200 Credits", *RockHound::pixel);

}

std::string SpaceStation::getName()
{
	return "spacestation";

}

void SpaceStation::onTick()
{
	rotVel = 0.05;

	if(playerEntity != NULL)
	{
		sf::Vector2f pos = getSpritePos();
		sf::Vector2f target = playerEntity->getSpritePos();
		sf::Vector2f dif = target - pos;
		float errorAngle = atan2(dif.y, dif.x);
		//std::cout << "errorAngle: " << errorAngle << "\n";
		errorAngle = (errorAngle*180/3.14159);
		if (errorAngle < 0.0f)
		{
			errorAngle = 360.0f - fabs(errorAngle);

		}

		errorAngle = errorAngle - getAngle();

		playerAt = errorAngle;
		playerDist = distanceTo(playerEntity);

		float playerWorldRatio = playerDist/game->settings.worldSize;
		if(playerWorldRatio > 0.7)
		{
			//send help (outpost)

		}

		fuelAvail = false;
		sellAvail = false;
		repairUpgradeAvail = false;

		if(playerDist >= 220 && playerDist <= 550)
		{
			//std::cout << "playerAt: " << playerAt << "\n";
			if((playerAt <= 90 && playerAt >= 0) || (playerAt <= -210 && playerAt >= -360))
			{
				if(playerEntity->credits >= 2 && playerEntity->fuel < playerEntity->maxFuel - playerEntity->maxFuel/6)
				{
					fuelAvail = true;

				}

				shopOpen = false;

			}
			else if((playerAt >= 270 && playerAt <= 360) || (playerAt <= 0 && playerAt >= -100))
			{
				if(playerEntity->inv.entities.size() >= 1)
				{
					sellAvail = true;

				}

				shopOpen = false;

			}
			else if((playerAt >= 150 && playerAt < 270) || (playerAt <= -90 && playerAt >= -200))
			{
				repairUpgradeAvail = true;

			}
			else
			{
				shopOpen = false;

			}

		}

	}

}

void SpaceStation::onDraw()
{

}

void SpaceStation::onDeath()
{

}

bool SpaceStation::doesCollide(Entity* other)
{
	return true;

}

void SpaceStation::onCollide(Entity* other)
{

}

void SpaceStation::handleShop()
{
	bool epressed = game->EDown();
	float soundVol = (playerDist/200)*100;

	if(fuelAvail)
	{
		if(fuelButton.draw(game->window) || epressed)
		{
			std::cout << "buy fuel button\n";
			unsigned int fuelPerCredit = 5;
			unsigned int maxFuelNeeded = playerEntity->maxFuel - playerEntity->fuel;
			while((maxFuelNeeded/fuelPerCredit) + 1 > playerEntity->credits)
			{
				maxFuelNeeded--;

			}

			unsigned int purchase = (maxFuelNeeded/fuelPerCredit) + 1;
			playerEntity->credits -= purchase;
			playerEntity->fuel += purchase*fuelPerCredit;

			if(!game->settings.muteSound)
			{
				RockHound::life.setVolume(soundVol*game->settings.volume);
				RockHound::life.play();

			}


		}

	}
	else if(sellAvail)
	{
		if(sellButton.draw(game->window) || epressed)
		{
			//std::cout << "sold crystal\maxThrottlen";
			unsigned int blue = playerEntity->inv.numEntitiesWithName("blue crystal");
			unsigned int green = playerEntity->inv.numEntitiesWithName("green crystal");
			unsigned int red = playerEntity->inv.numEntitiesWithName("red crystal");
			unsigned int gold = playerEntity->inv.numEntitiesWithName("gold crystal");
			playerEntity->credits += blue*10;
			playerEntity->credits += green*13;
			playerEntity->credits += red*20;
			playerEntity->credits += gold*30;
			playerEntity->inv.clear();

			if(!game->settings.muteSound)
			{
				playerEntity->chaching.setVolume(soundVol*game->settings.volume);
				playerEntity->chaching.play();

			}

		}

	}
	else if(repairUpgradeAvail)
	{
		if(!shopOpen)
		{
			if(shopButton.draw(game->window) || epressed)
			{
				shopOpen = true;

			}

		}
		else
		{
			unsigned int drillTier = 0;
			drillTier = playerEntity->hasTag("uber") ? 3 : playerEntity->hasTag("diamond") ? 2 : playerEntity->hasTag("titanium") ? 1 : 0;
			unsigned int cash = playerEntity->credits;

			if(drillTier == 0)
			{
				if(titaniumDrill.draw(game->window))
				{
					if(cash >= 100)
					{
						playerEntity->credits -= 100;
						playerEntity->drillStrength = 2.0f;
						playerEntity->addTag("titanium");

						if(!game->settings.muteSound)
						{
							playerEntity->chaching.setVolume(soundVol*game->settings.volume);
							playerEntity->chaching.play();

						}

					}

				}

			}

			if(drillTier <= 1)
			{
				if(diamondoniumDrill.draw(game->window))
				{
					if(cash >= 250)
					{
						playerEntity->credits -= 250;
						playerEntity->drillStrength = 4.0f;
						playerEntity->addTag("diamond");

						if(!game->settings.muteSound)
						{
							playerEntity->chaching.setVolume(soundVol*game->settings.volume);
							playerEntity->chaching.play();

						}

					}

				}

			}

			if(drillTier <= 2)
			{
				if(uberoniumDrill.draw(game->window))
				{
					if(cash >= 600)
					{
						playerEntity->credits -= 600;
						playerEntity->drillStrength = 8.0f;
						playerEntity->addTag("uber");

						if(!game->settings.muteSound)
						{
							playerEntity->chaching.setVolume(soundVol*game->settings.volume);
							playerEntity->chaching.play();

						}
					}

				}

			}

			float maxFuel = playerEntity->maxFuel;

			if(maxFuel < 100)
			{
				if(tank100.draw(game->window))
				{
					if(cash >= 100)
					{
						playerEntity->credits -= 100;
						playerEntity->maxFuel = 100;
						playerEntity->fuel = 100;

						if(!game->settings.muteSound)
						{
							playerEntity->chaching.setVolume(soundVol*game->settings.volume);
							playerEntity->chaching.play();

						}

					}

				}

			}

			if(maxFuel < 150)
			{
				if(tank150.draw(game->window))
				{
					if(cash >= 300)
					{
						playerEntity->credits -= 300;
						playerEntity->maxFuel = 150;
						playerEntity->fuel = 150;

						if(!game->settings.muteSound)
						{
							playerEntity->chaching.setVolume(soundVol*game->settings.volume);
							playerEntity->chaching.play();

						}

					}

				}

			}

			if(maxFuel <  250)
			{
				if(tank250.draw(game->window) && maxFuel < 250)
				{
					if(cash >= 500)
					{
						playerEntity->credits -= 500;
						playerEntity->maxFuel = 250;
						playerEntity->fuel = 250;

						if(!game->settings.muteSound)
						{
							playerEntity->chaching.setVolume(soundVol*game->settings.volume);
							playerEntity->chaching.play();

						}

					}

				}

			}


			Miner::Model smodel = playerEntity->mymodel;

			if(smodel == Miner::M3k)
			{
				if(mark4.draw(game->window))
				{
					if(cash >= 350)
					{
						playerEntity->credits -= 350;
						playerEntity->setModel(Miner::MMk4);

						if(!game->settings.muteSound)
						{
							playerEntity->chaching.setVolume(soundVol*game->settings.volume);
							playerEntity->chaching.play();

						}

					}

				}

			}

			if(smodel == Miner::MMk4 || smodel == Miner::M3k)
			{
				if(mark5.draw(game->window))
				{
					if(cash >= 700)
					{
						playerEntity->credits -= 700;
						playerEntity->setModel(Miner::MMk5);

						if(!game->settings.muteSound)
						{
							playerEntity->chaching.setVolume(soundVol*game->settings.volume);
							playerEntity->chaching.play();

						}

					}

				}

			}

			if(smodel == Miner::MMk5 || smodel == Miner::MMk4 || smodel == Miner::M3k)
			{
				if(mark6.draw(game->window))
				{
					if(cash >= 1200)
					{
						playerEntity->credits -= 1200;
						playerEntity->setModel(Miner::MMk6);

						if(!game->settings.muteSound)
						{
							playerEntity->chaching.setVolume(soundVol*game->settings.volume);
							playerEntity->chaching.play();

						}
					}

				}

			}

		}

	}

}

