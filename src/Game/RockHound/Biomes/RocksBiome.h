#ifndef ROCKSBIOME_H_
#define ROCKSBIOME_H_

#include "../Generator/Biome.h"
#include "../Rocks/Rock.h"

class RocksBiome : public Biome
{
public:
	RocksBiome(CrystalInfo::Color color, unsigned int weight);

	virtual ~RocksBiome();

	std::string getName();
	unsigned int getWeight();

	//TODO: use delauny triangulation to find the radius
	//tag everything with this biome name and sector id
	bool generate(Sector* sector, float radius);

	unsigned int weight;
	CrystalInfo::Color color;

};

#endif /* ROCKSBIOME_H_ */
