#ifndef EMPTYSPACE_H_
#define EMPTYSPACE_H_

#include "../Generator/Biome.h"

class EmptySpace : public Biome
{
public:
	EmptySpace(unsigned int weight);

	std::string getName();
	unsigned int getWeight();

	bool generate(Sector* sector, float radius);

	unsigned int weight;

};

#endif /* EMPTYSPACE_H_ */
