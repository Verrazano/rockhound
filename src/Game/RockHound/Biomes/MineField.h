#ifndef MINEFIELD_H_
#define MINEFIELD_H_

#include "../Generator/Biome.h"

class MineField : public Biome
{
public:
	MineField(unsigned int weight);

	virtual ~MineField();

	std::string getName();
	unsigned int getWeight();

	bool generate(Sector* sector, float radius);

	unsigned int weight;

};

#endif /* MINEFIELD_H_ */
