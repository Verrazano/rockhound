#include "RocksBiome.h"
#include "../../../Engine/Engine.h"
#include "../Rocks/Rock.h"
#include <math.h>
#include <iostream>

RocksBiome::RocksBiome(CrystalInfo::Color color, unsigned int weight)
{
	this->color = color;
	if(weight == 0)
		weight = 1;

	this->weight = weight;


}

RocksBiome::~RocksBiome()
{
}

std::string RocksBiome::getName()
{
	std::string crystal;
	if(color == CrystalInfo::Blue)
	{
		crystal = "blue";

	}
	else if(color == CrystalInfo::Green)
	{
		crystal = "green";

	}
	else if(color == CrystalInfo::Red)
	{
		crystal = "red";

	}
	else if(color == CrystalInfo::Gold)
	{
		crystal = "gold";

	}

	return crystal + " rocks";

}

unsigned int RocksBiome::getWeight()
{
	return weight;

}

bool RocksBiome::generate(Sector* sector, float radius)
{
	sf::Vector2f pos = sector->getSpritePos();
	unsigned int rocks = (pow(radius,2)*3.14159)/(50000*8);
	//std::cout << "radius: " << radius << "\n";
	//std::cout << "rocks: " << rocks << "\n";
	//sf::sleep(sf::seconds(2));

	float factor = 1;
	if(color == CrystalInfo::Blue)
	{
		factor = 1.5;

	}
	else if(color == CrystalInfo::Green)
	{
		factor = 1;

	}
	else if(color == CrystalInfo::Red)
	{
		factor = 0.8;

	}
	else if(color == CrystalInfo::Gold)
	{
		factor = 0.5;

	}

	rocks *= factor;

	for(unsigned int i = 0; i < rocks; i++)
	{
		RockInfo::Size size = (RockInfo::Size)(rand()%2);
		if(size == RockInfo::Bit)
		{
			size = RockInfo::Small;

		}
		unsigned int frame = rand()%9;
		radius++;
		sf::Vector2f rpos;
		rpos.x = game->randFrom(-radius, radius);
		rpos.y = game->randFrom(-radius, radius);

		rpos += pos;

		Rock* rock = new Rock(RockInfo::getInfo(size, frame), color, rpos);
		game->addEntity(rock);

	}

	return true;

}
