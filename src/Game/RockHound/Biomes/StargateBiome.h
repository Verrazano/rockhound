#ifndef STARGATEBIOME_H_
#define STARGATEBIOME_H_

#include "../Generator/Biome.h"
#include "../Rocks/Rock.h"

class StargateBiome : public Biome
{
public:
	StargateBiome(unsigned int weight, unsigned int limit);

	virtual ~StargateBiome();

	std::string getName();
	unsigned int getWeight();

	//TODO: use delauny triangulation to find the radius
	//tag everything with this biome name and sector id
	bool generate(Sector* sector, float radius);

	unsigned int weight;
	unsigned int count;
	unsigned int limit;
	std::string lastid;

};


#endif /* STARGATEBIOME_H_ */
