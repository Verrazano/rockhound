#include "MineField.h"
#include "../Mine.h"
#include <math.h>
#include "../../../Engine/Engine.h"

MineField::MineField(unsigned int weight)
{
	if(weight == 0)
		weight = 1;

	this->weight = weight;


}

MineField::~MineField()
{
}

std::string MineField::getName()
{
	return "minefield";

}

unsigned int MineField::getWeight()
{
	return weight;

}

bool MineField::generate(Sector* sector, float radius)
{
	sf::Vector2f pos = sector->getSpritePos();
	unsigned int mines = (pow(radius,2)*3.14159)/(50000*8);
	mines *= 1.5;

	radius++;
	for(unsigned int i = 0; i < mines; i++)
	{
		sf::Vector2f rpos;
		rpos.x = game->randFrom(-radius, radius);
		rpos.y = game->randFrom(-radius, radius);

		rpos += pos;

		game->addEntity(new Mine(rpos));

	}

	return true;

}
