#include "EmptySpace.h"
#include "../Debris/Crate.h"
#include "../../../Engine/Engine.h"

EmptySpace::EmptySpace(unsigned int weight)
{
	this->weight = weight;

}

std::string EmptySpace::getName(){ return "empty space"; }
unsigned int EmptySpace::getWeight(){return weight; }

bool EmptySpace::generate(Sector* sector, float radius)
{
	int num = rand()%6;
	for(unsigned int i = 0; i < num; i++)
	{
		sf::Vector2f pos = sector->getSpritePos();
		pos.x += (rand()%(int)radius*2)-radius;
		pos.y += (rand()%(int)radius*2)-radius;
		game->addEntity(new Crate(pos, rand()%360));

	}

	return true;

}

