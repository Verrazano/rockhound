#include "StargateBiome.h"
#include "../../../Engine/Engine.h"
#include "../Stargate.h"
#include <iostream>

StargateBiome::StargateBiome(unsigned int weight, unsigned int limit)
{
	this->weight = weight;
	this->limit = limit;
	count  = 0;

}

StargateBiome::~StargateBiome()
{

}

std::string StargateBiome::getName()
{
	return "stargate";

}

unsigned int StargateBiome::getWeight()
{
	return weight;

}

bool StargateBiome::generate(Sector* sector, float radius)
{
	if(count == 2)
		return false;

	std::cout << "adding stargate\n";
	std::string temp = lastid;
	Stargate* s = new Stargate(sector->getSpritePos(), rand()%360);
	s->setLink(lastid);
	lastid = game->entities.addEntity(s);
	count++;

	return true;

}

