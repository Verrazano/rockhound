#include "Miner.h"
#include "../../Engine/Engine.h"
#include <math.h>
#include <iostream>
#include "Crystals/Crystal.h"
#include "Rocks/Rock.h"

sf::Sound Miner::pickup;
sf::SoundBuffer Miner::pickupBuffer;
sf::Texture* Miner::minerTexture;
sf::Texture* Miner::mark4;
sf::Texture* Miner::mark5;
sf::Texture* Miner::mark6;
//sf::Texture Miner::gunTexture;
sf::Texture* Miner::tugboat;
sf::Texture* Miner::engine;
sf::Texture* Miner::drill;

Miner::Miner(float z, sf::Vector2f pos, bool fullControl) :
	Ship(z, pos, sf::Vector2f(127, 127), fullControl, 0.07, 0.2, 20, 3, 10)
{
	addTag("miner");
	static bool loaded = false;
	if(!loaded)
	{
		loaded = true;
		minerTexture = game->files.get<sf::Texture>("miner.png");
		mark4 = game->files.get<sf::Texture>("mark4.png");
		mark5 = game->files.get<sf::Texture>("mark5.png");
		mark6 = game->files.get<sf::Texture>("mark6.png");
		pickupBuffer.loadFromFile("res/itempickup.wav");
		pickup.setBuffer(pickupBuffer);
		game->particles.add("rockbit", sf::Color(160, 82, 45), 0, sf::Vector2u(30, 30), 100);
		tugboat = game->files.get<sf::Texture>("tugboatbody.png");
		engine = game->files.get<sf::Texture>("tugboatengine.png");
		drill = game->files.get<sf::Texture>("drill.png");
		//gunTexture.loadFromFile("res/miner/gun09.png");

	}

	/*Sprite& gun1 = sprite.addChild(SceneActor("gun", &gunTexture);
	gun1.center = sf::Vector2f(0, 100);
	gun1.angle = 90;
	Sprite& gun2 = sprite.addChild(SceneActor("gun2", &gunTexture);
	gun2.center = sf::Vector2f(0, -100);
	gun2.angle = 90;
	gun2.sprite.setScale(sf::Vector2f(-1, 1));*/

	drillStrength = 1.0f;
	//crystals = 0;

	maxFuel = 50;
	fuel = maxFuel;

	setModel(Miner::M3k);
	drillframe = 0;

}

Miner::~Miner()
{
}

std::string Miner::getName()
{
	return "miner";

}

void Miner::onTick()
{
	updateShip();
	updateFuel();
	if(isMining())
	{
		maxVelocity = (mspeed+5)/2;
		maxThrottle = (mspeed+5)/2;

		/*float drillTime = drillTimer.getElapsedTime().asSeconds();
		bool blurred = drillTime > 2;
		int heat = blurred == false ? 0 : drillTime/4;
		if(drillAnim.getElapsedTime().asSeconds() > 0.25)
		{
			drillframe++;
			if(blurred && drillframe > 2)
			{
				drillframe = 0;

			}
			if(drillframe > 1)
			{
				drillframe = 0;

			}

			drillAnim.restart();

		}

		if(blurred)
		{
			Sprite& drill = getSprite("drill");
			drill.sprite.setTextureRect(sf::IntRect(200 + drillframe*100, 0, 100, 100));
			Sprite& sheat = getSprite("drillheat");
			sheat.sprite.setTextureRect(sf::IntRect(400 + heat*100, 0, 100, 100));
			sheat.sprite.setFillColor(sf::Color::White);

		}
		else
		{
			Sprite& drill = getSprite("drill");
			drill.sprite.setTextureRect(sf::IntRect(drillframe*100, 0, 100, 100));
			Sprite& sheat = getSprite("drillheat");
			sheat.sprite.setFillColor(sf::Color::Transparent);

		}*/

	}
	else
	{
		maxVelocity = mspeed;
		maxThrottle = mspeed;

		drillTimer.restart();

		/*Sprite& drill = getSprite("drill");
		drill.sprite.setTextureRect(sf::IntRect(0, 0, 100, 100));*/
		drillframe = 0;

	}

	if(fuel < 0)
	{
		health = -1;

	}

}

void Miner::onDraw()
{
	//nothing special ... yet
	//TODO: anim if mining

}

void Miner::onDeath()
{
	//explode plix and if player do other stuff....

}

bool Miner::doesCollide(Entity* other)
{
	if(other->getName() == "mine")
	{
		return true;

	}

	if(other->getName() == "stargate")
	{
		return true;

	}

	if(other->hasTag("debri"))
	{
		return true;

	}


	if(other->getName() == "wall" || other->hasTag("wall"))
	{
		return true;

	}

	if(other->getName() == "rock" || other->hasTag("rock"))
	{
		if(isMining())
		{
			return true;

		}

		return false;

	}

	if(other->getName() == "crystal" || other->hasTag("crystal"))
	{
		if(inv.entities.size() < storage && other->doesCollide(this))
		{
			dynamic_cast<Crystal*>(other)->emitBits(20);
			inv.takeFrom(&game->entities, other->getIDString());
			other->remTag("shatter");

			if(!game->settings.muteSound)
			{

				pickup.setVolume(100*game->settings.volume);
				pickup.play();

			}

		}

		return false;

	}

	if(other->getName() == "bullet" || other->hasTag("bullet"))
	{
		return true;

	}

	return false;

}

void Miner::onCollide(Entity* other)
{
	if(other->getName() == "rock" || other->hasTag("rock"))
	{
		if(game->frame%20 == 0)
		{
			dynamic_cast<Rock*>(other)->emitBits(5, getAngle(), getSpritePos());

		}

		other->health -= drillStrength;
		//do mean things to it

	}

	if(other->getName() == "bullet")
	{
		health -= 10;
		other->health = -1;

	}

}

void Miner::setMining(bool state)
{
	mining = state;

}

bool Miner::isMining()
{
	return mining;

}

void Miner::updateFuel()
{
	//fixed time step
	float frameRate = game->settings.frameRate;
	float dt = 1/frameRate;
	if(isMining())
		dt *= 2;
	fuel -= dt;

}

bool Miner::isPressingThrottle()
{
	return false;

}

int Miner::isPressingDir()
{
	return 0;

}

float Miner::getThrottle()
{
	return 0;

}

sf::Vector2f Miner::getTarget()
{
	return sf::Vector2f(0, 0);

}

void Miner::emitRockBits(unsigned int count)
{
	float a = getAngle();
	float power = 100;
	for(unsigned int i = 0; i < count; i++)
	{
		unsigned int side = rand()%2;
		if(side == 0)
		{
			a -= 90;

		}
		else
		{
			a += 90;

		}
		float a1 = game->randFrom(-15, 15) + a;
		a1 = a1*3.14159/180;
		sf::Vector2f pos = getSpritePos();
		pos.x += game->randFrom(-30, 30);
		pos.y += game->randFrom(-30, 30);
		game->particles.emit("rockbit", pos, sf::Vector2f(cos(a1)*power, sin(a1)*power), 5);

	}

}

std::string Miner::getCrystalInfo()
{
	return "CRYSTAL:\n"+toString((int)inv.entities.size()) + " of " + toString(storage);

}

std::string Miner::getFuelInfo()
{
	return toString((int)fuel) + "L of " + toString((int)maxFuel) + "L left";

}

void Miner::setModel(Model nmodel)
{
	if(nmodel == mymodel && nmodel != Miner::M3k)
	{
		return;

	}

	health = maxHealth;

	mymodel = nmodel;
	if(nmodel == Miner::M3k)
	{
		storage = 5;
		sprite.addChild(SceneActor("main", minerTexture));
		//s.sprite.setOrigin(s.sprite.getOrigin().x, minerTexture.getSize().y - 200);
		//s.angle = 90;
		mspeed = 20;

	}
	else if(nmodel == Miner::MMk4)
	{
		storage = 12;
		sprite.addChild(SceneActor("main", minerTexture))->setTexture(mark4);
		mspeed = 23;

	}
	else if(nmodel == Miner::MMk5)
	{
		storage = 20;
		sprite.addChild(SceneActor("main", minerTexture))->setTexture(mark5);
		mspeed = 26;

	}
	else if(nmodel == Miner::MMk6)
	{
		storage = 35;
		sprite.addChild(SceneActor("main", minerTexture))->setTexture(mark6);
		mspeed = 30;

	}
	else if(nmodel == Miner::tugBoat)
	{
		storage = 10;


		/*Sprite& sdrill = sprite.addChild(SceneActor("drill", &drill, sf::IntRect(0, 0, 80, 80));
		sdrill.angle = -90;
		sdrill.center.x = 128/2 + 18;
		sdrill.center.y = -6;*/

		/*Sprite& drillheat = sprite.addChild(SceneActor("drillheat", &drill, sf::IntRect(0, 0, 100, 100));
		drillheat.sprite.setFillColor(sf::Color::Transparent);
		drillheat.angle = -90;
		drillheat.center.x = 128/2 + 18;
		drillheat.center.y = -6;*/

		/*Sprite& body = sprite.addChild(SceneActor("body", tugboat);
		body.angle = 90;

		Sprite& leftengine = sprite.addChild(SceneActor("leftengine", engine);
		leftengine.angle = -90;
		leftengine.center.y -= 128/2 + 30;
		leftengine.center.x -= 24;
		leftengine.sprite.setScale(-1, 1);

		Sprite& rightengine = sprite.addChild(SceneActor("rightengine", engine);
		rightengine.angle = -90;
		rightengine.center.y += 128/2 + 30;
		rightengine.center.x -= 24;*/

		mspeed = 15;


	}

}
