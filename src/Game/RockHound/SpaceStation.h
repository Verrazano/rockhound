#ifndef SPACESTATION_H_
#define SPACESTATION_H_

#include "../../Entity/Entity.h"
#include "../../Engine/Button.h"

class PlayerMiner;

class SpaceStation : public Entity
{
public:
	SpaceStation(sf::Vector2f pos);

	std::string getName();

	void onTick();

	void onDraw();

	void onDeath();

	bool doesCollide(Entity* other);

	void onCollide(Entity* other);

	//call this in game mode onDraw
	void handleShop();

	static sf::Texture* spaceStationTexture;
	static sf::Texture* gasIcon;
	static sf::Texture* sellCrystalIcon;

	PlayerMiner* playerEntity;

	bool fuelAvail;
	Button fuelButton;

	bool sellAvail;
	Button sellButton;

	bool repairUpgradeAvail;
	Button titaniumDrill;
	Button diamondoniumDrill;
	Button uberoniumDrill;

	Button tank100;
	Button tank150;
	Button tank250;

	Button mark4;
	Button mark5;
	Button mark6;

	bool shopOpen;
	Button shopButton;

	float playerAt;
	float playerDist;

};


#endif /* SPACESTATION_H_ */
