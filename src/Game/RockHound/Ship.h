#ifndef SHIP_H_
#define SHIP_H_

#include "../../Entity/Entity.h"

class Ship : public Entity
{
public:
	//if fullControll uses getTarget and getThrottle instead of isPressingThrottle and isPressingDir
	Ship(float z, sf::Vector2f pos, sf::Vector2f size, bool fullControl, float speed, float turnSpeed, float maxThrot = 25.0f, float maxTurn = 4.0f,
			float mass = 1.0f, float maxVel = 25.0f, float drag = 0.93f);

	virtual ~Ship();

	virtual std::string getName() = 0;

	virtual void onTick() = 0;

	virtual void onDraw() = 0;

	virtual void onDeath() = 0;

	virtual bool doesCollide(Entity* other) = 0;

	virtual void onCollide(Entity* other) = 0;

	virtual bool isPressingThrottle() = 0;

	//0 no dir, -1 left, 1 right
	virtual int isPressingDir() = 0;

	virtual float getThrottle() = 0;
	virtual sf::Vector2f getTarget() = 0;

	void updateShip();
	void emitSmoke(unsigned int count);

	float speed;
	float turnSpeed;
	float throttle;
	float maxThrottle;
	float turn;
	float maxTurn;

	bool fullControl;

	sf::Sound engine;
	static sf::SoundBuffer* engineBuffer;

};

#endif /* MINER_H_ */
