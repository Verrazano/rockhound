#include "Mine.h"
#include "../../Engine/Engine.h"
#include <math.h>

sf::Texture* Mine::mineText;

Mine::Mine(sf::Vector2f pos) : Entity(0.5, pos, sf::Vector2f(50, 50))
{
	static bool loaded = false;
	if(!loaded)
	{
		loaded = true;

		mineText = game->files.get<sf::Texture>("mine.png");

	}

	addTag("mine");

	updateRate = 30;
	isStatic = true;
	rotVel = 1.0f;
	drag = 0.98;

	m_rot = game->randFrom(0, 3) - 1;
	m_rot /= 4;
	m_rot*=2;
	rotVel = m_rot;

	sprite.addChild(SceneActor("mine", mineText, sf::IntRect(0, 0, 76, 118)));
	range = 450.0f;
	targetID = "";

	health = 25;
	frame2 = false;

	bomb = Explosion(this, 200, 200, 25);

}

std::string Mine::getName()
{
	return "mine";

}

void Mine::onTick()
{
	rotVel = m_rot;
	/*if(targetID == "")
	{
		if(animTime.getElapsedTime().asSeconds() > 0.3)
		{
			if(!frame2)
			{
				frame2 = true;
				Sprite& s = getSprite("mine");
				s.sprite.setTextureRect(sf::IntRect(78, 0, 118, 118));
				s.sprite.setSize(sf::Vector2f(118, 118));
				s.sprite.setOrigin(118/2, 118/2);

			}
			else
			{
				frame2 = false;
				Sprite& s = getSprite("mine");
				s.sprite.setTextureRect(sf::IntRect(0, 0, 78, 118));
				s.sprite.setSize(sf::Vector2f(78, 118));
				s.sprite.setOrigin(78/2, 118/2);

			}

		}

		std::vector<Entity*> targets = game->entities.getEntitiesWithTagInDist(range, "ship", this);
		if(targets.size() == 0)
		{
			return;

		}

		targetID = targets[0]->getIDString();
		updateRate = 5;

	}
	else
	{
		if(animTime.getElapsedTime().asSeconds() > 0.3)
		{
			if(!frame2)
			{
				frame2 = true;
				Sprite& s = getSprite("mine");
				s.sprite.setTextureRect(sf::IntRect(274, 0, 118, 118));
				s.sprite.setSize(sf::Vector2f(118, 118));
				s.sprite.setOrigin(118/2, 118/2);

			}
			else
			{
				frame2 = false;
				Sprite& s = getSprite("mine");
				s.sprite.setTextureRect(sf::IntRect(196, 0, 78, 118));
				s.sprite.setSize(sf::Vector2f(78, 118));
				s.sprite.setOrigin(78/2, 118/2);

			}

		}

		Entity* target = game->entities.getEntity(targetID);
		if(target == NULL)
		{
			//lost target die play anim to die
			health = -1;
			return;

		}

		float dist = distanceTo(target);
		if(dist > range)
		{
			//mine out of place and lost target die ect...
			health = -1;
			return;

		}

		sf::Vector2f diff = target->getSpritePos();
		diff -= getSpritePos();
		float a = atan2f(diff.y, diff.x);
		a = a*180/3.14159;

		addForce(a, dist*0.01);

	}*/
	//TODO redo all this crap

}

void Mine::onDraw()
{
//do nothing
}

void Mine::onDeath()
{
	//explode into smoke
	//play explody sounds

	unsigned int count = 100;

	float power = 50;
	for(unsigned int i = 0; i < count; i++)
	{
		float angle = rand()%360;
		angle = angle*3.14159/180;
		sf::Vector2f pos = getSpritePos();
		pos.x += game->randFrom(-60, 60);
		pos.y += game->randFrom(-60, 60);
		game->particles.emit("smoke", pos, sf::Vector2f(cos(angle)*power, sin(angle)*power), 8, 0.5);

	}


}

bool Mine::doesCollide(Entity* other)
{
	if(other->hasTag("ship"))
	{
		return true;

	}

	return false;

}

void Mine::onCollide(Entity* other)
{
	if(other->hasTag("ship"))
	{
		bomb.explode();
		health = -1;

	}

}
