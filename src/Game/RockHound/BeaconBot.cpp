#include "BeaconBot.h"
#include "../../Engine/Engine.h"

sf::Texture* BeaconBot::beaconbotText;

BeaconBot::BeaconBot(sf::Vector2f pos) :
	Entity(2, pos, sf::Vector2f(64, 64))
{
	static bool loaded = false;
	if(!loaded)
	{
		loaded = true;
		beaconbotText = game->files.get<sf::Texture>("beaconbot.png");

	}

	sprite.addChild(SceneActor("main", beaconbotText));

	turn = rand()%3-1;
	turn *= 0.3;
	rotVelDrag = 0.99;
	updateRate = 30;
	maxHealth = 500;
	health = maxHealth;

}

std::string BeaconBot::getName()
{
	return "beaconbot";

}

void BeaconBot::onTick()
{
	rotVel = turn;
	health -= 0.1;

}

void BeaconBot::onDraw()
{

}

void BeaconBot::onDeath()
{

}

bool BeaconBot::doesCollide(Entity* other)
{
	return true;

}

void BeaconBot::onCollide(Entity* other)
{

}
