#ifndef COLONY_H_
#define COLONY_H_

#include "../../Entity/Entity.h"

class Colony : public Entity
{
public:
	Colony(sf::Vector2f pos);

	std::string getName();

	virtual void onTick();

	virtual void onDraw();

	virtual void onDeath();

	virtual bool doesCollide(Entity* other);

	virtual void onCollide(Entity* other);


};

#endif /* COLONY_H_ */
