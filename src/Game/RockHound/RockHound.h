#ifndef ROCKHOUND_H_
#define ROCKHOUND_H_

#include "../GameMode.h"
#include "../../Engine/Button.h"
#include "../../Engine/GUI/Slider.h"
#include <SFML/Audio.hpp>
#include <iostream>
#include "../../Util/XboxController/XboxController.h"

class PlayerMiner;
class SpaceStation;

class RockHound : public GameMode
{
public:
	RockHound();

	bool onMenu();

	void onStart();

	//called before entities tick
	void onTick();

	//called during the gui draw sequence
	void onDraw();

	void onEntityCreated(Entity* e);

	void onEntityDestroyed(Entity* e);

	bool isGameOver();

	void startWarning(std::string newwarning);

	void onLoad();

	void generateWorld(unsigned int worldSize, unsigned int seed);

	PlayerMiner* playerEntity;
	SpaceStation* spaceStation;

	sf::RectangleShape pointer;
	sf::RectangleShape beacon;

	static sf::Texture* guiCrystal;
	static sf::Texture* pointerTexture;
	static sf::Texture* beaconPointer;
	static sf::Texture* guiCash;
	sf::RectangleShape cash;

	Button startButton;
	Button settingsButton;
	Button exitButton;
	float titleBounce;
	bool titleDir;
	sf::Clock gameOverTimer;
	sf::Clock spawnPirate;
	unsigned int pirates;
	static sf::Music spaceamb;

	std::vector<std::string> taglines;
	std::string tagline;

	std::string warning;
	sf::Clock warningTimer;
	bool warningUp;

	static sf::Font* pixel;
	static sf::Sound alarm;
	static sf::SoundBuffer* alarmBuffer;

	static sf::Sound life;
	static sf::SoundBuffer* lifeBuffer;

	bool settingsMenu;
	bool controlsMenu;
	bool miscOptionsMenu;
	//settings menu buttons and stuff
	Button fullScreenButton;
	Button muteButton;
	Button debugButton;
	Button backButton;
	Button controlsButton;
	Button miscOptionsButton;

	Slider musicVolume;
	sf::Text volumeText;

	XboxController controller;

};


#endif /* ROCKHOUND_H_ */
