#include "RockInfo.h"
#include <iostream>

RockInfo RockInfo::small0;
RockInfo RockInfo::small1;
RockInfo RockInfo::small2;
RockInfo RockInfo::small3;
RockInfo RockInfo::small4;
RockInfo RockInfo::small5;
RockInfo RockInfo::small6;
RockInfo RockInfo::small7;
RockInfo RockInfo::small8;

RockInfo RockInfo::large0;
RockInfo RockInfo::large1;
RockInfo RockInfo::large2;
RockInfo RockInfo::large3;
RockInfo RockInfo::large4;


RockInfo::RockInfo()
{
	m_size = RockInfo::Small;
	m_frame = 0;

}

RockInfo::RockInfo(RockInfo::Size size, unsigned int frame)
{
	m_size = size;
	m_frame = frame;

}

RockInfo::Size RockInfo::getSize()
{
	return m_size;

}

unsigned int RockInfo::getFrame()
{
	return m_frame;

}

unsigned int RockInfo::getAttachments()
{
	return m_attachPoints.size();

}

std::vector<sf::Vector2f>& RockInfo::getAttachPoints()
{
	return m_attachPoints;

}

void RockInfo::addAttachPoint(sf::Vector2f point)
{
	m_attachPoints.push_back(point);

}

void RockInfo::setAttachPoints(std::vector<sf::Vector2f> points)
{
	m_attachPoints = points;

}

void RockInfo::setupRockInfos()
{
	//TODO: setup the attach points
	small0 = RockInfo(RockInfo::Small, 0);
	small0.addAttachPoint(sf::Vector2f(0, 0));

	small1 = RockInfo(RockInfo::Small, 1);
	small1.addAttachPoint(sf::Vector2f(0, 0));

	small2 = RockInfo(RockInfo::Small, 2);
	small2.addAttachPoint(sf::Vector2f(0, 0));

	small3 = RockInfo(RockInfo::Small, 3);
	small3.addAttachPoint(sf::Vector2f(0, 0));

	small4 = RockInfo(RockInfo::Small, 4);
	small4.addAttachPoint(sf::Vector2f(0, 0));

	small5 = RockInfo(RockInfo::Small, 5);
	small5.addAttachPoint(sf::Vector2f(0, 0));

	small6 = RockInfo(RockInfo::Small, 6);
	small6.addAttachPoint(sf::Vector2f(0, 0));

	small7 = RockInfo(RockInfo::Small, 7);
	small7.addAttachPoint(sf::Vector2f(0, 0));

	small8 = RockInfo(RockInfo::Small, 8);
	small8.addAttachPoint(sf::Vector2f(0, 0));


	large0 = RockInfo(RockInfo::Large, 0);
	large0.addAttachPoint(sf::Vector2f(0, 0));

	large1 = RockInfo(RockInfo::Large, 1);
	large1.addAttachPoint(sf::Vector2f(0, 0));

	large2 = RockInfo(RockInfo::Large, 2);
	large2.addAttachPoint(sf::Vector2f(0, 0));

	large3 = RockInfo(RockInfo::Large, 3);
	large3.addAttachPoint(sf::Vector2f(0, 0));

	large4 = RockInfo(RockInfo::Large, 4);
	large4.addAttachPoint(sf::Vector2f(0, 0));


}

RockInfo& RockInfo::getInfo(RockInfo::Size size, unsigned int frame)
{
	if(size != RockInfo::Small && size != RockInfo::Large)
	{
		size = RockInfo::Small;

	}

	if(size == RockInfo::Small)
	{
		switch(frame)
		{
		case 0:
			return small0;
		case 1:
			return small1;
		case 2:
			return small2;
		case 3:
			return small3;
		case 4:
			return small4;
		case 5:
			return small5;
		case 6:
			return small6;
		case 7:
			return small7;
		case 8:
			return small8;
		default:
			return small1;

		}

	}
	else
	{
		switch(frame)
		{
		case 0:
			return large0;
		case 1:
			return large1;
		case 2:
			return large2;
		case 3:
			return large3;
		case 4:
			return large4;
		default:
			return large1;

		}

	}

}
