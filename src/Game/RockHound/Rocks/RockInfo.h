#ifndef ROCKINFO_H_
#define ROCKINFO_H_

#include <SFML/Graphics.hpp>

//Holds info about a rock
//Size, frame in image
//attachment points for crystals or other stuff
class RockInfo
{
public:
	typedef enum Size
	{
		Small = 0,
		Large,
		Bit

	} Size;

	RockInfo();
	RockInfo(RockInfo::Size size, unsigned int frame);

	RockInfo::Size getSize();
	unsigned int getFrame();

	unsigned int getAttachments();
	std::vector<sf::Vector2f>& getAttachPoints();
	void addAttachPoint(sf::Vector2f point);
	void setAttachPoints(std::vector<sf::Vector2f> points);

	//static function that fills out the points for the static instances
	//of RockInfo below
	static void setupRockInfos();

	//returns one of the static RockInfo instances below given a size and frame
	static RockInfo& getInfo(RockInfo::Size size, unsigned int frame);

	static RockInfo small0;
	static RockInfo small1;
	static RockInfo small2;
	static RockInfo small3;
	static RockInfo small4;
	static RockInfo small5;
	static RockInfo small6;
	static RockInfo small7;
	static RockInfo small8;

	static RockInfo large0;
	static RockInfo large1;
	static RockInfo large2;
	static RockInfo large3;
	static RockInfo large4;

private:
	RockInfo::Size m_size;
	unsigned int m_frame;
	std::vector<sf::Vector2f> m_attachPoints;

};

#endif /* ROCKINFO_H_ */
