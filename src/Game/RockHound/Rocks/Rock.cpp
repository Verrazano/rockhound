#include "Rock.h"
#include "../../../Engine/Engine.h"
#include "../../../Util/convert/convertstring.h"
#include "../Crystals/Crystal.h"
#include <iostream>
#include <math.h>

sf::Texture* Rock::smallRocks;
sf::Texture* Rock::largeRocks;
sf::Texture* Rock::rockBits;

Rock::Rock(RockInfo info, CrystalInfo::Color crystalColor, sf::Vector2f pos) :
	Entity(-1, pos, sf::Vector2f(150, 150), 20)
{
	loadResources();

	//settings
	addTag("rock");

	updateRate = 30;
	isStatic = true;

	rotVelDrag = 1.0f;

	//members
	m_info = info;

	m_crystalColor = crystalColor;
	m_surfaceCrystals = 0;
	m_crystals = 0;

	m_rot = game->randFrom(0, 3) - 1;
	m_rot /= 4;
	rotVel = m_rot;

	//health
	RockInfo::Size size = m_info.getSize();
	if(size == RockInfo::Small)
	{
		std::cout << "small\n";
		maxHealth = 100;

	}
	else if(size == RockInfo::Large)
	{
		std::cout << "large\n";
		maxHealth = 150;

	}
	else if(size == RockInfo::Bit)
	{
		maxHealth = 50;

	}

	setupRock();

}

std::string Rock::getName()
{
	return "rock";

}

void Rock::onTick()
{
}

void Rock::onDraw()
{
	if(health == maxHealth)
		return;

	float healthratio = health/maxHealth;
	sf::Vector2f pos = getSpritePos();
	pos.y -= 100;
	pos.x -= healthratio*200/2;
	game->drawPane(pos, sf::Vector2f(200*healthratio, 20), sf::Color(255*(1-healthratio), 255*healthratio, 0, 240));

}

void Rock::onDeath()
{
	sf::Vector2f pos = getSpritePos();
	for(unsigned int i = 0; i < m_crystals; i++)
	{
		float angle = rand()%360;
		float rot = game->randFrom(0, 3) - 1;
		rot /= 4;

		CrystalInfo::Type type = (CrystalInfo::Type)((rand()%3) + 2);

		game->addEntity(new Crystal(getCrystalColor(), type,
				pos, 300, angle, rot));

	}

	unsigned int sizeFactor;
	RockInfo::Size size = m_info.getSize();
	if(size == RockInfo::Small)
	{
		sizeFactor = 1;

	}
	else if(size == RockInfo::Large)
	{
		sizeFactor = 2;

	}
	else if(size == RockInfo::Bit)
	{
		sizeFactor = 0;

	}

	emitBits(20*sizeFactor, getAngle(), pos);

}

bool Rock::doesCollide(Entity* other)
{
	return true;

}

void Rock::onCollide(Entity* other)
{
}

RockInfo& Rock::getInfo()
{
	return m_info;

}

CrystalInfo::Color Rock::getCrystalColor()
{
	return m_crystalColor;

}

unsigned int Rock::getSurfaceCrystals()
{
	return m_surfaceCrystals;

}

unsigned int Rock::getCrystals()
{
	return m_crystals;

}

void Rock::emitBits(unsigned int count, float angle, sf::Vector2f pos)
{
	float power = 200;
	for(unsigned int i = 0; i < count; i++)
	{
		unsigned int side = rand()%2;
		if(side == 0)
		{
			angle -= 90;

		}
		else
		{
			angle += 90;

		}

		angle = game->randFrom(-15, 15) + angle;
		angle = angle*3.14159/180;
		sf::Vector2f pos = getSpritePos();
		pos.x += game->randFrom(-30, 30);
		pos.y += game->randFrom(-30, 30);
		game->particles.emit("rock bit", pos, sf::Vector2f(cos(angle)*power, sin(angle)*power), 5);

	}

}

sf::IntRect Rock::getFrame(RockInfo::Size size, unsigned int frame)
{
	if(size == RockInfo::Small)
	{
		if(!(frame < 9))
		{
			frame = 8;

		}

		unsigned int x = frame%3;
		unsigned int y = frame/3;

		return sf::IntRect(x*300, y*300, 300, 300);

	}
	else if(size == RockInfo::Large)
	{
		if(!(frame < 5))
		{
			frame = 4;

		}

		return sf::IntRect(frame*300, 0, 300, 300);

	}
	else if(size == RockInfo::Bit)
	{
		if(!(frame < 5))
		{
			frame = 4;

		}

		return sf::IntRect(frame*64, 0, 64, 64);

	}

	return sf::IntRect(0, 0, 1, 1);

}

void Rock::setupRock()
{
	SceneActor* s = sprite.addChild(SceneActor("rock", smallRocks, getFrame(m_info.getSize(), m_info.getFrame())));
	if(m_info.getSize() == RockInfo::Large)
	{
		//std::cout << "LARGE ROCK\n";
		s->setScale(1.5, 1.5);

	}
	for(unsigned int i = 0; i < m_info.getAttachments(); i++)
	{
		unsigned n = rand()%2;
		if(n == 0)
		{
			sf::Texture* crystalText = NULL;
			if(m_crystalColor == CrystalInfo::Blue)
			{
				crystalText = Crystal::blueCrystal;

			}
			else if(m_crystalColor == CrystalInfo::Green)
			{
				crystalText = Crystal::greenCrystal;

			}
			else if(m_crystalColor == CrystalInfo::Red)
			{
				crystalText = Crystal::redCrystal;

			}
			else if(m_crystalColor == CrystalInfo::Gold)
			{
				crystalText = Crystal::goldCrystal;

			}

			CrystalInfo::Type type = (CrystalInfo::Type)((rand()%2) + 2);
			sf::IntRect frame = Crystal::getFrame(m_crystalColor, type);
			SceneActor* c = sprite.addChild(SceneActor("crystal" + toString(i), crystalText, frame));
			c->setScale(0.7, 0.7);
			c->setRotation(rand()%360);
			m_surfaceCrystals++;

		}

	}

	m_crystals = m_surfaceCrystals;

}

void Rock::loadResources()
{
	static bool loaded = false;
	if(!loaded)
	{
		loaded = true;

		//textures
		RockInfo::setupRockInfos();
		smallRocks = game->files.get<sf::Texture>("smallrocks.png");
		largeRocks = game->files.get<sf::Texture>("largerocks.png");
		rockBits = game->files.get<sf::Texture>("rockbits.png");

		//particle
		ParticleManager& particles = game->particles;
		particles.add("rock bit", rockBits, sf::Vector2u(64, 64), 250);


	}

}
