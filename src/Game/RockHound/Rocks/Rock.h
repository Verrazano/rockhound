#ifndef ROCK_H_
#define ROCK_H_

//rock (aka asteroid for mining)
//temp sprite todo make sprites (using the rocks for now for frame of reference for test driving the mining ship)

#include "../../../Entity/Entity.h"
#include "RockInfo.h"
#include "../Crystals/Crystal.h"

class Rock : public Entity
{
public:
	Rock(RockInfo info, CrystalInfo::Color crystalColor, sf::Vector2f pos);

	//returns crystalColor + name, i.e: blue rock
	std::string getName();

	//Do nothing
	void onTick();

	//Draw health bar
	void onDraw();

	//explode into rockbits
	//create crystals
	void onDeath();

	//will always collide if the other entity wants to
	bool doesCollide(Entity* other);

	void onCollide(Entity* other);

	RockInfo& getInfo();
	CrystalInfo::Color getCrystalColor();
	unsigned int getSurfaceCrystals();
	unsigned int getCrystals();

	void emitBits(unsigned int count, float angle, sf::Vector2f pos);

	static sf::IntRect getFrame(RockInfo::Size size, unsigned int frame);

	static sf::Texture* smallRocks;
	static sf::Texture* largeRocks;
	static sf::Texture* rockBits;

private:
	void setupRock();

	RockInfo m_info;

	CrystalInfo::Color m_crystalColor;
	unsigned int m_surfaceCrystals;
	unsigned int m_crystals;

	float m_rot;

	static void loadResources();

};


#endif /* ROCK_H_ */
