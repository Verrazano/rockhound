#include "RockHound.h"
#include "../../Engine/Engine.h"
#include "Miner.h"
#include <time.h>
#include "Rocks/Rock.h"
#include "PlayerMiner.h"
#include "SpaceStation.h"
#include <math.h>
#include "Crystals/Crystal.h"
#include "Pirate.h"
#include "Generator/Sector.h"
#include "Generator/Generator.h"
#include "Biomes/RocksBiome.h"
#include "Biomes/EmptySpace.h"
#include "Biomes/MineField.h"
#include "Biomes/StargateBiome.h"
#include <sstream>

sf::Texture* RockHound::guiCrystal;
sf::Texture* RockHound::pointerTexture;
sf::Texture* RockHound::beaconPointer;
sf::Texture* RockHound::guiCash;
sf::Font* RockHound::pixel;
sf::Music RockHound::spaceamb;
sf::Sound RockHound::alarm;
sf::SoundBuffer* RockHound::alarmBuffer;
sf::Sound RockHound::life;
sf::SoundBuffer* RockHound::lifeBuffer;

RockHound::RockHound()
{
	static bool loaded = false;
	if(!loaded)
	{
		loaded = true;
		guiCrystal = game->files.get<sf::Texture>("crystalgui.png");
		pointerTexture = game->files.get<sf::Texture>("pointer.png");
		beaconPointer = game->files.get<sf::Texture>("beaconbotpointer.png");
		guiCash = game->files.get<sf::Texture>("cash.png");
		pixel = game->files.get<sf::Font>("m04.ttf");
		spaceamb.openFromFile("res/space_0.wav");
		alarmBuffer = game->files.get<sf::SoundBuffer>("alarm.wav");
		alarm.setBuffer(*alarmBuffer);
		alarm.setVolume(50.0f);
		spaceamb.setLoop(true);
		spaceamb.setVolume(100.0f);

		lifeBuffer = game->files.get<sf::SoundBuffer>("lifepickup.wav");
		life.setBuffer(*lifeBuffer);

		game->inputs.addButton([this]()->bool{return this->controller.buttonStart();}, "cStart");
		game->inputs.addButton([this]()->bool{return this->controller.rightTrigger();}, "cRightTrigger");
		game->inputs.addValue([this]()->float{return this->controller.leftStickX();}, "cLeftX");
		game->inputs.addValue([this]()->float{return this->controller.leftStickY();}, "cLeftY");
		game->inputs.addButton([this]()->bool{return this->controller.isConnected();}, "cConnected");

	}

	playerEntity = NULL;

	spaceamb.play();

	pointer.setSize(sf::Vector2f(143/2, 210/2));
	pointer.setOrigin(sf::Vector2f(143/4, 0));
	pointer.setTexture(pointerTexture);
	pointer.setFillColor(sf::Color(255, 255, 255, 100));

	beacon.setSize(sf::Vector2f(95/2, 135/2));
	beacon.setOrigin(sf::Vector2f(95/4, 0));
	beacon.setTexture(beaconPointer);
	beacon.setFillColor(sf::Color(255, 255, 255, 100));

	cash.setSize(sf::Vector2f(43, 78));
	cash.setOrigin(43/2, 78/2);
	cash.setTexture(guiCash);

	startButton =
			Button(sf::Vector2f(game->guiWidth/2, game->guiHeight/2 - 48), sf::Vector2f(64*3, 48), "Start Game", *pixel);
	settingsButton =
			Button(sf::Vector2f(game->guiWidth/2, game->guiHeight/2 + 96 - 48), sf::Vector2f(64*3, 48), "Settings", *pixel);

	exitButton =
			Button(sf::Vector2f(game->guiWidth/2, game->guiHeight/2 + 192 - 48), sf::Vector2f(64*3, 48), "Exit", *pixel);

	//settings menu buttons

	settingsMenu = false;

	backButton =
			Button(sf::Vector2f(game->guiWidth/2, game->guiHeight - 96), sf::Vector2f(64*3, 48), "Back", *pixel);

	std::string fscreenname = "Fullscreen: ";
	if(game->settings.fullScreen)
	{
		fscreenname += "On";

	}
	else
	{
		fscreenname += "Off";

	}

	fullScreenButton =
			Button(sf::Vector2f(96*2, 96), sf::Vector2f(64*3, 48), fscreenname, *pixel);

	std::string mutename = "Mute: ";
	if(game->settings.muteSound)
	{
		mutename += "On";

	}
	else
	{
		mutename += "Off";

	}

	muteButton =
			Button(sf::Vector2f(96*2, 96*2), sf::Vector2f(64*3, 48), mutename, *pixel);

	std::string debugname = "Debug: ";
	if(game->settings.debug)
	{
		debugname += "On";

	}
	else
	{
		debugname += "Off";

	}

	debugButton =
			Button(sf::Vector2f(96*2, 96*3), sf::Vector2f(64*3, 48), debugname, *pixel);

	controlsButton =
				Button(sf::Vector2f(96*5, 96), sf::Vector2f(64*3, 48), "Controls", *pixel);
	controlsMenu = false;

	miscOptionsButton =
			Button(sf::Vector2f(96*5, 96*2), sf::Vector2f(64*3, 48), "Misc.", *pixel);
	miscOptionsMenu = false;

	musicVolume =
			Slider(sf::Vector2f(96*7.5, 96*1), sf::Vector2f(64*3, 48), 100);
	musicVolume.setValue(game->settings.volume);

	volumeText.setFont(*pixel);
	volumeText.setColor(sf::Color::White);
	volumeText.setCharacterSize(14);
	std::string vs = "100";
	{
		std::stringstream ss;
		ss << game->settings.volume;
		ss >> vs;

	}
	volumeText.setString("Volume: " + vs + "%");
	sf::FloatRect vrect = volumeText.getLocalBounds();
	volumeText.setPosition((96*7.5) - vrect.width/2, (96*1)-vrect.height/2);

	titleBounce = -16;
	titleDir = false;
	taglines.push_back("crystals, crystals everywhere.");
	taglines.push_back("collin is a derp face.");
	taglines.push_back("part of a healthy breakfast.");
	taglines.push_back("rocks are crunchy.");
	taglines.push_back("pirates are mean.");
	taglines.push_back("moving takes up a lot of time.");
	taglines.push_back("ld is fun, but hard.");
	taglines.push_back("do you even mine bro?");
	taglines.push_back("upgrade your stuff!");
	taglines.push_back("I ain't got time for that");
	taglines.push_back("such space, much empty, very wow");
	taglines.push_back("wow are those stars pixels?");
	taglines.push_back("yes");
	taglines.push_back("no");
	taglines.push_back("all hail king of the losers");
	taglines.push_back("shout out to baby miller");
	taglines.push_back("pokemon is still a game");
	taglines.push_back("irc or die");
	taglines.push_back("c++ 4lyf");
	taglines.push_back("inb4 motherload... not");
	taglines.push_back("chump");
	taglines.push_back("ur a chump");
	taglines.push_back("charlie is a chump");
	taglines.push_back("#justchumpthings");

	tagline = taglines[rand()%taglines.size()];

}

bool RockHound::onMenu()
{
	game->setViewTarget(sf::Vector2f(0, 0));
	std::vector<Entity*> crystals = game->entities.getEntitiesWithTag("crystal");
	unsigned int needed = 10 - crystals.size();
	for(unsigned int i = 0; i < needed; i++)
	{
		Entity* crystal = new Crystal(CrystalInfo::Blue, CrystalInfo::Full, sf::Vector2f(0, 0), rand()%50, rand()%360, rand()%3-1);
		crystal->remTag("shatter");
		game->addEntity(crystal);

	}

	//hamdle music
	spaceamb.setVolume(100*game->settings.volume);

	if(spaceamb.getStatus() == sf::Sound::Playing && game->settings.muteSound)
	{
		spaceamb.pause();

	}
	else if(spaceamb.getStatus() == !sf::Sound::Playing && !game->settings.muteSound)
	{
		spaceamb.play();

	}

	if(controlsMenu)
	{
		if(backButton.draw(game->window))
		{
			controlsMenu = false;

		}

	}
	else if(miscOptionsMenu)
	{
		if(backButton.draw(game->window))
		{
			miscOptionsMenu = false;

		}

	}
	else if(!settingsMenu)
	{
	//game->window.draw(pane);

		if(startButton.draw(game->window) || game->inputs.isState("cStart", ButtonCallback::justPressed))
		{
			return true;

		}

		if(settingsButton.draw(game->window))
		{
			settingsMenu = true;

		}

		if(exitButton.draw(game->window))
		{
			game->onClose();

		}

		if(titleDir && titleBounce < 7)
		{
			titleBounce += 0.5;

		}
		else if(titleDir && titleBounce >= 7)
		{
			titleDir = false;

		}
		else if(!titleDir && titleBounce > -7)
		{
			titleBounce += -0.5;

		}
		else if(!titleDir && titleBounce <= -7)
		{
			titleDir = true;

		}

		//std::string text, sf::Vector2f pos, sf::Font& font, sf::Color color, float a, unsigned int size
		game->drawText("Rock Hound", sf::Vector2f(game->guiWidth/2, 50), *RockHound::pixel, sf::Color::White, titleBounce, 20, true);
		game->drawText(tagline, sf::Vector2f(game->guiWidth/2, 100), *RockHound::pixel, sf::Color::White, 0, 14, true);

	}
	else
	{
		if(backButton.draw(game->window))
		{
			settingsMenu = false;
			game->settings.write("settings.cfg");

		}

		if(fullScreenButton.draw(game->window))
		{
			std::string fscreenname = fullScreenButton.btext.getString().toAnsiString();
			if(fscreenname == "Fullscreen: On")
			{
				fscreenname = "Fullscreen: Off";
				game->settings.fullScreen = false;

			}
			else
			{
				fscreenname = "Fullscreen: On";
				game->settings.fullScreen = true;


			}

			fullScreenButton.btext.setString(fscreenname);

		}

		if(controlsButton.draw(game->window))
		{
			controlsMenu = true;

		}

		if(miscOptionsButton.draw(game->window))
		{
			miscOptionsMenu = true;

		}

		if(muteButton.draw(game->window))
		{
			std::string mutename = muteButton.btext.getString().toAnsiString();
			if(mutename == "Mute: On")
			{
				mutename = "Mute: Off";
				game->settings.muteSound = false;

			}
			else
			{
				mutename = "Mute: On";
				game->settings.muteSound = true;


			}

			muteButton.btext.setString(mutename);

		}

		if(musicVolume.draw(game->window))
		{
			std::stringstream ss;
			ss << (int)musicVolume.getValue();
			std::string v;
			ss >> v;
			volumeText.setString("Volume: " + v + "%");
			game->settings.volume = (float)(int)musicVolume.getValue();

		}
		game->window.draw(volumeText);

		if(debugButton.draw(game->window))
		{
			std::string debugname = debugButton.btext.getString().toAnsiString();
			if(debugname == "Debug: On")
			{
				debugname = "Debug: Off";
				game->settings.debug = false;

			}
			else
			{
				debugname = "Debug: On";
				game->settings.debug = true;


			}

			debugButton.btext.setString(debugname);

		}

	}

	return false;

}

void RockHound::onStart()
{
	generateWorld(game->settings.worldSize, game->settings.seed);
	pointer.setPosition(sf::Vector2f(game->guiWidth/2, 100));
	beacon.setPosition(sf::Vector2f(game->guiWidth/2 + 100, 100));
	std::string id = game->addEntity(new PlayerMiner(sf::Vector2f(300, 0)));
	playerEntity = dynamic_cast<PlayerMiner*>(game->entities.getEntity(id));

	id = game->addEntity(new SpaceStation(sf::Vector2f(0, 0)));
	spaceStation = dynamic_cast<SpaceStation*>(game->entities.getEntity(id));
	//playerEntity->addTag("player");
	game->setViewTarget(playerEntity);

	pirates = 0;
	gameOverTimer.restart();
	spawnPirate.restart();

}

//called before entities tick
void RockHound::onTick()
{
	if(playerEntity != NULL)
		gameOverTimer.restart();

	spaceamb.setVolume(100*game->settings.volume);

	if(spaceamb.getStatus() == sf::Sound::Playing && game->settings.muteSound)
	{
		spaceamb.pause();

	}
	else if(spaceamb.getStatus() == !sf::Sound::Playing && !game->settings.muteSound)
	{
		spaceamb.play();

	}

}

//called during the gui draw sequence
void RockHound::onDraw()
{
	if(playerEntity != NULL)
	{
		/*std::cout << "it's happening\n";
		float dist = dynamic_cast<PlayerMiner*>(playerEntity)->distToTarget();
		sf::Vector2f target(game->viewWidth/2, game->viewHeight/2);
		target.y -= dist;
		game->drawPane(target, sf::Vector2f(50, 50), sf::Color(255, 0, 0, 80));*/

		float fuelRatio = playerEntity->fuel / playerEntity->maxFuel;
		float fullHeight = 260;
		fullHeight *= fuelRatio;
		sf::Vector2f fuelPos = sf::Vector2f(20, game->guiHeight - fullHeight - 10);
		game->drawPane(fuelPos, sf::Vector2f(20, fullHeight), sf::Color(204, 204, 0, 180));
		fuelPos.y += fullHeight - 20;
		fuelPos.x += 2;
		game->drawText(playerEntity->getFuelInfo(), fuelPos, *pixel, sf::Color::White, -90);

		if(playerEntity == NULL)
			return;

		float healthRatio = playerEntity->health / playerEntity->maxHealth;
		float fullLen = 260;
		fullLen *= healthRatio;
		sf::Vector2f healthPos = sf::Vector2f(game->guiWidth - game->guiWidth/2, game->guiHeight - 50);
		healthPos.y += 10;
		game->drawPane(healthPos, sf::Vector2f(fullLen, 20), sf::Color(0, 200, 0, 180));
		healthPos.y += 2;
		game->drawText(playerEntity->getHealthInfo(), healthPos, *pixel, sf::Color::White);

		if(playerEntity == NULL)
			return;

		sf::Vector2f target = playerEntity->getTarget();
		target -= playerEntity->getSpritePos();
		target.x += game->guiWidth/2;
		target.y += game->guiHeight/2;
		game->drawPane(target, sf::Vector2f(12, 12), sf::Color(255, 0, 0, 80));

		std::string crystalInfo = playerEntity->getCrystalInfo();
		sf::Vector2f crystalInfoPos = sf::Vector2f(50, game->guiHeight-60);
		game->drawSprite(crystalInfoPos, guiCrystal);
		crystalInfoPos.x += 70;
		crystalInfoPos.y += 20;
		game->drawText(crystalInfo, crystalInfoPos, *pixel, sf::Color::White);

		crystalInfoPos.x += 150;
		crystalInfoPos.y -= 10;
		cash.setPosition(crystalInfoPos);
		game->window.draw(cash);
		crystalInfoPos.x += 50;
		crystalInfoPos.y += 10;
		game->drawText(playerEntity->getCreditsInfo(), crystalInfoPos, *pixel, sf::Color::White);

		healthPos.y -= 40;
		game->drawText(playerEntity->getItemInfo(), healthPos, *pixel, sf::Color::White);

		if(fuelRatio <= 0.175 && !warningUp)
		{
			startWarning("Low on fuel");

		}

		if(spaceStation != NULL)
		{
			sf::Vector2f ppos = playerEntity->getSpritePos();
			sf::Vector2f spos = spaceStation->getSpritePos();
			//885*0.7, 708*0.7
			spos.x += 885/2*0.7;
			spos.y += 708/2*0.7;
			sf::Vector2f dif = spos - ppos;
			float angle = atan2f(dif.y, dif.x) + 90;
			angle = angle*180/3.14159;

			pointer.setRotation(angle);
			game->window.draw(pointer);

			spaceStation->handleShop();

		}

		std::vector<Entity*> beacons = game->entities.getEntitiesWithName("beaconbot");
		if(beacons.size() > 0)
		{
			Entity* beaconE = beacons[0];
			sf::Vector2f ppos = playerEntity->getSpritePos();
			sf::Vector2f spos = beaconE->getSpritePos();
			sf::Vector2f dif = spos - ppos;
			float angle = atan2f(dif.y, dif.x) + 90;
			angle = angle*180/3.14159;

			beacon.setRotation(angle);
			game->window.draw(beacon);

		}

	}

	if(warningUp)
	{
		float warningTime = warningTimer.getElapsedTime().asSeconds();
		if(warningTime > 8)
		{
			warningUp = false;

		}

		float ratio = 5/warningTime;
		if(ratio > 1)
		{
			ratio = 1;

		}

		sf::Color warnColor;
		warnColor = sf::Color(255, 0, 0, 255*ratio);

		game->drawText(warning, sf::Vector2f(game->guiWidth/2, game->guiHeight/2 + 150), *RockHound::pixel, warnColor, 0, 20, true);

	}

	//4, 60
	float pirateTime = 60*(pirates/2);
	if(pirateTime == 0)
		pirateTime = 60.0f;
	if(pirates < 4 && spawnPirate.getElapsedTime().asSeconds() >= pirateTime)
	{
		startWarning("Pirate seen in sector");
		float worldSize = game->settings.worldSize;
		sf::Vector2f pos(game->randFrom(-worldSize, worldSize), game->randFrom(-worldSize, worldSize));
		game->addEntity(new Pirate(pos));
		pirates++;

	}

}

void RockHound::onEntityCreated(Entity* e)
{
	if(e->hasTag("player"))
	{
		game->out("player created");

	}

}

void RockHound::onEntityDestroyed(Entity* e)
{
	if(e->hasTag("player"))
	{
		game->out("player killed");
		playerEntity = NULL;

	}

	if(e->getName() == "spacestation")
	{
		spaceStation = NULL;

	}

	if(e->getName() == "pirate")
	{
		pirates--;
		spawnPirate.restart();

	}

}

bool RockHound::isGameOver()
{

	if(gameOverTimer.getElapsedTime().asSeconds() > 2.0f)
	{
		return true;

	}

	return false;

}

void RockHound::startWarning(std::string newwarning)
{
	warning = newwarning;
	warningUp = true;
	warningTimer.restart();

	if(!game->settings.muteSound)
	{
		alarm.setVolume(50*game->settings.volume);
		alarm.play();

	}

}

void RockHound::onLoad()
{
	float length = 300;
	length *= loadPercent;

	sf::Vector2f center(game->guiWidth/2, game->guiHeight/2);
	game->drawPane(sf::Vector2f(center.x - length/2, center.y-30), sf::Vector2f(length, 60), sf::Color(0, 255, 0, 180));

	float percent = loadPercent*100;
	game->drawText("%"+toString((int)percent), center, *RockHound::pixel, sf::Color::White, 0, 14, true);

	game->drawText(loadPhase, sf::Vector2f(center.x, center.y - 200), *RockHound::pixel, sf::Color::White, 0, 14, true);


}

void RockHound::generateWorld(unsigned int worldSize, unsigned int seed)
{
	game->entities.clear();

	Generator gen;
	gen.addBiome(new EmptySpace(200));
	gen.addBiome(new MineField(10));//10
	gen.addBiome(new RocksBiome(CrystalInfo::Blue, 60));
	gen.addBiome(new RocksBiome(CrystalInfo::Green, 30));
	gen.addBiome(new RocksBiome(CrystalInfo::Red, 16));
	gen.addBiome(new RocksBiome(CrystalInfo::Gold, 5));
	gen.addBiome(new StargateBiome(25, 2));

	gen.generate(worldSize, seed);

}
