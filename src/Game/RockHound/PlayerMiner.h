#ifndef PLAYERMINER_H_
#define PLAYERMINER_H_

#include "Miner.h"
#include <iostream>

class PlayerMiner : public Miner
{
public:
	PlayerMiner(sf::Vector2f pos);

	void onTick();

	virtual bool isPressingThrottle();
	virtual int isPressingDir();

	virtual float getThrottle();
	virtual sf::Vector2f getTarget();

	float distToTarget();

	void onDeath();

	std::string getCreditsInfo();
	std::string getHealthInfo();
	std::string getItemInfo();

	sf::Vector2f target;

	unsigned int credits;

	sf::Sound chaching;
	static sf::SoundBuffer* chachingBuffer;

	Entity* item;

};

#endif /* PLAYERMINER_H_ */
