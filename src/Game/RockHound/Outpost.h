#ifndef OUTPOST_H_
#define OUTPOST_H_

#include "Ship.h"
#include "PlayerMiner.h"

class Outpost : public Ship
{
public:
	Outpost(sf::Vector2f pos, sf::Vector2f target);

	std::string getName();

	void onTick();

	void onDraw();

	void onDeath();

	bool doesCollide(Entity* other);

	void onCollide(Entity* other);

	bool isPressingThrottle();

	int isPressingDir();

	float getThrottle();
	sf::Vector2f getTarget();

	PlayerMiner* player;
	static sf::Texture outpostText;

	sf::Vector2f target;
	bool done;

};

#endif /* OUTPOST_H_ */
