#include "Explosion.h"
#include "../../Engine/Engine.h"
#include <math.h>
#include <iostream>

Explosion::Explosion(){}

Explosion::Explosion(Entity* entity, float range, float knockback, float damage)
{
	this->entity = entity;
	this->range = range;
	this->knockback = knockback;
	this->damage = damage;
	list = 2;

}

void Explosion::explode()
{
	if(entity == NULL)
	{
		return;

	}

	std::vector<Entity*> entities;
	entities = game->entities.getEntitiesWithinDist(range, entity);
	std::vector<Entity*>::iterator it;
	std::cout << "exploding\n";

	if(list == 2)
	{
		for(it = entities.begin(); it != entities.end(); it++)
		{
			if(list == 0)
			{
				Entity* ent = *it;
				for(unsigned int i = 0; i < blacklist.size(); i++)
				{
					if(ent->hasTag(blacklist[i]) || ent->getName() == blacklist[i])
					{
						entities.erase(it);
						it--;
						break;

					}

				}

			}
			else if(list == 1)
			{
				Entity* ent = *it;
				for(unsigned int i = 0; i < whitelist.size(); i++)
				{
					if(!ent->hasTag(whitelist[i]) || ent->getName() != whitelist[i])
					{
						entities.erase(it);
						it--;
						break;

					}

				}

			}

		}

	}

	for(unsigned int i = 0; i < entities.size(); i++)
	{
		float dist = entity->distanceTo(entities[i]);
		float distRatio = dist/range;
		distRatio = 1 - distRatio;

		float knockbackR = knockback*distRatio;
		float damageR = damage*distRatio;

		sf::Vector2f diff = entities[i]->getSpritePos();
		diff -= entity->getSpritePos();
		float a = atan2f(diff.y, diff.x);
		a = a*180/3.14519;
		entities[i]->health -= damageR;
		entities[i]->addForce(a, knockbackR);
		std::cout << "exploding: " << entities[i]->getName() << " with: " << knockbackR << "\n";

	}

}

void Explosion::setWhiteList(std::vector<std::string> listv)
{
	whitelist = listv;
	list = 1;

}

void Explosion::setBlackList(std::vector<std::string> listv)
{
	blacklist = listv;
	list = 0;

}
