#ifndef STARGATE_H_
#define STARGATE_H_

#include "../../Entity/Entity.h"

class Stargate : public Entity
{
public:
	Stargate(sf::Vector2f pos, float a);

	std::string getName();

	void onTick();

	void onDraw();

	void onDeath();

	bool doesCollide(Entity* other);

	void onCollide(Entity* other);

	void setLink(std::string link);

	void emitBits(unsigned int count);

	std::string idlink;

};

#endif /* STARGATE_H_ */
