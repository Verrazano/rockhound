#ifndef PIRATE_H_
#define PIRATE_H_

#include "Ship.h"
#include "PlayerMiner.h"

class Pirate : public Ship
{
public:
	Pirate(sf::Vector2f pos);

	virtual std::string getName();

	virtual void onTick();

	virtual void onDraw();

	virtual void onDeath();

	virtual bool doesCollide(Entity* other);

	virtual void onCollide(Entity* other);

	virtual bool isPressingThrottle();

	//0 no dir, -1 left, 1 right
	virtual int isPressingDir();

	virtual float getThrottle();
	virtual sf::Vector2f getTarget();

	PlayerMiner* player;
	bool canFollow;
	sf::Vector2f target;
	sf::Clock shootTimer;

	static sf::Texture* pirateTexture;

};


#endif /* PIRATE_H_ */
