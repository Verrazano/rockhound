#ifndef BEACONBOT_H_
#define BEACONBOT_H_

#include "../../Entity/Entity.h"

class BeaconBot : public Entity
{
public:
	BeaconBot(sf::Vector2f pos);

	std::string getName();

	void onTick();

	void onDraw();

	void onDeath();

	bool doesCollide(Entity* other);

	void onCollide(Entity* other);

	static sf::Texture* beaconbotText;
	float turn;

};

#endif /* BEACONBOT_H_ */
