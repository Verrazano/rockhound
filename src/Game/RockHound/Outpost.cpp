#include "Outpost.h"

sf::Texture Outpost::outpostText;

Outpost::Outpost(sf::Vector2f pos, sf::Vector2f target) :
		Ship(2, pos, sf::Vector2f(228/2, 246/2), true, 0.001, 0.001, 0.01, 0.01, 1, 4)
{
	static bool loaded =false;
	if(!loaded)
	{
		loaded = true;
		outpostText.loadFromFile("outpost.png");

	}

	//addSprite("main", &outpostText);
	this->target = target;
	done = false;
	updateRate = 2;

}

std::string Outpost::getName()
{
	return "outpost";

}

void Outpost::onTick()
{

}

void Outpost::onDraw()
{

}

void Outpost::onDeath()
{

}

bool Outpost::doesCollide(Entity* other)
{

}

void Outpost::onCollide(Entity* other)
{

}

bool Outpost::isPressingThrottle()
{

}

//0 no dir, -1 left, 1 right
int Outpost::isPressingDir()
{

}

float Outpost::getThrottle()
{

}

sf::Vector2f Outpost::getTarget()
{

}
