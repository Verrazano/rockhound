#ifndef EXPLOSION_H_
#define EXPLOSION_H_

#include <string>
#include <vector>
#include "../../Entity/Entity.h"

class Explosion
{
public:
	Explosion();
	Explosion(Entity* entity, float range, float knockback, float damage);

	void explode();

	std::vector<std::string> whitelist; //names or tags
	std::vector<std::string> blacklist; //names or tags;
	int list;//0 = blacklist, 1 = whitelist, 2 = neither
	//2 by default

	void setWhiteList(std::vector<std::string> listv);
	void setBlackList(std::vector<std::string> listv);

	Entity* entity;
	float range;
	float knockback;
	float damage;

};

#endif /* EXPLOSION_H_ */
