#include "Ship.h"
#include <math.h>
#include <iostream>
#include "../../Engine/Engine.h"

sf::SoundBuffer* Ship::engineBuffer;

Ship::Ship(float z, sf::Vector2f pos, sf::Vector2f size, bool fullControl, float speed, float turnSpeed, float maxThro, float maxTurn,
		float mass, float maxVel, float drag) :
		Entity(z, pos, size, mass, maxVel, drag)
{
	this->fullControl = fullControl;
	this->speed = speed;
	this->turnSpeed = turnSpeed;
	this->maxThrottle = maxThro;
	this->maxTurn = maxTurn;
	this->addTag("ship");
	turn = 0.0f;
	throttle = 0.0f;

	static bool loaded = false;
	if(!loaded)
	{
		loaded = true;
		game->particles.add("smoke", sf::Color(200, 200, 200, 50), 15, sf::Vector2u(30, 30), 5000);
		engineBuffer =  game->files.get<sf::SoundBuffer>("engine.ogg");

	}

	engine.setBuffer(*engineBuffer);
	engine.setLoop(true);

	if(game->settings.muteSound)
		return;

	engine.play();
	engine.setVolume(0.0f);

}

Ship::~Ship()
{
}

void Ship::updateShip()
{
	if(!fullControl)
	{
		if(isPressingThrottle())
		{
			throttle += speed;


		}
		else
		{
			throttle = 0.0f;

		}

		if(throttle >= maxThrottle)
		{
			throttle = maxThrottle;

		}

		int dir = isPressingDir();
		if(dir == -1)//left
		{
			turn -= turnSpeed;

		}
		else if(dir == 1)
		{
			turn += turnSpeed;

		}
		else
		{
			turn = 0.0f;

		}

		if(fabs(turn) >= maxTurn)
		{
			turn = turn/fabs(turn)*maxTurn;

		}


		setAngle(getAngle() + turn);
		addForce(getAngle(), throttle);

		if(game->frame%2 == 0)
		{
			unsigned int parts = throttle*3;
			if(parts > 10)
				parts = 10;
			emitSmoke(parts);

		}

	}
	else
	{
		if(engine.getStatus() == sf::Sound::Playing && game->settings.muteSound)
		{
			engine.stop();

		}
		else if(engine.getStatus() != sf::Sound::Playing && !game->settings.muteSound)
		{
			engine.play();

		}

		sf::Vector2f target = getTarget();
		//std::cout << "target: " << target.x << ", " << target.y << "\n";
		float mthrottle = getThrottle();

		if(!game->settings.muteSound)
		{

			if(hasTag("player"))
			{
				float mag = sqrt(pow(velocity.x, 2) + pow(velocity.y, 2));
				float vol = (mag/maxVelocity*100/2)-30;
				if(vol < 0)
					vol = 0;
				if(vol > 100)
					vol = 100;
				//std::cout << "vol: " << vol << "\n";
				float pitch = (throttle/maxThrottle*2);

				//std::cout << "pitcj: " << pitch << "\n";
				engine.setPitch(pitch);
				engine.setVolume(vol*game->settings.volume);

			}

		}

		sf::Vector2f pos = getSpritePos();
		sf::Vector2f dif = target - pos;
		float errorAngle = atan2(dif.y, dif.x);
		//std::cout << "errorAngle: " << errorAngle << "\n";
		errorAngle = (errorAngle*180/3.14159);
		if (errorAngle < 0.0f)
		{
			errorAngle = 360.0f - fabs(errorAngle);

		}

		errorAngle = errorAngle - getAngle();

		if(fabs(errorAngle) > 180.0f)
		{
			float dir = errorAngle/fabs(errorAngle);
			errorAngle = fabs(errorAngle) - 360.0f;
			errorAngle *= dir;

		}

		errorAngle *= turnSpeed;

		throttle += mthrottle*speed;
		if(mthrottle == 0)
		{
			throttle = 0.0f;

		}

		if(fabs(throttle) >= maxThrottle)
		{
			throttle = maxThrottle*(throttle/fabs(throttle));

		}

		setAngle(getAngle() + errorAngle);
		addForce(getAngle(), throttle);

		if(game->frame%2 == 0)
		{
			unsigned int parts = throttle*3;
			if(parts > 10)
				parts = 10;
			emitSmoke(parts);

		}

	}

}

void Ship::emitSmoke(unsigned int count)
{
	float a = getAngle();
	float power = 12;
	for(unsigned int i = 0; i < count; i++)
	{
		float a1 = game->randFrom(-16, 16) + a - 180;
		a1 = a1*3.14159/180;
		sf::Vector2f pos = getSpritePos();
		pos.x += game->randFrom(-15, 15);
		pos.y += game->randFrom(-15, 15);
		game->particles.emit("smoke", pos, sf::Vector2f(cos(a1)*throttle*power, sin(a1)*throttle*power), 3);

	}

}
