#ifndef MINER_H_
#define MINER_H_

#include "Ship.h"
#include "../../Entity/EntityManager/EntityManager.h"

class Miner : public Ship
{
public:
	Miner(float z, sf::Vector2f pos, bool fullControl);

	virtual ~Miner();

	virtual std::string getName();

	//call this if you override the miner class
	virtual void onTick();

	virtual void onDraw();

	virtual void onDeath();

	//collide with walls always
	//collide with rocks if NOT mining
	//otherwise don't collide
	virtual bool doesCollide(Entity* other);

	virtual void onCollide(Entity* other);

	void setMining(bool state);
	bool isMining();

	//please override these they are just defaults that return uselessness
	virtual bool isPressingThrottle();
	virtual int isPressingDir();

	virtual float getThrottle();
	virtual sf::Vector2f getTarget();

	void updateFuel();

	void emitRockBits(unsigned int count);

	float maxFuel;
	float fuel;
	float drillStrength;
	unsigned int storage;
	bool mining;

	std::string getCrystalInfo();
	std::string getFuelInfo();

	typedef enum Model
	{
		M3k = 0,
		MMk4,
		MMk5,
		MMk6,
		tugBoat

	} Model;

	float mspeed;

	void setModel(Model nmodel);

	Model mymodel;

	static sf::Texture* minerTexture;
	static sf::Texture* mark4;
	static sf::Texture* mark5;
	static sf::Texture* mark6;

	static sf::Sound pickup;
	static sf::SoundBuffer pickupBuffer;
	//test
	//static sf::Texture gunTexture;

	static sf::Texture* tugboat;
	static sf::Texture* engine;
	static sf::Texture* drill;

	sf::Clock drillTimer;
	sf::Clock drillAnim;
	int drillframe;

	EntityManager inv;

};

#endif /* MINER_H_ */
