#include "GameMode.h"
#include "../Engine/Engine.h"

void GameMode::load(std::string loadPhase, float loadPercent)
{
	this->loadPhase = loadPhase;
	this->loadPercent = loadPercent;
	game->window.clear(sf::Color(104, 40, 96));
	game->window.setView(game->guiView);
	game->window.draw(game->stars);
	onLoad();
	game->window.display();

}
