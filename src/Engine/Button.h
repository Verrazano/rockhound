#ifndef BUTTON_H_
#define BUTTON_H_

#include <SFML/Graphics.hpp>

class Button
{
public:
	Button(){}
	Button(sf::Vector2f pos, sf::Vector2f size, std::string text, sf::Font& font);
	Button(sf::Vector2f pos, sf::Vector2f size, sf::Texture* iconText, sf::Vector2f iconSize);
	Button(sf::Vector2f pos, sf::Vector2f size, sf::Texture* iconText, sf::Vector2f iconSize, std::string text, sf::Font& font);

	//returns true if just clicked (uses engine to determine junk)
	bool draw(sf::RenderWindow& window);

	sf::RectangleShape button;
	sf::RectangleShape highlight;
	sf::RectangleShape icon;
	sf::Text btext;

};


#endif /* BUTTON_H_ */
