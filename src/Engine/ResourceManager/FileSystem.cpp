#include "FileSystem.h"

FileSystem::FileSystem()
{
}

FileSystem::~FileSystem()
{
}

void FileSystem::search(std::string path)
{
	poll(path);

}

void FileSystem::reset()
{
	m_paths.clear();

}

bool FileSystem::hasPath(std::string name)
{
	return getIndex(name) < getNumOfPaths();

}

std::string FileSystem::fullPath(std::string name)
{
	if(!hasPath(name))
	{
		return name;

	}

	return getPath(getIndex(name));

}

unsigned int FileSystem::getNumOfPaths()
{
	return m_paths.size();

}

std::string FileSystem::getPath(unsigned int index)
{
	if(index >= getNumOfPaths())
	{
		return "";

	}

	return m_paths[index];

}

unsigned int FileSystem::getIndex(std::string name)
{
	for(unsigned int i = 0; i < m_paths.size(); i++)
	{
		std::string path = m_paths[i];
		if(path == name || path.rfind(name) < path.size())
		{
			return i;

		}

	}

	return getNumOfPaths();

}

std::vector<std::string> FileSystem::getPaths()
{
	return m_paths;

}

void FileSystem::setPaths(std::vector<std::string> paths)
{
	m_paths = paths;

}

DIR* FileSystem::isFolder(std::string path)
{
	DIR* directory;
	directory = opendir(path.c_str());
	return directory;

}

std::string FileSystem::getExtension(std::string path)
{
	return path.substr(path.rfind(".")+1);

}

bool FileSystem::hasExtension(std::string path, std::string extension)
{
	return getExtension(path) == extension;

}

std::string FileSystem::getName(std::string path)
{
	return path.substr(path.rfind("/")+1);

}

bool FileSystem::poll(std::string fpath)
{
    DIR* directory = isFolder(fpath);
    std::string path = fpath;

    if(directory == NULL)
    {
    	return false;

    }

    struct dirent* entry;
    entry = readdir(directory);
    while(entry != NULL)
    {
    	std::string name(entry->d_name);
        if(name != "." && name != "..")
        {
            path = path + "/" + name;
            if(!poll(path))
            {
                m_paths.push_back(path);

            }

            path = fpath;


        }

        entry = readdir(directory);

    }

    delete entry;
    closedir(directory);
    return true;

}
