#ifndef TEXTUREFACTORY_H_
#define TEXTUREFACTORY_H_

#include "../ResourceFactory.h"

class TextureFactory : public ResourceFactory
{
public:
	TextureFactory(std::string extension);
	~TextureFactory();

	static Resource* factory(std::string path);
	static void recycler(void* data);

};

#endif /* TEXTUREFACTORY_H_ */
