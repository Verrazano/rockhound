#ifndef SOUNDFACTORY_H_
#define SOUNDFACTORY_H_

#include "../ResourceFactory.h"

class SoundFactory : public ResourceFactory
{
public:
	SoundFactory(std::string extension);
	~SoundFactory();

	static Resource* factory(std::string path);
	static void recycler(void* data);

};



#endif /* SOUNDFACTORY_H_ */
