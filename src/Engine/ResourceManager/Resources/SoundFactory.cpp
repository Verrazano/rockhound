#include "SoundFactory.h"
#include <SFML/Audio.hpp>
#include "../Resource.h"
#include <iostream>

SoundFactory::SoundFactory(std::string extension) : ResourceFactory(extension, SoundFactory::factory, SoundFactory::recycler)
{
}

SoundFactory::~SoundFactory()
{
}

Resource* SoundFactory::factory(std::string path)
{
	sf::SoundBuffer* t = new sf::SoundBuffer;
	if(!t->loadFromFile(path))
	{
		delete t;
		return NULL;

	}

	Resource* r = new Resource(t, path, SoundFactory::recycler);
	return r;

}

void SoundFactory::recycler(void* data)
{
	delete (sf::SoundBuffer*)data;

}
