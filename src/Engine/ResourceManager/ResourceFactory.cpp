#include "ResourceFactory.h"
#include "Resource.h"

ResourceFactory::ResourceFactory()
{
}

ResourceFactory::ResourceFactory(std::string extension, ResourceFactory::Factory factory, ResourceFactory::Recycler recycler)
{
	m_extension = extension;
	m_factory = factory;
	m_recycler = recycler;

}

ResourceFactory::~ResourceFactory()
{
}

std::string ResourceFactory::getExtension()
{
	return m_extension;

}

ResourceFactory::Factory ResourceFactory::getFactory()
{
	return m_factory;

}

ResourceFactory::Recycler ResourceFactory::getRecycler()
{
	return m_recycler;

}


Resource* ResourceFactory::create(std::string path)
{
	return m_factory(path);

}

