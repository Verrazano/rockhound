#include "EngineResourceManager.h"
#include "Resources/TextureFactory.h"
#include "Resources/FontFactory.h"
#include "Resources/SoundFactory.h"

EngineResourceManager::EngineResourceManager()
{
	addFactory(TextureFactory("bmp"));
	addFactory(TextureFactory("png"));
	addFactory(TextureFactory("tga"));
	addFactory(TextureFactory("jpg"));
	addFactory(TextureFactory("gif"));
	addFactory(TextureFactory("psd"));
	addFactory(TextureFactory("pic"));

	addFactory(FontFactory("ttf"));
	addFactory(FontFactory("otf"));
	addFactory(FontFactory("fnt"));

	addFactory(SoundFactory("wav"));
	addFactory(SoundFactory("ogg"));
	addFactory(SoundFactory("flac"));

}
