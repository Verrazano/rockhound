#include "EngineSettings.h"
#include "../Entity/EntityManager/EntityManager.h"
#include <SFML/Graphics.hpp>
#include "Log/Log.h"
#include "../Game/GameMode.h"
#include "../Particles/ParticleManager.h"
#include "../Util/convert/convertstring.h"
#include "ResourceManager/EngineResourceManager.h"
#include "InputManager/InputManager.h"

class Engine
{
public:
	Engine();
	void setGameMode(GameMode* mode);

	~Engine();

	void tick();

	void onClose();

	bool isRunning();

	void out(std::string msg);
	void outBreak(); //prints \n====================\n

	void updateFrameText();

	void setViewTarget(sf::Vector2f pos);
	void setViewTarget(Entity* target, bool turnWith = false);

	sf::Vector2f getGameMousePos();
	sf::Vector2f getGUIMousePos();

	EngineSettings settings;

	//sfml window stuff
	sf::RenderWindow window;
	sf::Event event;
	sf::View gameView;
	bool turnWith;
	Entity* target;
	sf::View guiView;
	sf::FloatRect gameRect;

	Log log;

	unsigned int frame;
	sf::Font font;
	sf::Text frameText;
	sf::Clock frameTimer;
	float lastDt;
	unsigned int draws;

	//TODO: z-order
	//this is going to get ugly
	EntityManager entities;
	EntityManager* activeEntities;
	static const int viewWidth = 854*2;
	static const int viewHeight = 480*2;
	static const int guiWidth = 854;
	static const int guiHeight = 480;

	GameMode* gameMode;

	std::string addEntity(Entity* e);
	int randFrom(int min, int max);

	void drawText(std::string text, sf::Vector2f pos, sf::Color color = sf::Color::Black, float a = 0, unsigned int size = 14);
	void drawText(std::string text, sf::Vector2f pos, sf::Font& font, sf::Color color = sf::Color::Black, float a = 0, unsigned int size = 14, bool center = false);
	void drawPane(sf::Vector2f pos, sf::Vector2f size, sf::Color color, float a = 0);
	void drawSprite(sf::Vector2f pos, sf::Texture* texture, float a = 0, float scale = 1);

	//when first pressed
	void updateMouseDown();
	bool mouseLeftDown();

	void updateEDown();
	bool EDown();

	sf::Text guiText;
	sf::RectangleShape guiSprite;
	ParticleManager particles;
	sf::VertexArray stars;
	bool mouseState;
	bool mouseDown;
	bool eState;
	bool ePressed;

	EngineResourceManager files;

	InputManager inputs;

	void setActiveEntities(EntityManager* manager);

	//nah, lets not mess with box2d junk not really need we can add random rotations and stuff
	//maybe "upgrade" to box2d later

};

extern Engine* game;


