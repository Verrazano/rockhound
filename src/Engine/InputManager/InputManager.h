#ifndef INPUTMANAGER_H_
#define INPUTMANAGER_H_

#include <list>
#include <string>
#include "ButtonCallback.h"
#include "ValueCallback.h"

class InputManager
{
public:
	InputManager();
	virtual ~InputManager();

	void update();

	void addButton(ButtonCallback::Callback callback, std::string name);
	void addValue(ValueCallback::Callback callback, std::string name);

	bool hasButton(std::string name);
	bool hasValue(std::string name);

	void remButton(std::string name);
	void remValue(std::string name);

	bool isState(std::string name, ButtonCallback::State state);
	float getValue(std::string name);

	ButtonCallback* getButtonCallback(std::string name);
	ValueCallback* getValueCallback(std::string name);

private:
	std::list<ButtonCallback> m_buttons;
	std::list<ValueCallback> m_values;

};

#endif /* INPUTMANAGER_H_ */
