#include "ValueCallback.h"

ValueCallback::ValueCallback(ValueCallback::Callback callback, std::string name)
{
	m_callback = callback;
	m_name = name;
	m_value = 0.0f;

}

ValueCallback::~ValueCallback()
{
}

float ValueCallback::getValue()
{
	return m_value;

}

void ValueCallback::update()
{
	m_value = m_callback();

}

std::string ValueCallback::getName()
{
	return m_name;

}
