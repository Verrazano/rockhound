#include "ButtonCallback.h"

ButtonCallback::ButtonCallback(ButtonCallback::Callback callback, std::string name)
{
	m_callback = callback;
	m_name = name;
	m_state = ButtonCallback::notPressed;

}

ButtonCallback::~ButtonCallback()
{
}

ButtonCallback::State ButtonCallback::getState()
{
	return m_state;

}

bool ButtonCallback::isState(ButtonCallback::State state)
{
	if(state == ButtonCallback::pressed)
	{
		return m_state == ButtonCallback::pressed || m_state == ButtonCallback::justPressed;

	}
	else if(state == ButtonCallback::notPressed)
	{
		return m_state == ButtonCallback::notPressed || m_state == ButtonCallback::justReleased;

	}

	return m_state == state;

}

void ButtonCallback::update()
{
	bool pressed = m_callback();
	if(pressed)
	{
		if(m_state == ButtonCallback::justReleased || m_state == ButtonCallback::notPressed)
		{
			m_state = ButtonCallback::justPressed;
			return;

		}

		m_state = ButtonCallback::pressed;

	}
	else
	{
		if(m_state == ButtonCallback::justPressed || m_state == ButtonCallback::pressed)
		{
			m_state = ButtonCallback::justReleased;
			return;

		}

		m_state = ButtonCallback::notPressed;

	}

}

std::string ButtonCallback::getName()
{
	return m_name;

}
