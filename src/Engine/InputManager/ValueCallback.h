#ifndef VALUECALLBACK_H_
#define VALUECALLBACK_H_

#include <string>
#include <functional>

class ValueCallback
{
public:
	typedef std::function<float(void)> Callback;

	ValueCallback(Callback callback, std::string name);
	virtual ~ValueCallback();

	float getValue();

	void update();

	std::string getName();

private:
	Callback m_callback;
	std::string m_name;
	float m_value;

};

#endif /* VALUECALLBACK_H_ */
