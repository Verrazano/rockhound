#ifndef BUTTONCALLBACK_H_
#define BUTTONCALLBACK_H_

#include <string>
#include <functional>

class ButtonCallback
{
public:
	typedef std::function<bool(void)> Callback;

	ButtonCallback(ButtonCallback::Callback callback, std::string name);
	virtual ~ButtonCallback();

	typedef enum State
	{
		pressed = 0,
		justPressed,
		justReleased,
		notPressed

	} State;

	ButtonCallback::State getState();
	bool isState(ButtonCallback::State state);

	void update();

	std::string getName();

private:
	ButtonCallback::Callback m_callback;
	std::string m_name;

	ButtonCallback::State m_state;

};

#endif /* BUTTONCALLBACK_H_ */
