#include "InputManager.h"

InputManager::InputManager()
{
}

InputManager::~InputManager()
{
}

void InputManager::update()
{
	std::list<ButtonCallback>::iterator it;
	for(it = m_buttons.begin(); it != m_buttons.end(); it++)
	{
		it->update();

	}

	std::list<ValueCallback>::iterator it2;
	for(it2 = m_values.begin(); it2 != m_values.end(); it2++)
	{
		it2->update();

	}

}

void InputManager::addButton(ButtonCallback::Callback callback, std::string name)
{
	if(hasButton(name))
	{
		return;

	}

	m_buttons.push_back(ButtonCallback(callback, name));

}

void InputManager::addValue(ValueCallback::Callback callback, std::string name)
{
	if(hasValue(name))
	{
		return;

	}

	m_values.push_back(ValueCallback(callback, name));

}

bool InputManager::hasButton(std::string name)
{
	return getButtonCallback(name) != NULL;

}

bool InputManager::hasValue(std::string name)
{
	return getValueCallback(name) != NULL;

}

void InputManager::remButton(std::string name)
{
	std::list<ButtonCallback>::iterator it;
	for(it = m_buttons.begin(); it != m_buttons.end(); it++)
	{
		if(it->getName() == name)
		{
			m_buttons.erase(it);
			return;

		}

	}

}

void InputManager::remValue(std::string name)
{
	std::list<ValueCallback>::iterator it;
	for(it = m_values.begin(); it != m_values.end(); it++)
	{
		if(it->getName() == name)
		{
			m_values.erase(it);
			return;

		}

	}

}

bool InputManager::isState(std::string name, ButtonCallback::State state)
{
	if(!hasButton(name))
	{
		if(state == ButtonCallback::notPressed)
		{
			return true;

		}

		return false;

	}

	return getButtonCallback(name)->isState(state);
}

float InputManager::getValue(std::string name)
{
	if(!hasValue(name))
	{
		return 0.0f;

	}

	return getValueCallback(name)->getValue();

}

ButtonCallback* InputManager::getButtonCallback(std::string name)
{
	std::list<ButtonCallback>::iterator it;
	for(it = m_buttons.begin(); it != m_buttons.end(); it++)
	{
		if(it->getName() == name)
		{
			return &(*it);

		}

	}

	return NULL;

}

ValueCallback* InputManager::getValueCallback(std::string name)
{
	std::list<ValueCallback>::iterator it;
	for(it = m_values.begin(); it != m_values.end(); it++)
	{
		if(it->getName() == name)
		{
			return &(*it);

		}

	}

	return NULL;

}
