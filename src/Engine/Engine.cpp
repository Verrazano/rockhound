#include "Engine.h"
#include "../Scripting/Config/Config.h"
#include <iostream>
#include "../Util/convert/convertstring.h"
#include <time.h>
#include "../Util/convert/mousetoview.h"

Engine* game;

void Engine::setGameMode(GameMode* mode)
{
	gameMode = mode;

}

Engine::Engine()
{
	out("Initializing game engine");
	std::cout << "engine: " << this << "\n";
	outBreak();
	out("reading config: settings.cfg");

	if(!settings.read(Config::load("settings.cfg")))
	{
		out("couldn't read, writing default config");
		settings.write("settings.cfg");

	}

	if(settings.frameRate < 20.0f)
	{
		settings.frameRate = 20.0f;

	}

	if(!settings.useSeed)
	{
		settings.seed = time(NULL);

	}

	srand(settings.seed);

	outBreak();
	out("settings");
	out("frameRate = " + toString(settings.frameRate));
	out("windowWidth = " + toString(settings.width));
	out("windowHeight = " + toString(settings.height));
	out("antiAliasing = " + toString(settings.antiAliasing));
	out("is full screen mode? " + toString(settings.fullScreen));
	out("use user seed? " + toString(settings.useSeed));
	out("seed = " + toString(settings.seed));
	out("worldSize = " + toString(settings.worldSize));
	out("debug on? " + toString(settings.debug));
	out("resourcePath " + settings.resourcePath);
	out("font " + settings.font);
	out("mute? " + toString(settings.muteSound));
	out("volume = " + toString(settings.volume));
	outBreak();

	files.search(settings.resourcePath);
	out("files: " + toString(files.getNumOfPaths()));
	for(unsigned int i = 0; i < files.getNumOfPaths(); i++)
	{
		out("precaching: \"" + files.getPath(i) + "\"");

	}

	outBreak();

	if(!files.has(settings.font))
	{
		out("couldn't load consolas font :C");

	}
	else
	{
		font = *files.get<sf::Font>(settings.font);

	}

	frameText.setFont(font);
	frameText.setColor(sf::Color::Red);
	frameText.setCharacterSize(14);

	out("creating window");

	setActiveEntities(&entities);

	window.setFramerateLimit(settings.frameRate);
	sf::ContextSettings windowSettings;
	windowSettings.antialiasingLevel = settings.antiAliasing;
	if(settings.fullScreen)
	{
		out("checking available full screen modes");
		std::vector<sf::VideoMode> modes = sf::VideoMode::getFullscreenModes();
		for(unsigned int i = 0; i < modes.size(); i++)
		{
			out("["+toString(i)+"] width: " + toString(modes[i].width) + " height: " + toString(modes[i].height) + " bpp: " + toString(modes[i].bitsPerPixel));

		}

		out("using mode [0]");

		window.create(sf::VideoMode::getFullscreenModes()[0], "Beneath The Surface - LD29", sf::Style::Fullscreen, windowSettings);

	}
	else
	{
		window.create(sf::VideoMode(settings.width, settings.height), "Beneath The Surface - LD29", sf::Style::Close, windowSettings);

	}

	gameRect = sf::FloatRect(0, 0, viewWidth, viewHeight);
	gameView = sf::View(gameRect);
	target = NULL;
	guiView = sf::View(sf::FloatRect(0, 0, guiWidth, guiHeight));

	//width = 854;
	//height = 480;

	frame = 0;
	lastDt = 0;

	if(gameMode == NULL)
	{
		outBreak();
		out("WARNING -- gameMode is null");

	}

	turnWith = false;

	stars = sf::VertexArray(sf::Points, 10000);
	for(unsigned int i = 0; i < 10000; i++)
	{
		sf::Vector2f pos = sf::Vector2f(randFrom(-settings.worldSize, settings.worldSize), randFrom(-settings.worldSize, settings.worldSize));
		stars[i] = sf::Vertex(pos, sf::Color::White);

	}

	mouseState = false;
	mouseDown = false;

	inputs.addValue([this]() -> float {return sf::Mouse::getPosition(this->window).x;}, "mouseX");
	inputs.addValue([this]() -> float {return sf::Mouse::getPosition(this->window).y;}, "mouseY");

	inputs.addButton([]() -> bool {return sf::Mouse::isButtonPressed(sf::Mouse::Left);}, "lmb");
	inputs.addButton([]() -> bool {return sf::Mouse::isButtonPressed(sf::Mouse::Right);}, "rmb");

}

Engine::~Engine()
{
}

void Engine::tick()
{
	while(window.pollEvent(event))
	{
		if(event.type == sf::Event::Closed)
		{
			onClose();

		}

	}

	updateMouseDown();
	updateEDown();

	inputs.update();

	//on tick

	if(gameMode != NULL && gameMode->started)
	{
		gameMode->onTick();

	}

	activeEntities->handleCollisons();

	if(target != NULL)
	{
		sf::Vector2f pos = target->getSpritePos();
		gameRect = sf::FloatRect(pos.x - viewWidth/2, pos.y - viewHeight/2, viewWidth, viewHeight);
		gameView = sf::View(gameRect);
		if(turnWith)
		{
			//+90 to correct sideways ness
			gameView.setRotation(target->getAngle() + 90);

		}

	}

	window.clear(sf::Color(104, 40, 96));
	//draw stuffs

	//game objects
	window.setView(gameView);

	window.draw(stars);

	if(frame%settings.particleUpdateRate == 0)
	{
		//update particles if ok
		particles.update();

	}

	particles.draw(window);

	draws = 0;
	std::vector<Entity*>& te = activeEntities->entities;
	for(unsigned int i = 0; i < te.size(); i++)
	{
		if(frame%te[i]->updateRate == 0)
		{
			te[i]->onTick();

		}

		te[i]->updateBody();

		sf::FloatRect body = te[i]->getBody();
		if(gameRect.intersects(body))
		{
			te[i]->updateSprites();
			window.draw(te[i]->sprite);
			draws++;

			if(settings.debug)
			{
				window.draw(te[i]->body);

			}

			te[i]->onDraw();

		}

	}

	//gui objects
	window.setView(guiView);

	if(gameMode != NULL && gameMode->started)
	{
		gameMode->onDraw();

	}

	//do menu drawing and check if game starts if so start game
	if(gameMode != NULL && !gameMode->started && gameMode->onMenu())
	{
		entities.entities.clear();
		activeEntities->entities.clear();
		setActiveEntities(&entities);
		gameMode->onStart();
		gameMode->started = true;
		std::cout << "entities: " << activeEntities->entities.size() << "\n";

	}

	if(settings.debug)
	{
		updateFrameText();
		window.draw(frameText);

	}

	for(unsigned int i = 0; i < activeEntities->entities.size(); i++)
	{
		Entity* e = activeEntities->entities[i];
		if(e->isDead())
		{
			e->onDeath();
			if(gameMode != NULL)
			{
				gameMode->onEntityDestroyed(e);

			}

			activeEntities->remEntity(e);
			i--;
			continue;

		}

	}

	window.display();

	if(gameMode->started)
	{
		if(gameMode != NULL && gameMode->isGameOver())
		{
			activeEntities->clear();
			entities.entities.clear();
			gameMode->started = false;

		}

	}

	lastDt = frameTimer.restart().asSeconds();
	frame++;

}

void Engine::onClose()
{
	out("application close requested");
	//maybe ask the player if they are sure
	window.close();

}

bool Engine::isRunning()
{
	return window.isOpen();

}

void Engine::out(std::string msg)
{
	std::cout << msg << "\n";
	log.append(msg);


}

void Engine::outBreak()
{
	out("\n====================\n");

}

void Engine::updateFrameText()
{
	float dt = frameTimer.getElapsedTime().asSeconds();
	float frameRate = 1.0f/dt;
	std::string frameInfo;
	frameInfo += "ms: " + toString(dt*1000.0f) + "\n";
	frameInfo += "fps: " + toString(frameRate) + "\n";
	frameInfo += "frame: " + toString(frame) + "\n";
	frameInfo += "draws: " + toString(draws);
	frameText.setString(frameInfo);

}

void Engine::setViewTarget(sf::Vector2f pos)
{
	turnWith = false;
	target = NULL;
	gameRect = sf::FloatRect(pos.x - viewWidth/2, pos.y - viewHeight/2, viewWidth, viewHeight);
	gameView = sf::View(gameRect);

}

void Engine::setViewTarget(Entity* target, bool turnWith)
{
	this->turnWith = turnWith;
	this->target = target;

}

sf::Vector2f Engine::getGameMousePos()
{
	return mouseToView(gameView, window);

}

sf::Vector2f Engine::getGUIMousePos()
{
	return mouseToView(guiView, window);

}

std::string Engine::addEntity(Entity* e)
{
	std::string id = entities.addEntity(e);
	if(gameMode != NULL)
	{
		gameMode->onEntityCreated(e);

	}

	return id;

}

int Engine::randFrom(int min, int max)
{
	return (rand()%(max-min))+min;

}


void Engine::drawText(std::string text, sf::Vector2f pos, sf::Color color, float a, unsigned int size)
{
	drawText(text, pos, font, color, a, size);

}

void Engine::drawText(std::string text, sf::Vector2f pos, sf::Font& font, sf::Color color, float a, unsigned int size, bool center)
{
	guiText.setString(text);
	guiText.setPosition((int)pos.x, (int)pos.y);
	guiText.setColor(color);
	guiText.setCharacterSize(size);
	guiText.setFont(font);
	if(center)
	{
		guiText.setOrigin(guiText.getLocalBounds().width/2, guiText.getLocalBounds().height/2);

	}
	else
	{
		guiText.setOrigin(0, 0);

	}
	guiText.setRotation(a);
	window.draw(guiText);

}

void Engine::drawPane(sf::Vector2f pos, sf::Vector2f size, sf::Color color, float a)
{
	guiSprite.setRotation(a);
	guiSprite.setTexture(NULL);
	guiSprite.setSize(size);
	guiSprite.setOrigin(0, 0);
	guiSprite.setPosition(pos);
	guiSprite.setScale(1, 1);
	guiSprite.setFillColor(color);
	window.draw(guiSprite);

}

void Engine::drawSprite(sf::Vector2f pos, sf::Texture* texture, float a, float scale)
{
	if(texture == NULL)
		return;

	sf::Vector2u size = texture->getSize();
	guiSprite.setSize(sf::Vector2f(size.x, size.y));
	guiSprite.setOrigin(size.x/2, size.y/2);
	guiSprite.setPosition(pos);
	guiSprite.setScale(scale, scale);
	guiSprite.setFillColor(sf::Color::White);
	guiSprite.setTexture(texture);
	guiSprite.setRotation(a);
	window.draw(guiSprite);

}

void Engine::updateMouseDown()
{
	if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
		if(!mouseState)
		{
			mouseDown = true;
			mouseState = true;
			return;

		}

		mouseDown = false;

	}
	else
	{
		mouseDown = false;
		mouseState = false;

	}

}

bool Engine::mouseLeftDown()
{
	return mouseDown;

}

void Engine::updateEDown()
{
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::E))
	{
		if(!eState)
		{
			ePressed = true;
			eState = true;
			return;

		}

		ePressed = false;

	}
	else
	{
		ePressed = false;
		eState = false;

	}

}

bool Engine::EDown()
{
	return ePressed;

}

void Engine::setActiveEntities(EntityManager* manager)
{
	if(manager == NULL)
	{
		activeEntities = &entities;
		return;

	}

	activeEntities = manager;

}
