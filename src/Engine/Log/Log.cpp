#include "Log.h"
#include <ctime>
#include "../../Util/convert/convertstring.h"
#include <iostream>

Log::Log()
{
	name = "logs/log";
	file.open(getLogTitle().c_str());

}

Log::Log(std::string name)
{
	this->name = name;
	file.open(getLogTitle().c_str());

}

Log::~Log()
{
	file.close();

}

void Log::append(std::string s)
{
	if(file)
	{
		file << s << "\n";
		file.flush();

	}

}

std::string Log::getName()
{
	return name;

}

void Log::setName(std::string name)
{
	this->name = name;

}

std::string Log::getLogTitle()
{
	std::string m, d, y, h, min, s;
	time_t rawtime;
	time(&rawtime);
	tm curt = *localtime(&rawtime);
	m = toString((1+curt.tm_mon)%12);
	d = toString(curt.tm_mday%30);
	y = toString(curt.tm_year+1900);
	h = toString(curt.tm_hour%23);
	min = toString(curt.tm_min%58);
	s = toString(curt.tm_sec%59);

	return getName() + "-" + m + "-" + d + "-" + y + "-" + h + "-" + min + "-" + s + ".txt";

}
