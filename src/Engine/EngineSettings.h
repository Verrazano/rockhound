#ifndef ENGINESETTINGS_H_
#define ENGINESETTINGS_H_

#include "../Scripting/Settings/Settings.h"

class EngineSettings : public Settings
{
public:
	EngineSettings()
	{
		//default settings
		frameRate = 60.0f;
		width = 854;
		height = 480;
		antiAliasing = 8;
		fullScreen = false;
		useSeed = false;
		seed = 0;
		//in meters
		worldSize = 10000.0f;
		debug = false;
		particleUpdateRate = 1;
		resourcePath = "res";
		font = "consolas.ttf";
		muteSound = false;
		volume = 100.0f;

		//setup reader
		add("frameRate", offsetof(EngineSettings, frameRate), frameRate);
		add("width", offsetof(EngineSettings, width), width);
		add("height", offsetof(EngineSettings, height), height);
		add("antiAliasing", offsetof(EngineSettings, antiAliasing), antiAliasing);
		add("fullScreen", offsetof(EngineSettings, fullScreen), fullScreen);
		add("useSeed", offsetof(EngineSettings, useSeed), useSeed);
		add("seed", offsetof(EngineSettings, seed), seed);
		add("worldSize", offsetof(EngineSettings, worldSize), worldSize);
		add("debug", offsetof(EngineSettings, debug), debug);
		add("particleUpdateRate", offsetof(EngineSettings, particleUpdateRate), particleUpdateRate);
		add("resourcePath", offsetof(EngineSettings, resourcePath), resourcePath);
		add("font", offsetof(EngineSettings, font), font);
		add("muteSound", offsetof(EngineSettings, muteSound), muteSound);
		add("volume", offsetof(EngineSettings, volume), volume);

	}

	float frameRate;
	uint32_t width;
	uint32_t height;
	uint32_t antiAliasing;
	bool fullScreen;
	bool useSeed;
	int seed;
	float worldSize;
	bool debug;
	unsigned int particleUpdateRate;
	std::string resourcePath;
	std::string font;
	bool muteSound;
	float volume;

};

#endif /* ENGINESETTINGS_H_ */
