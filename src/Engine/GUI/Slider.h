#ifndef SLIDER_H_
#define SLIDER_H_

#include <SFML/Graphics.hpp>

class Slider
{
public:
	Slider();
	Slider(sf::Vector2f pos, sf::Vector2f size, float max);

	bool draw(sf::RenderWindow& window);

	float getValue();

	void setValue(float val);

	sf::RectangleShape button;
	sf::RectangleShape line;
	float max;
	float val;
	bool pressed;

};

#endif /* SLIDER_H_ */
