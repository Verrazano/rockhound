#include "Slider.h"
#include "../Engine.h"

Slider::Slider()
{
}

Slider::Slider(sf::Vector2f pos, sf::Vector2f size, float max)
{
	line.setSize(sf::Vector2f(size.x, size.y/8));
	line.setOrigin(sf::Vector2f(size.x/2, (size.y/8)/2));
	line.setFillColor(sf::Color(50, 50, 50));
	line.setPosition(pos);

	button.setSize(sf::Vector2f(size.x/10, size.y));
	button.setOrigin((size.x/10)/2, size.y/2);
	button.setFillColor(sf::Color(50, 50, 50, 100));
	button.setPosition(pos);
	button.setOutlineThickness(2);
	button.setOutlineColor(sf::Color::Black);

	this->max = max;
	setValue(0);
	pressed = false;

}

bool Slider::draw(sf::RenderWindow& window)
{
	window.draw(line);
	window.draw(button);

	bool mouseDown = game->inputs.isState("lmb", ButtonCallback::pressed);

	sf::Vector2f guiMouse = game->getGUIMousePos();
	float leftBound = line.getPosition().x - line.getSize().x/2;
	float rightBound = line.getPosition().x + line.getSize().x/2;
	if((pressed && mouseDown) || (mouseDown &&
			(line.getGlobalBounds().contains(guiMouse) || button.getGlobalBounds().contains(guiMouse))))
	{
		pressed = true;
		if(guiMouse.x < leftBound)
		{
			guiMouse.x = leftBound;

		}
		else if(guiMouse.x > rightBound)
		{
			guiMouse.x = rightBound;

		}

		button.setPosition(guiMouse.x, line.getPosition().y);
		sf::Vector2f s = line.getSize();
		guiMouse.x -= line.getPosition().x;
		float v = (guiMouse.x/(s.x/max))+max/2;
		val = v;
		return mouseDown;


	}
	else
	{
		pressed = false;

	}

	return false;

}

float Slider::getValue()
{
	return val;

}

void Slider::setValue(float val)
{
	val -= max/2;
	this->val = val;
	sf::Vector2f s = line.getSize();
	float p = val*(s.x/max);
	button.setPosition(p+line.getPosition().x, line.getPosition().y);

}
