#include "Button.h"
#include "Engine.h"

Button::Button(sf::Vector2f pos, sf::Vector2f size, std::string text, sf::Font& font)
{
	button.setSize(size);
	button.setOrigin(size.x/2, size.y/2);
	button.setFillColor(sf::Color(50, 50, 50, 100));
	button.setPosition(pos);
	button.setOutlineThickness(2);
	button.setOutlineColor(sf::Color::Black);

	highlight = button;
	highlight.setFillColor(sf::Color(255, 255, 0, 100));

	btext.setFont(font);
	btext.setColor(sf::Color::White);
	btext.setCharacterSize(14);
	btext.setString(text);

	sf::FloatRect rect = btext.getLocalBounds();
	btext.setPosition(pos.x - rect.width/2, pos.y - rect.height/2);

}

Button::Button(sf::Vector2f pos, sf::Vector2f size, sf::Texture* iconText, sf::Vector2f iconSize)
{
	button.setSize(size);
	button.setOrigin(size.x/2, size.y/2);
	button.setFillColor(sf::Color(50, 50, 50, 100));
	button.setPosition(pos);
	button.setOutlineThickness(2);
	button.setOutlineColor(sf::Color::Black);

	highlight = button;
	highlight.setFillColor(sf::Color(255, 255, 0, 100));

	icon.setSize(iconSize);
	icon.setOrigin(iconSize.x/2, iconSize.y/2);
	icon.setTexture(iconText);
	icon.setPosition(pos);

}

Button::Button(sf::Vector2f pos, sf::Vector2f size, sf::Texture* iconText, sf::Vector2f iconSize, std::string text, sf::Font& font)
{
	button.setSize(size);
	button.setOrigin(size.x/2, size.y/2);
	button.setFillColor(sf::Color(50, 50, 50, 100));
	button.setPosition(pos);
	button.setOutlineThickness(2);
	button.setOutlineColor(sf::Color::Black);

	highlight = button;
	highlight.setFillColor(sf::Color(255, 255, 0, 100));

	icon.setSize(iconSize);
	icon.setOrigin(iconSize.x/2, iconSize.y/2);
	icon.setTexture(iconText);
	icon.setPosition(pos);

	btext.setFont(font);
	btext.setColor(sf::Color::White);
	btext.setCharacterSize(14);
	btext.setString(text);

	sf::FloatRect rect = btext.getLocalBounds();
	btext.setPosition(pos.x - rect.width/2, pos.y - rect.height/2);

}

bool Button::draw(sf::RenderWindow& window)
{
	window.draw(button);
	window.draw(icon);
	window.draw(btext);

	bool mouseDown = game->inputs.isState("lmb", ButtonCallback::justPressed);
	sf::Vector2f guiMouse = game->getGUIMousePos();
	if(button.getGlobalBounds().contains(guiMouse))
	{
		window.draw(highlight);
		return mouseDown;

	}

	return false;

}
