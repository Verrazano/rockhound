#include "Member.h"

BoolMember::BoolMember(std::string name, size_t offset, bool value, std::string type) :
	Member(name, offset, value, type)
{
}

BoolMember::~BoolMember()
{
}

std::string BoolMember::getValue(char* base)
{
    bool* member = NULL;
    member = (bool*)(base + getOffset());
    m_value = (*member);
    return m_value ? "true" : "false";

}
