#ifndef MEMBERBASE_H_
#define MEMBERBASE_H_

#include <string>

class MemberBase
{
public:
	MemberBase(std::string name, size_t offset);
	virtual ~MemberBase();

	std::string write(char* base);

	virtual std::string getType() = 0;
	virtual std::string getValue(char* base) = 0;

	virtual void set(char* base, void* value) = 0;

	std::string getName();
	size_t getOffset();

private:
	std::string m_name;
	size_t m_offset;

};

#endif /* MEMBERBASE_H_ */
