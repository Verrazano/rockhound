#ifndef SETTINGS_H_
#define SETTINGS_H_

#include <list>
#include <string>
#include <stddef.h>
#include <stdint.h>

#include "MemberBase.h"

class Settings
{
public:
	Settings();
	virtual ~Settings();

	bool read(std::string file);
	bool write(std::string path);

	void add(std::string name, size_t offset, int32_t value);
	void add(std::string name, size_t offset, uint32_t value);
	void add(std::string name, size_t offset, float value);
	void add(std::string name, size_t offset, bool value);
	void add(std::string name, size_t offset, std::string value);

private:
	std::list<MemberBase*> m_members;

};

#endif /* SETTINGS_H_ */
