#include "Config.h"
#include <fstream>
#include <sstream>
#include <iostream>

Config::Config()
{
}

Config::Config(std::string path)
{
	setPath(path);

}

//if blank uses getPath()
//returns a bool on success
bool Config::read(std::string path)
{
	if(path != "")
		setPath(path);

	if(path == "")
		return "";

	std::string fileStr = load(path);

	if(fileStr == "")
		return false;

	parse(fileStr);
	return true;

}

bool Config::write(std::string path)
{
	if(path != "")
		setPath(path);

	std::ofstream f;
	f.open(m_path.c_str());

	for(unsigned int i = 0; i < getNumOfItems(); i++)
	{
		f << getItem(i).getLine() << ";\n";

	}

	return true;

}

std::string Config::load(std::string path)
{
	if(path == "")
		return "";

	std::fstream f;
	f.open(path.c_str());

	if(!f)
		return "";

	std::string fileStr;
	while(!f.eof())
	{
		std::string line;
		std::getline(f, line);
		//removes comments and combines lines
		if(line.find("//"))
		{
			line = line.substr(0, line.find("//"));

		}

		if(line.find("/*") < line.size())
		{
			std::string l2;
			while(l2.find("*/") > l2.size() && !f.eof())
			{
				std::getline(f, l2);
				line += l2;

			}
			if(line.find("*/") < line.size())
			{
				line = line.substr(line.find("/*"), line.find("*/")+2);
			}
			else
				line = "";

		}

		while(line.find("\r") < line.size()){ line.replace(line.find("\r"), 1, ""); }
		while(line.find("\t") < line.size()){ line.replace(line.find("\t"), 1, ""); }

		fileStr += line;

	}

	return fileStr;

}

void Config::parse(std::string file)
{
	if(file == "")
		return;

	m_items.clear();

	m_fileStr = file;
	std::istringstream f(m_fileStr);

	while(!f.eof())
	{
		std::string cline;
		std::getline(f, cline);

		while(cline.find(";") < cline.size())
		{
	         std::string line = cline.substr(0, cline.find(";"));
	         cline = cline.substr(cline.find(";")+1);
	         addItem(line);

		}

	}

}

void Config::setPath(std::string path)
{
	m_path = path;

}

std::string Config::getPath()
{
	return m_path;

}

std::string Config::getFile()
{
	return m_fileStr;

}

ConfigItem Config::getItem(std::string key)
{
	return getItem(getIndex(key));

}

bool Config::hasItem(std::string key)
{
	return getIndex(key) != -1;

}

std::string Config::getType(std::string key)
{
	return getType(getIndex(key));

}

std::string Config::getLine(std::string key)
{
	return getLine(getIndex(key));

}

std::string Config::getValue(std::string key)
{
	return getValue(getIndex(key));

}

std::vector<std::string> Config::getParams(std::string key)
{
	return getParams(getIndex(key));

}

unsigned int Config::getNumOfParams(std::string key)
{
	return getNumOfParams(getIndex(key));

}

std::string Config::getParam(std::string key, unsigned int index)
{
	return getParam(getIndex(key), index);

}

int Config::getIndex(std::string key)
{
	for(unsigned int i = 0; i < m_items.size(); i++)
	{
		if(m_items[i].getKey() == key)
		{
			return i;

		}

	}

	return -1;

}

unsigned int Config::getNumOfItems()
{
	return m_items.size();

}

ConfigItem Config::getItem(unsigned int index)
{
	if(!hasItem(index))
	{
		ConfigItem c;
		return c;

	}

	return m_items[index];

}

bool Config::hasItem(unsigned int index)
{
	return m_items.size() > index;

}

std::string Config::getKey(unsigned int index)
{
	if(!hasItem(index))
		return "";
	return m_items[index].getKey();

}

std::string Config::getType(unsigned int index)
{
	if(!hasItem(index))
		return "";
	return m_items[index].getType();

}

std::string Config::getLine(unsigned int index)
{
	if(!hasItem(index))
		return "";
	return m_items[index].getLine();

}

std::string Config::getValue(unsigned int index)
{
	if(!hasItem(index))
		return "";
	return m_items[index].getValue();

}

std::vector<std::string> Config::getParams(unsigned int index)
{
	if(!hasItem(index))
	{
		std::vector<std::string> v;
		return v;

	}

	return m_items[index].getParams();

}

unsigned int Config::getNumOfParams(unsigned int index)
{
	if(!hasItem(index))
		return 0;
	return m_items[index].getNumOfParams();

}

std::string Config::getParam(unsigned int itemIndex, unsigned int paramIndex)
{
	if(!hasItem(itemIndex))
		return "";
	return m_items[itemIndex].getParam(paramIndex);

}

void Config::remItem(std::string key)
{
	std::vector<ConfigItem>::iterator it;
	for(it = m_items.begin(); it != m_items.end(); it++)
	{
		if(it->getKey() == key)
		{
			m_items.erase(it);
			return;

		}

	}

}

void Config::remItem(unsigned int index)
{
	remItem(getKey(index));

}

void Config::addItem(ConfigItem item)
{
	if(item.getLine() == "")
		return;

	if(hasItem(item.getKey()))
		remItem(item.getKey());

	m_items.push_back(item);

}

void Config::addItem(std::string line)
{
	ConfigItem c;
	if(parseItem(line, c))
	{
		addItem(c);

	}

}

//returns true on success
bool Config::parseItem(std::string line, ConfigItem& item)
{
	if(line == "")
		return false;

	//removes spaces before the sections first none space
	line = line.substr(line.find_first_not_of(" "));

	//removes spaces after the sections last none space character
	line = line.substr(0, line.find_last_not_of(" ")+1);

	std::string fullline = line;

	//finds the type of the variable
	std::string type = line.substr(0, line.find_first_of(" "));
	if(type == "")
		return false;

	line = line.substr(line.find_first_of(" ")+1);
	line = line.substr(line.find_first_not_of(" "));

	//finds the variable name
	std::string key = line.substr(0, line.find_first_of(" "));
	if(key == "")
		return false;

	line = line.substr(line.find_first_of(" ")+1);
	line = line.substr(line.find_first_not_of(" "));

	if(!(line.find("=") < line.size()))
		return false;

	//gets value of the variable
	std::string value = line.substr(line.find("=")+1);
	if(value == "")
		return false;

	value = value.substr(value.find_first_not_of(" "));
	line = line.substr(line.find_first_not_of(" "));

	//parses the parameteres of the variable by checking for
	std::vector<std::string> params;
	std::string paramStr = value;

	while(paramStr.find(",") < paramStr.size())
	{
		std::string param = paramStr.substr(0, paramStr.find(","));
		paramStr = paramStr.substr(paramStr.find(",")+1);

		param = param.substr(param.find_first_not_of(" "));
		param = param.substr(0, param.find_last_not_of(" ")+1);

		if(param.find("\"") == 0)
			param = param.substr(1);

		if(param.find("\"") == param.size()-1)
			param = param.substr(0, param.size()-1);

		if(param == "")
			continue;

		params.push_back(param);

	}

	//removes spaces and stuff
	paramStr = paramStr.substr(paramStr.find_first_not_of(" "));
	paramStr = paramStr.substr(0, paramStr.find_last_not_of(" ")+1);

	//gets strings inbetween quotes
	if(paramStr.find("\"") == 0)
		paramStr = paramStr.substr(1);

	if(paramStr.find("\"") == paramStr.size()-1)
		paramStr =paramStr.substr(0, paramStr.size()-1);

	params.push_back(paramStr);

	if(value.find("\"") == 0)
		value = value.substr(1);

	//need to handle escaped quotes
	if(value.find("\"") == value.size()-1)
		value = value.substr(0, value.size()-1);

	item.m_key = key;
	item.m_type = type;
	item.m_line = fullline;
	item.m_value = value;
	item.m_params = params;

	return true;

}

bool Config::getInt(std::string key, int& val)
{
	if(getItem(key).getType() == "int")
	{
		val = getItem(key).valueAsInt();
		return true;

	}

	return false;

}

bool Config::getUint(std::string key, unsigned int& val)
{
	if(getItem(key).getType() == "uint")
	{
		val = getItem(key).valueAsUInt();
		return true;

	}

	return false;

}

bool Config::getFloat(std::string key, float& val)
{
	if(getItem(key).getType() == "float")
	{
		val = getItem(key).valueAsFloat();
		return true;

	}

	return false;

}

bool Config::getString(std::string key, std::string& val)
{
	if(getItem(key).getType() == "string")
	{
		val = getItem(key).getValue();
		return true;

	}

	return false;

}
