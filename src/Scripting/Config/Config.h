#ifndef CONFIG_H_
#define CONFIG_H_

#include <string>
#include <vector>
#include "ConfigItem.h"

class Config
{
public:
	Config();
	Config(std::string path);

	//if blank uses getPath()
	//returns a bool on success
	bool read(std::string path = "");
	bool write(std::string path = "");
	static std::string load(std::string path = "");

	void parse(std::string file);

	void setPath(std::string path);
	std::string getPath();

	std::string getFile();

	ConfigItem getItem(std::string key);
	bool hasItem(std::string key);
	std::string getType(std::string key);
	std::string getLine(std::string key);
	std::string getValue(std::string key);
	std::vector<std::string> getParams(std::string key);
	unsigned int getNumOfParams(std::string key);
	std::string getParam(std::string key, unsigned int index);
	//returns -1 if not succesfull
	int getIndex(std::string key);

	unsigned int getNumOfItems();
	ConfigItem getItem(unsigned int index);
	bool hasItem(unsigned int index);
	std::string getKey(unsigned int index);
	std::string getType(unsigned int index);
	std::string getLine(unsigned int index);
	std::string getValue(unsigned int index);
	std::vector<std::string> getParams(unsigned int index);
	unsigned int getNumOfParams(unsigned int index);
	std::string getParam(unsigned int itemIndex, unsigned int paramIndex);

	void remItem(std::string key);
	void remItem(unsigned int index);

	void addItem(ConfigItem item);
	void addItem(std::string line);

	//doesn't change val if the key doesn't exist
	bool getInt(std::string key, int& val);
	bool getUint(std::string key, unsigned int& val);
	bool getFloat(std::string key, float& val);
	bool getString(std::string key, std::string& val);

	//returns true on success
	bool parseItem(std::string line, ConfigItem& item);

private:
	std::string m_path;
	std::string m_fileStr;
	std::vector<ConfigItem> m_items;

};

#endif /* CONFIG_H_ */
